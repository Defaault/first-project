package math.education.com.mathModel.geometry;

import math.education.com.util.CalculatorUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Random;

import static org.junit.Assert.assertEquals;

@RunWith(PowerMockRunner.class)
@PrepareForTest(CalculatorUtil.class)
public class Task24Test {

    private static final String PATH_PICTURE = "task24.png";

    @Mock
    private Random random;

    @InjectMocks
    private Task24 task24;

    @Test
    public void task1() {
        PowerMockito.mockStatic(CalculatorUtil.class);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 2, 50))
                .thenReturn(3)
                .thenReturn(2);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 1, 10))
                .thenReturn(4)
                .thenReturn(3);

        task24.generateTask();

        assertEquals(PATH_PICTURE, task24.getPathPicture());
        assertEquals(String.valueOf(4), task24.getCorrectAnswer());
        assertEquals(String.valueOf(3), task24.getAnswerExample());
    }

    @Test
    public void task2() {
        PowerMockito.mockStatic(CalculatorUtil.class);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 2, 50))
                .thenReturn(2);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 1, 10))
                .thenReturn(3);

        task24.generateTask();

        assertEquals(PATH_PICTURE, task24.getPathPicture());
        assertEquals(String.valueOf(4), task24.getCorrectAnswer());
        assertEquals(String.valueOf(3), task24.getAnswerExample());
    }
}