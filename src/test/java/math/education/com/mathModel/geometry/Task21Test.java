package math.education.com.mathModel.geometry;

import math.education.com.util.CalculatorUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Random;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;

@RunWith(PowerMockRunner.class)
@PrepareForTest(CalculatorUtil.class)
public class Task21Test {

    private static final int[][] PYTHAGOREAN_TROIKA = {
            {3, 4, 5},
            {5, 12, 13},
            {8, 15, 17},
            {7, 24, 25},
            {20, 21, 29},
            {12, 35, 37},
            {9, 40, 41}
    };

    @Mock
    private Random random;

    @InjectMocks
    private Task21 task21;

    @Test
    public void test1() {
        PowerMockito.mockStatic(CalculatorUtil.class);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 1, 10)).thenReturn(2);

        for (int i = 0; i < PYTHAGOREAN_TROIKA.length; i++) {
            doReturn(i).when(random).nextInt(PYTHAGOREAN_TROIKA.length);

            task21.generateTask();

            assertEquals(String.valueOf(PYTHAGOREAN_TROIKA[i][1]), task21.getCorrectAnswer());
            assertEquals(String.valueOf(2), task21.getAnswerExample());
        }

    }

    @Test
    public void test2() {
        PowerMockito.mockStatic(CalculatorUtil.class);

        for (int i = 0; i < PYTHAGOREAN_TROIKA.length; i++) {
            PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 1, 10)).thenReturn(PYTHAGOREAN_TROIKA[i][1]).thenReturn(2);
            doReturn(i).when(random).nextInt(PYTHAGOREAN_TROIKA.length);

            task21.generateTask();

            assertEquals(String.valueOf(PYTHAGOREAN_TROIKA[i][1]), task21.getCorrectAnswer());
            assertEquals(String.valueOf(2), task21.getAnswerExample());
        }

    }

}