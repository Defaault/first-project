package math.education.com.mathModel.geometry;

import math.education.com.util.CalculatorUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Random;

import static org.junit.Assert.assertEquals;

@RunWith(PowerMockRunner.class)
@PrepareForTest(CalculatorUtil.class)
public class Task14Test {

    @Mock
    private Random random;

    @InjectMocks
    private Task14 task14;

    @Test
    public void task1() {
        PowerMockito.mockStatic(CalculatorUtil.class);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, -10, 10))
                .thenReturn(2)
                .thenReturn(2)
                .thenReturn(3)
                .thenReturn(3)
                .thenReturn(5);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 1, 10))
                .thenReturn(1);

        task14.generateTask();

        assertEquals("5", task14.getCorrectAnswer());
        assertEquals("1", task14.getAnswerExample());
    }

    @Test
    public void task2() {
        PowerMockito.mockStatic(CalculatorUtil.class);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, -10, 10))
                .thenReturn(1)
                .thenReturn(2)
                .thenReturn(3)
                .thenReturn(4)
                .thenReturn(5)
                .thenReturn(2)
                .thenReturn(2)
                .thenReturn(3)
                .thenReturn(3)
                .thenReturn(5);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 1, 10))
                .thenReturn(5)
                .thenReturn(2);

        task14.generateTask();

        assertEquals("5", task14.getCorrectAnswer());
        assertEquals("2", task14.getAnswerExample());
    }
}