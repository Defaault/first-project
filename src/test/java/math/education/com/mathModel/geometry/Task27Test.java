package math.education.com.mathModel.geometry;

import math.education.com.util.CalculatorUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Random;

import static org.junit.Assert.assertEquals;

@RunWith(PowerMockRunner.class)
@PrepareForTest(CalculatorUtil.class)
public class Task27Test {

    @Mock
    private Random random;

    @InjectMocks
    private Task27 task27;

    @Test
    public void test1() {
        PowerMockito.mockStatic(CalculatorUtil.class);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, -10, 10)).thenReturn(3);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 10, 100)).thenReturn(10);
        PowerMockito.when(CalculatorUtil.roundNumber(1.5, 1)).thenReturn(2.0);
        PowerMockito.when(CalculatorUtil.roundNumber(1, 1)).thenReturn(1.0);

        task27.generateTask();

        assertEquals("2.0", task27.getCorrectAnswer());
        assertEquals("-1.0", task27.getAnswerExample());
    }

    @Test
    public void test2() {
        PowerMockito.mockStatic(CalculatorUtil.class);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, -10, 10)).thenReturn(0).thenReturn(0).thenReturn(3);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 10, 100)).thenReturn(10);
        PowerMockito.when(CalculatorUtil.roundNumber(1.5, 1)).thenReturn(2.0);
        PowerMockito.when(CalculatorUtil.roundNumber(1, 1)).thenReturn(2.0).thenReturn(1.0);

        task27.generateTask();

        assertEquals("2.0", task27.getCorrectAnswer());
        assertEquals("-1.0", task27.getAnswerExample());
    }

}