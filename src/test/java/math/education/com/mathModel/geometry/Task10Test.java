package math.education.com.mathModel.geometry;

import math.education.com.util.CalculatorUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Random;

import static org.junit.Assert.assertEquals;

@RunWith(PowerMockRunner.class)
@PrepareForTest(CalculatorUtil.class)
public class Task10Test {

    private static final String PATH_PICTURE = "task10.png";

    @Mock
    private Random random;

    @InjectMocks
    private Task10 task10;

    @Test
    public void task1() {
        PowerMockito.mockStatic(CalculatorUtil.class);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 1, 10))
                .thenReturn(1)
                .thenReturn(2)
                .thenReturn(3);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 12, 100))
                .thenReturn(12);

        task10.generateTask();

        assertEquals(PATH_PICTURE, task10.getPathPicture());
        assertEquals(String.valueOf(1), task10.getCorrectAnswer());
        assertEquals(String.valueOf(3), task10.getAnswerExample());
    }

    @Test
    public void task2() {
        PowerMockito.mockStatic(CalculatorUtil.class);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 1, 10))
                .thenReturn(1)
                .thenReturn(2)
                .thenReturn(2)
                .thenReturn(3);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 12, 100))
                .thenReturn(13)
                .thenReturn(20);

        task10.generateTask();

        assertEquals(PATH_PICTURE, task10.getPathPicture());
        assertEquals(String.valueOf(2), task10.getCorrectAnswer());
        assertEquals(String.valueOf(3), task10.getAnswerExample());
    }
}