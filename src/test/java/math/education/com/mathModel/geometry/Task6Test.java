package math.education.com.mathModel.geometry;

import math.education.com.util.CalculatorUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Random;

import static org.junit.Assert.assertEquals;

@RunWith(PowerMockRunner.class)
@PrepareForTest(CalculatorUtil.class)
public class Task6Test {

    private static final String PATH_PICTURE = "task6.png";

    @Mock
    private Random random;

    @InjectMocks
    private Task6 task6;

    @Test
    public void test1() {
        PowerMockito.mockStatic(CalculatorUtil.class);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 1, 20))
                .thenReturn(1)
                .thenReturn(3);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 1, 100))
                .thenReturn(3)
                .thenReturn(5)
                .thenReturn(10);
        PowerMockito.when(CalculatorUtil.roundNumber(3, 1))
                .thenReturn(4.0);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 1, 10))
                .thenReturn(4)
                .thenReturn(5);

        task6.generateTask();

        assertEquals(PATH_PICTURE, task6.getPathPicture());
        assertEquals(String.valueOf(4), task6.getCorrectAnswer());
        assertEquals(String.valueOf(5), task6.getAnswerExample());
    }

    @Test
    public void test2() {
        PowerMockito.mockStatic(CalculatorUtil.class);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 1, 20))
                .thenReturn(3);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 1, 100))
                .thenReturn(10);
        PowerMockito.when(CalculatorUtil.roundNumber((double) 3, 1))
                .thenReturn(4.0);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 1, 10))
                .thenReturn(5);

        task6.generateTask();

        assertEquals(PATH_PICTURE, task6.getPathPicture());
        assertEquals(String.valueOf(4), task6.getCorrectAnswer());
        assertEquals(String.valueOf(5), task6.getAnswerExample());
    }

}