package math.education.com.mathModel.geometry;

import math.education.com.util.CalculatorUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Random;

import static org.junit.Assert.assertEquals;

@RunWith(PowerMockRunner.class)
@PrepareForTest(CalculatorUtil.class)
public class Task4Test {

    private static final String PATH_PICTURE = "task4.png";

    @Mock
    private Random random;

    @InjectMocks
    private Task4 task4;

    @Test
    public void task1() {
        PowerMockito.mockStatic(CalculatorUtil.class);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 1, 10))
                .thenReturn(2);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 2, 10))
                .thenReturn(3);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 1, 2))
                .thenReturn(1);
        PowerMockito.when(CalculatorUtil.roundNumber((5 / (double) 3), 1))
                .thenReturn(1.1);
        PowerMockito.when(CalculatorUtil.roundNumber((double) CalculatorUtil.getRandomIntFromTo(random, 10, 100) / 10, 1))
                .thenReturn(1.2);

        task4.generateTask();

        assertEquals(PATH_PICTURE, task4.getPathPicture());
        assertEquals(String.valueOf(1.1), task4.getCorrectAnswer());
        assertEquals(String.valueOf(1.2), task4.getAnswerExample());
    }

    @Test
    public void task2() {
        PowerMockito.mockStatic(CalculatorUtil.class);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 1, 10))
                .thenReturn(2);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 2, 10))
                .thenReturn(3);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 1, 2))
                .thenReturn(1);
        PowerMockito.when(CalculatorUtil.roundNumber((5 / (double) 3), 1))
                .thenReturn(1.1);
        PowerMockito.when(CalculatorUtil.roundNumber((double) CalculatorUtil.getRandomIntFromTo(random, 10, 100) / 10, 1))
                .thenReturn(1.1)
                .thenReturn(1.2);

        task4.generateTask();

        assertEquals(PATH_PICTURE, task4.getPathPicture());
        assertEquals(String.valueOf(1.1), task4.getCorrectAnswer());
        assertEquals(String.valueOf(1.2), task4.getAnswerExample());
    }

}