package math.education.com.mathModel.geometry;

import math.education.com.util.CalculatorUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Random;

import static org.junit.Assert.assertEquals;

@RunWith(PowerMockRunner.class)
@PrepareForTest(CalculatorUtil.class)
public class Task11Test {

    private static final String PATH_PICTURE = "task11.png";

    @Mock
    private Random random;

    @InjectMocks
    private Task11 task11;

    @Test
    public void test1() {
        PowerMockito.mockStatic(CalculatorUtil.class);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 1, 10)).thenReturn(1).thenReturn(2).thenReturn(3);

        task11.generateTask();

        assertEquals(String.valueOf(8), task11.getCorrectAnswer());
        assertEquals(String.valueOf(12), task11.getAnswerExample());
        assertEquals(PATH_PICTURE, task11.getPathPicture());
    }

    @Test
    public void test2() {
        PowerMockito.mockStatic(CalculatorUtil.class);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 1, 10)).thenReturn(1).thenReturn(2)
                .thenReturn(2).thenReturn(3);

        task11.generateTask();

        assertEquals(String.valueOf(8), task11.getCorrectAnswer());
        assertEquals(String.valueOf(12), task11.getAnswerExample());
        assertEquals(PATH_PICTURE, task11.getPathPicture());
    }

}