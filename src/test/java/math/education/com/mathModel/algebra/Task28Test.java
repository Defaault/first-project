package math.education.com.mathModel.algebra;

import math.education.com.util.CalculatorUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Random;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest(CalculatorUtil.class)
public class Task28Test {

    @Mock
    private Random random;

    @InjectMocks
    private Task28 task28;

    @Test
    public void test1() {
        PowerMockito.mockStatic(CalculatorUtil.class);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 1, 10)).thenReturn(9).thenReturn(10);
        doReturn(0.4).when(random).nextDouble();


        task28.generateTask();

        assertEquals(String.valueOf(9), task28.getCorrectAnswer());
        assertEquals(String.valueOf(10), task28.getAnswerExample());
    }

    @Test
    public void test2() {
        PowerMockito.mockStatic(CalculatorUtil.class);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 1, 10)).thenReturn(2);
        doReturn(2.0).when(random).nextDouble();

        task28.generateTask();

        assertEquals(String.valueOf(243), task28.getCorrectAnswer());
        assertEquals(String.valueOf(2), task28.getAnswerExample());
    }

    @Test
    public void test3() {
        PowerMockito.mockStatic(CalculatorUtil.class);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 1, 10)).thenReturn(2);
        when(random.nextDouble()).thenReturn(2.0).thenReturn(0.1);

        task28.generateTask();

        assertEquals(String.valueOf(72), task28.getCorrectAnswer());
        assertEquals(String.valueOf(2), task28.getAnswerExample());
    }
}