package math.education.com.mathModel.algebra;

import math.education.com.util.CalculatorUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Random;

import static org.junit.Assert.assertEquals;

@RunWith(PowerMockRunner.class)
@PrepareForTest(CalculatorUtil.class)
public class Task16Test {

    @Mock
    private Random random;

    @InjectMocks
    private Task16 task16;

    @Test
    public void task1() {
        PowerMockito.mockStatic(CalculatorUtil.class);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 1, 10))
                .thenReturn(1)
                .thenReturn(2)
                .thenReturn(1)
                .thenReturn(2)
                .thenReturn(3)
                .thenReturn(4);

        task16.generateTask();

        assertEquals("(x - " + 2 + ") / " + 1, task16.getCorrectAnswer());
        assertEquals("(x - " + 4 + ") / " + 3, task16.getAnswerExample());
    }

    @Test
    public void task2() {
        PowerMockito.mockStatic(CalculatorUtil.class);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 1, 10))
                .thenReturn(1)
                .thenReturn(2)
                .thenReturn(3)
                .thenReturn(4);

        task16.generateTask();

        assertEquals("(x - " + 2 + ") / " + 1, task16.getCorrectAnswer());
        assertEquals("(x - " + 4 + ") / " + 3, task16.getAnswerExample());
    }
}