package math.education.com.mathModel.algebra;

import math.education.com.util.CalculatorUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Random;

import static org.junit.Assert.assertEquals;

@RunWith(PowerMockRunner.class)
@PrepareForTest(CalculatorUtil.class)
public class Task8Test {

    @Mock
    private Random random;

    @InjectMocks
    private Task8 task8;

    @Test
    public void task1() {
        PowerMockito.mockStatic(CalculatorUtil.class);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 1, 5))
                .thenReturn(1)
                .thenReturn(4);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 2, 5))
                .thenReturn(3);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 1, 10))
                .thenReturn(4)
                .thenReturn(5);

        task8.generateTask();

        assertEquals(String.valueOf(4), task8.getCorrectAnswer());
        assertEquals(String.valueOf(5), task8.getAnswerExample());

    }

    @Test
    public void task2() {
        PowerMockito.mockStatic(CalculatorUtil.class);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 1, 5))
                .thenReturn(1)
                .thenReturn(4);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 2, 5))
                .thenReturn(3);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 1, 10))
                .thenReturn(2);

        task8.generateTask();

        assertEquals(String.valueOf(4), task8.getCorrectAnswer());
        assertEquals(String.valueOf(2), task8.getAnswerExample());

    }
}