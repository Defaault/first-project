package math.education.com.mathModel.algebra;

import math.education.com.util.CalculatorUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Random;

import static org.junit.Assert.assertEquals;

@RunWith(PowerMockRunner.class)
@PrepareForTest(CalculatorUtil.class)
public class Task3Test {

    @Mock
    private Random random;

    @InjectMocks
    private Task3 task3;

    @Test
    public void test1() {
        PowerMockito.mockStatic(CalculatorUtil.class);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 1, 10)).thenReturn(1).thenReturn(2);

        task3.generateTask();

        assertEquals(String.valueOf("1 / " + String.valueOf(1)), task3.getCorrectAnswer());
        assertEquals(String.valueOf("1 / " + String.valueOf(2)), task3.getAnswerExample());
    }

    @Test
    public void test2() {
        PowerMockito.mockStatic(CalculatorUtil.class);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 1, 10)).thenReturn(1).thenReturn(1).thenReturn(2);

        task3.generateTask();

        assertEquals(String.valueOf("1 / " + String.valueOf(1)), task3.getCorrectAnswer());
        assertEquals(String.valueOf("1 / " + String.valueOf(2)), task3.getAnswerExample());
    }

}