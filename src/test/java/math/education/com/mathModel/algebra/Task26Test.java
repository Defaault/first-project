package math.education.com.mathModel.algebra;

import math.education.com.util.CalculatorUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Random;

import static org.junit.Assert.assertEquals;

@RunWith(PowerMockRunner.class)
@PrepareForTest(CalculatorUtil.class)
public class Task26Test {

    @Mock
    private Random random;

    @InjectMocks
    private Task26 task26;

    @Test
    public void task1() {
        PowerMockito.mockStatic(CalculatorUtil.class);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 100, 200))
                .thenReturn(100);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 2, 10))
                .thenReturn(3);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 1, 2))
                .thenReturn(2);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 1, 10))
                .thenReturn(3)
                .thenReturn(5);
        PowerMockito.when(CalculatorUtil.roundNumber(1.8 - 0.6, 1))
                .thenReturn(1.2);
        PowerMockito.when(CalculatorUtil.roundNumber((double) CalculatorUtil.getRandomIntFromTo(random, 10, 100) / 10, 1))
                .thenReturn(1.3);

        task26.generateTask();

        assertEquals(String.valueOf(1.2), task26.getCorrectAnswer());
        assertEquals(String.valueOf(1.3), task26.getAnswerExample());
    }

    @Test
    public void task2() {
        PowerMockito.mockStatic(CalculatorUtil.class);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 100, 200))
                .thenReturn(100);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 2, 10))
                .thenReturn(3)
                .thenReturn(3);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 1, 2))
                .thenReturn(2)
                .thenReturn(2);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 1, 10))
                .thenReturn(3)
                .thenReturn(4)
                .thenReturn(3)
                .thenReturn(5);
        PowerMockito.when(CalculatorUtil.roundNumber(1.8 - 0.6, 1))
                .thenReturn(1.2);
        PowerMockito.when(CalculatorUtil.roundNumber((double) CalculatorUtil.getRandomIntFromTo(random, 10, 100) / 10, 1))
                .thenReturn(1.2)
                .thenReturn(1.3);

        task26.generateTask();

        assertEquals(String.valueOf(1.2), task26.getCorrectAnswer());
        assertEquals(String.valueOf(1.3), task26.getAnswerExample());
    }
}