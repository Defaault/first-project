package math.education.com.mathModel.algebra;

import math.education.com.util.CalculatorUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Random;

import static org.junit.Assert.assertEquals;

@RunWith(PowerMockRunner.class)
@PrepareForTest(CalculatorUtil.class)
public class Task25Test {

    @Mock
    private Random random;

    @InjectMocks
    private Task25 task25;

    @Test
    public void test1() {
        PowerMockito.mockStatic(CalculatorUtil.class);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 1, 10)).thenReturn(1);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 10, 100)).thenReturn(11);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 10, 99)).thenReturn(11);

        task25.generateTask();

        assertEquals("10", task25.getCorrectAnswer());
        assertEquals("11", task25.getAnswerExample());
    }

    @Test
    public void test2() {
        PowerMockito.mockStatic(CalculatorUtil.class);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 1, 10)).thenReturn(1);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 10, 100)).thenReturn(10).thenReturn(11);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 10, 99)).thenReturn(10).thenReturn(11);

        task25.generateTask();

        assertEquals("10", task25.getCorrectAnswer());
        assertEquals("11", task25.getAnswerExample());
    }
}