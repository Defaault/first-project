package math.education.com.mathModel.algebra;

import math.education.com.util.CalculatorUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Random;

import static org.junit.Assert.assertEquals;

@RunWith(PowerMockRunner.class)
@PrepareForTest(CalculatorUtil.class)
public class Task23Test {

    @Mock
    private Random random;

    @InjectMocks
    private Task23 task23;

    @Test
    public void task1() {
        PowerMockito.mockStatic(CalculatorUtil.class);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 2, 10))
                .thenReturn(10);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 100, 500))
                .thenReturn(101)
                .thenReturn(100);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 1, 10))
                .thenReturn(10)
                .thenReturn(2);

        task23.generateTask();

        assertEquals(String.valueOf(10), task23.getCorrectAnswer());
        assertEquals(String.valueOf(2), task23.getAnswerExample());
    }

    @Test
    public void task2() {
        PowerMockito.mockStatic(CalculatorUtil.class);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 2, 10))
                .thenReturn(2);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 100, 500))
                .thenReturn(100);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 1, 10))
                .thenReturn(5);

        task23.generateTask();

        assertEquals(String.valueOf(50), task23.getCorrectAnswer());
        assertEquals(String.valueOf(5), task23.getAnswerExample());
    }
}