package math.education.com.mathModel.algebra;

import math.education.com.util.CalculatorUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Random;

import static org.junit.Assert.assertEquals;

@RunWith(PowerMockRunner.class)
@PrepareForTest(CalculatorUtil.class)
public class Task13Test {

    @Mock
    private Random random;

    @InjectMocks
    private Task13 task13;

    @Test
    public void test1() {
        PowerMockito.mockStatic(CalculatorUtil.class);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 2, 10)).thenReturn(3);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 1, 3)).thenReturn(1);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 1, 10)).thenReturn(2);

        task13.generateTask();

        assertEquals(String.valueOf(9), task13.getCorrectAnswer());
        assertEquals(String.valueOf(2), task13.getAnswerExample());
    }

    @Test
    public void test2() {
        PowerMockito.mockStatic(CalculatorUtil.class);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 2, 10)).thenReturn(2);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 1, 2)).thenReturn(1);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 1, 10)).thenReturn(4).thenReturn(2);

        task13.generateTask();

        assertEquals(String.valueOf(4), task13.getCorrectAnswer());
        assertEquals(String.valueOf(2), task13.getAnswerExample());
    }


}