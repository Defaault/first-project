package math.education.com.mathModel.algebra;

import math.education.com.util.CalculatorUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Random;

import static org.junit.Assert.assertEquals;

@RunWith(PowerMockRunner.class)
@PrepareForTest(CalculatorUtil.class)
public class Task12Test {

    @Mock
    private Random random;

    @InjectMocks
    private Task12 task12;

    @Test
    public void task1() {
        PowerMockito.mockStatic(CalculatorUtil.class);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 2, 5))
                .thenReturn(2);
        PowerMockito.when((CalculatorUtil.getRandomIntFromTo(random, 1, 10)))
                .thenReturn(1)
                .thenReturn(1)
                .thenReturn(2);

        task12.generateTask();

        assertEquals("1", task12.getCorrectAnswer());
        assertEquals("2", task12.getAnswerExample());
    }

    @Test
    public void task2() {
        PowerMockito.mockStatic(CalculatorUtil.class);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 2, 5))
                .thenReturn(2);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 1, 10))
                .thenReturn(1)
                .thenReturn(2);

        task12.generateTask();

        assertEquals("1", task12.getCorrectAnswer());
        assertEquals("2", task12.getAnswerExample());
    }
}