package math.education.com.mathModel.algebra;

import math.education.com.util.CalculatorUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Random;

import static org.junit.Assert.assertEquals;

@RunWith(PowerMockRunner.class)
@PrepareForTest(CalculatorUtil.class)
public class Task19Test {

    @Mock
    private Random random;


    @InjectMocks
    private Task19 task19;

    @Test
    public void test1() {
        PowerMockito.mockStatic(CalculatorUtil.class);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 1, 10)).thenReturn(1).thenReturn(3).thenReturn(4);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 2, 20)).thenReturn(2);

        task19.generateTask();

        assertEquals(String.valueOf(2), task19.getCorrectAnswer());
        assertEquals(String.valueOf(3), task19.getAnswerExample());
    }

    @Test
    public void test2() {
        PowerMockito.mockStatic(CalculatorUtil.class);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 1, 10)).thenReturn(3).thenReturn(1).thenReturn(2).thenReturn(4).thenReturn(3);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 2, 20)).thenReturn(2);

        task19.generateTask();

        assertEquals(String.valueOf(2), task19.getCorrectAnswer());
        assertEquals(String.valueOf(4), task19.getAnswerExample());
    }

}