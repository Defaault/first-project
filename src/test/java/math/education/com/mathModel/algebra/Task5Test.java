package math.education.com.mathModel.algebra;

import math.education.com.util.CalculatorUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Random;

import static org.junit.Assert.assertEquals;

@RunWith(PowerMockRunner.class)
@PrepareForTest(CalculatorUtil.class)
public class Task5Test {

    @Mock
    private Random random;

    @InjectMocks
    private Task5 task5;

    @Test
    public void test1() {
        PowerMockito.mockStatic(CalculatorUtil.class);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 1, 10)).thenReturn(1).thenReturn(2).thenReturn(3).thenReturn(4);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 10, 100)).thenReturn(11);
        PowerMockito.when(CalculatorUtil.roundNumber((2 / (double) 3), 1)).thenReturn(1.1);
        PowerMockito.when(CalculatorUtil.roundNumber((double) 11 / 10, 1)).thenReturn(1.2);

        task5.generateTask();

        assertEquals(String.valueOf(1.1), task5.getCorrectAnswer());
        assertEquals("-" + String.valueOf(1.2), task5.getAnswerExample());
    }

    @Test
    public void test2() {
        PowerMockito.mockStatic(CalculatorUtil.class);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 1, 10)).thenReturn(1).thenReturn(2).thenReturn(3).thenReturn(4);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 10, 100)).thenReturn(11);
        PowerMockito.when(CalculatorUtil.roundNumber((2 / (double) 3), 1)).thenReturn(1.1);
        PowerMockito.when(CalculatorUtil.roundNumber((double) 11 / 10, 1)).thenReturn(1.1).thenReturn(1.2);

        task5.generateTask();

        assertEquals(String.valueOf(1.1), task5.getCorrectAnswer());
        assertEquals("-" + String.valueOf(1.2), task5.getAnswerExample());
    }

}