package math.education.com.mathModel.algebra;

import math.education.com.util.CalculatorUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Random;

import static org.junit.Assert.assertEquals;

@RunWith(PowerMockRunner.class)
@PrepareForTest(CalculatorUtil.class)
public class Task20Test {

    @Mock
    private Random random;

    @InjectMocks
    private Task20 task20;

    @Test
    public void task1() {
        PowerMockito.mockStatic(CalculatorUtil.class);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, -10, 10))
                .thenReturn(-9)
                .thenReturn(9)
                .thenReturn(1);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 1, 10))
                .thenReturn(2);

        task20.generateTask();

        assertEquals("9", task20.getCorrectAnswer());
        assertEquals("1", task20.getAnswerExample());
    }

    @Test
    public void task2() {
        PowerMockito.mockStatic(CalculatorUtil.class);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, -10, 10))
                .thenReturn(1)
                .thenReturn(2);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 1, 10))
                .thenReturn(1);

        task20.generateTask();

        assertEquals("10", task20.getCorrectAnswer());
        assertEquals("2", task20.getAnswerExample());
    }
}