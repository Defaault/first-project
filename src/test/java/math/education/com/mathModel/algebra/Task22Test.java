package math.education.com.mathModel.algebra;

import math.education.com.util.CalculatorUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Random;

import static org.junit.Assert.assertEquals;

@RunWith(PowerMockRunner.class)
@PrepareForTest(CalculatorUtil.class)
public class Task22Test {

    @Mock
    private Random random;

    @InjectMocks
    private Task22 task22;

    @Test
    public void task1() {
        PowerMockito.mockStatic(CalculatorUtil.class);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 1, 10))
                .thenReturn(1)
                .thenReturn(2)
                .thenReturn(3)
                .thenReturn(4);

        task22.generateTask();

        assertEquals(3 + " - a", task22.getCorrectAnswer());
        assertEquals(4 + " - a", task22.getAnswerExample());
    }

    @Test
    public void test2() {
        PowerMockito.mockStatic(CalculatorUtil.class);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 1, 10))
                .thenReturn(1)
                .thenReturn(2)
                .thenReturn(4);

        task22.generateTask();

        assertEquals(3 + " - a", task22.getCorrectAnswer());
        assertEquals(4 + " - a", task22.getAnswerExample());
    }

}