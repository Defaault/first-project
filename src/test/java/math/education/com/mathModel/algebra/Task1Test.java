package math.education.com.mathModel.algebra;

import math.education.com.util.CalculatorUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Random;

import static org.junit.Assert.assertEquals;

@RunWith(PowerMockRunner.class)
@PrepareForTest(CalculatorUtil.class)
public class Task1Test {

    @Mock
    private Random random;

    @InjectMocks
    private Task1 task1;

    @Test
    public void test1() {
        PowerMockito.mockStatic(CalculatorUtil.class);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 1, 10)).thenReturn(1).thenReturn(2).thenReturn(3);

        task1.generateTask();

        assertEquals("a + " + 2, task1.getCorrectAnswer());
        assertEquals("a + " + 3, task1.getAnswerExample());
    }

    @Test
    public void test2() {
        PowerMockito.mockStatic(CalculatorUtil.class);
        PowerMockito.when(CalculatorUtil.getRandomIntFromTo(random, 1, 10)).thenReturn(1).thenReturn(2).thenReturn(2).thenReturn(4);

        task1.generateTask();

        assertEquals("a + " + 2, task1.getCorrectAnswer());
        assertEquals("a + " + 4, task1.getAnswerExample());
    }

}