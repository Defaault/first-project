package math.education.com.filter;

import math.education.com.constant.RoleEnum;
import math.education.com.entity.dto.InnerSession;
import math.education.com.entity.Role;
import math.education.com.entity.User;
import math.education.com.service.SessionService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.*;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.verifyStatic;

@RunWith(PowerMockRunner.class)
@PrepareForTest(SecurityContextHolder.class)
public class SessionAuthenticationFilterTest {

    private static final String SESSION_ID = "sessionId";
    private static final String SESSION_ID_VALUE = "1";
    private static final String USER = "user";
    private static final String EMAIL = "asd@asd.asd";
    private static final int SIZE_AUTHORITIES = 1;

    @Mock
    private SessionService sessionService;

    @InjectMocks
    private SessionAuthenticationFilter sessionAuthenticationFilter;

    @Mock
    private HttpServletRequest request;

    @Mock
    private HttpServletResponse response;

    @Mock
    private FilterChain filterChain;

    @Mock
    private InnerSession innerSession;

    @Mock
    private User user;

    @Mock
    private Role role;

    @Captor
    private ArgumentCaptor<UsernamePasswordAuthenticationToken> authenticationCaptor;

    @Mock
    private SecurityContext securityContext;

    @Test
    public void nullSessionTest() throws ServletException, IOException {
        mockStatic(SecurityContextHolder.class);

        doReturn(SESSION_ID_VALUE).when(request).getParameter(SESSION_ID);
        doReturn(null).when(sessionService).getInnerSession(SESSION_ID_VALUE);

        sessionAuthenticationFilter.doFilterInternal(request, response, filterChain);

        verifyStatic(SecurityContextHolder.class, never());
        SecurityContextHolder.getContext();
        verify(filterChain).doFilter(request, response);
    }

    @Test
    public void notNullSessionAndNullUserTest() throws ServletException, IOException {
        mockStatic(SecurityContextHolder.class);

        doReturn(SESSION_ID_VALUE).when(request).getParameter(SESSION_ID);
        doReturn(innerSession).when(sessionService).getInnerSession(SESSION_ID_VALUE);

        doReturn(null).when(innerSession).getAttribute(USER);

        sessionAuthenticationFilter.doFilterInternal(request, response, filterChain);

        verifyStatic(SecurityContextHolder.class, never());
        SecurityContextHolder.getContext();
        verify(filterChain).doFilter(request, response);
    }

    @Test
    public void notNullSessionAndUserTest() throws Exception {
        mockStatic(SecurityContextHolder.class);

        doReturn(SESSION_ID_VALUE).when(request).getParameter(SESSION_ID);
        doReturn(innerSession).when(sessionService).getInnerSession(SESSION_ID_VALUE);

        doReturn(user).when(innerSession).getAttribute(USER);
        doReturn(role).when(user).getRole();
        doReturn(RoleEnum.USER).when(role).getRole();
        doReturn(EMAIL).when(user).getEmail();

        when(SecurityContextHolder.getContext()).thenReturn(securityContext);

        sessionAuthenticationFilter.doFilterInternal(request, response, filterChain);

        verify(securityContext).setAuthentication(authenticationCaptor.capture());

        UsernamePasswordAuthenticationToken actualAuthentication = authenticationCaptor.getValue();

        assertEquals(EMAIL, actualAuthentication.getPrincipal());
        assertNull(actualAuthentication.getCredentials());

        List<GrantedAuthority> authorities = (List<GrantedAuthority>) actualAuthentication.getAuthorities();

        assertEquals(SIZE_AUTHORITIES, authorities.size());

        GrantedAuthority authority = authorities.get(0);

        assertEquals(RoleEnum.USER.name(), authority.getAuthority());

        verify(filterChain).doFilter(request, response);
    }

}