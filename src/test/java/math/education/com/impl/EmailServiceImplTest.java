package math.education.com.impl;

import math.education.com.service.impl.EmailServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import static java.lang.System.lineSeparator;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class EmailServiceImplTest {

    private static final String USER_MAIL = "qwe@qwe.qwe";
    private static final String SESSION_ID = "sessionId";
    private static final String USER_NAME = "Default";

    @Mock
    private JavaMailSenderImpl javaMailSender;

    @InjectMocks
    private EmailServiceImpl emailService;

    @Captor
    private ArgumentCaptor<SimpleMailMessage> simpleMailMessageArgumentCaptor;

    @Value("${math.education.domain}")
    private String domain;

    @Test
    public void sendMessageTest() {

        emailService.sendMessage(SESSION_ID, USER_MAIL, USER_NAME);

        verify(javaMailSender).send(simpleMailMessageArgumentCaptor.capture());

        SimpleMailMessage simpleMailMessage = simpleMailMessageArgumentCaptor.getValue();

        String url = domain + "/topics?sessionId=" + SESSION_ID;

        String expectedMessage = "Hello, " +
                USER_NAME +
                lineSeparator() +
                "Thank you, for registration on our site." +
                lineSeparator() +
                "Please, confirm your E-mail click on the confirmation link below" +
                lineSeparator() +
                url +
                lineSeparator() +
                lineSeparator() +
                lineSeparator() +
                lineSeparator() +
                "Regard, Dream Team";

        assertEquals(USER_MAIL, simpleMailMessage.getTo()[0]);
        assertEquals("Authorization for math education test", simpleMailMessage.getSubject());
        assertEquals(expectedMessage, simpleMailMessage.getText());
    }
}