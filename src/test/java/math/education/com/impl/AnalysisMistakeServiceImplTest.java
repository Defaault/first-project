package math.education.com.impl;

import math.education.com.analysis.Analysis;
import math.education.com.constant.MistakeEnum;
import math.education.com.container.MistakeContainer;
import math.education.com.entity.Mistake;
import math.education.com.entity.User;
import math.education.com.entity.UserMistake;
import math.education.com.repository.MistakeRepository;
import math.education.com.repository.UserMistakeRepository;
import math.education.com.service.impl.AnalysisMistakeServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static java.util.Collections.emptyMap;
import static java.util.Collections.emptySet;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class AnalysisMistakeServiceImplTest {

    @Mock
    private MistakeContainer mistakeContainer;

    @Mock
    private UserMistakeRepository userMistakeRepository;

    @Mock
    private MistakeRepository mistakeRepository;

    @InjectMocks
    private AnalysisMistakeServiceImpl analysisMistakeService;

    @Mock
    private Analysis analysis;

    @Mock
    private User user;

    @Mock
    private UserMistake userMistake;

    @Mock
    private Mistake mistake;

    @Captor
    private ArgumentCaptor<UserMistake> userMistakeCaptor;

    @Test
    public void analysisMistakesWithEmptyMistakeContainer() {
        doReturn(emptyMap()).when(mistakeContainer).getMistakeContainer();

        analysisMistakeService.analysisMistakes(null, null, null);

        verify(userMistakeRepository, never()).save(any(UserMistake.class));
    }

    @Test
    public void analysisMistakesWithoutMistake() {
        Map<MistakeEnum, Analysis> containers = new HashMap<>();
        containers.put(MistakeEnum.MISSED_SIGN, analysis);

        doReturn(containers).when(mistakeContainer).getMistakeContainer();
        doReturn(false).when(analysis).isMistake(null, null);

        analysisMistakeService.analysisMistakes(null, null, null);
        verify(userMistakeRepository, never()).save(any(UserMistake.class));
    }

    @Test
    public void analysisMistakesWithExistsUserMistake() {
        Map<MistakeEnum, Analysis> containers = new HashMap<>();
        containers.put(MistakeEnum.MISSED_SIGN, analysis);

        Set<UserMistake> userMistakes = new HashSet<>();
        userMistakes.add(userMistake);

        doReturn(containers).when(mistakeContainer).getMistakeContainer();
        doReturn(true).when(analysis).isMistake(null, null);
        doReturn(userMistakes).when(user).getUsersMistakes();
        doReturn(mistake).when(userMistake).getMistake();
        doReturn(MistakeEnum.MISSED_SIGN).when(mistake).getMistake();
        doReturn(1).when(userMistake).getCountMistake();

        analysisMistakeService.analysisMistakes(null, null, user);

        verify(userMistake).setCountMistake(2);
        verify(userMistakeRepository).save(userMistake);
    }

    @Test
    public void analysisMistakesWithNonExistsUserMistake() {
        Map<MistakeEnum, Analysis> containers = new HashMap<>();
        containers.put(MistakeEnum.MISSED_SIGN, analysis);

        doReturn(containers).when(mistakeContainer).getMistakeContainer();
        doReturn(true).when(analysis).isMistake(null, null);
        doReturn(emptySet()).when(user).getUsersMistakes();
        doReturn(mistake).when(mistakeRepository).findByMistake(MistakeEnum.MISSED_SIGN);

        analysisMistakeService.analysisMistakes(null, null, user);

        verify(user).addUserMistake(any(UserMistake.class));
        verify(userMistakeRepository).save(userMistakeCaptor.capture());

        UserMistake newUserMistake = userMistakeCaptor.getValue();

        assertEquals(1, newUserMistake.getCountMistake());
    }

}
