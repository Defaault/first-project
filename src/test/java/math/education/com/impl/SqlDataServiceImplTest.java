package math.education.com.impl;

import math.education.com.entity.dto.SqlData;
import math.education.com.service.impl.SqlDataServiceImpl;
import math.education.com.util.KeyGenerator;
import math.education.com.util.ReadFile;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static java.lang.System.lineSeparator;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.powermock.api.mockito.PowerMockito.doReturn;
import static org.powermock.api.mockito.PowerMockito.*;
import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest(value = {KeyGenerator.class, ReadFile.class, SqlDataServiceImpl.class})
public class SqlDataServiceImplTest {

    private static final String PATH = "/application-prod.properties";
    private static final String URL = "newUrl";
    private static final String USERNAME = "newUsername";
    private static final String PASSWORD = "newPassword";
    private static final List<String> lines = new ArrayList<>(asList(
            "spring.datasource.url=oldUrl",
            "spring.datasource.username=oldUsername",
            "spring.datasource.password=oldPassword"
    ));

    @Mock
    private Random random;

    @InjectMocks
    private SqlDataServiceImpl sqlDataService;

    @Mock
    private FileWriter fileWriter;

    @Mock
    private BufferedWriter writer;

    @Test
    public void createNewDataTest() throws Exception {
        mockStatic(KeyGenerator.class);
        when(KeyGenerator.generateKey(random, 8)).thenReturn(URL).thenReturn(USERNAME);
        when(KeyGenerator.generateKey(random, 16)).thenReturn(PASSWORD);

        mockStatic(ReadFile.class);
        when(ReadFile.readFile(PATH)).thenReturn(lines);

        whenNew(FileWriter.class).withArguments("src/main/resources" + PATH).thenReturn(fileWriter);
        whenNew(BufferedWriter.class).withArguments(fileWriter).thenReturn(writer);

        String url = "spring.datasource.url=jdbc:mysql://db4free.net:3306/db_math_" + URL + "?useSSL=false&useUnicode=yes&characterEncoding=UTF-8";
        String username = "spring.datasource.username=" + "db_user_" + USERNAME;
        String password = "spring.datasource.password=" + PASSWORD;

        doReturn(writer).when(writer).append(url);
        doReturn(writer).when(writer).append(username);
        doReturn(writer).when(writer).append(password);

        SqlData newData = sqlDataService.createNewData();

        verify(writer).append(url);
        verify(writer).append(username);
        verify(writer).append(password);
        verify(writer, times(3)).append(lineSeparator());

        assertEquals("db_math_" + URL, newData.getDatabaseName());
        assertEquals("db_user_" + USERNAME, newData.getUserName());
        assertEquals(PASSWORD, newData.getPassword());
    }

    @Test
    public void emptyFileTest() throws Exception {
        mockStatic(KeyGenerator.class);
        when(KeyGenerator.generateKey(random, 8)).thenReturn(URL).thenReturn(USERNAME);
        when(KeyGenerator.generateKey(random, 16)).thenReturn(PASSWORD);

        mockStatic(ReadFile.class);
        when(ReadFile.readFile(PATH)).thenReturn(emptyList());

        whenNew(FileWriter.class).withArguments("src/main/resources" + PATH).thenReturn(fileWriter);
        whenNew(BufferedWriter.class).withArguments(fileWriter).thenReturn(writer);

        SqlData newData = sqlDataService.createNewData();

        verify(writer, never()).append(anyString());

        assertEquals("db_math_" + URL, newData.getDatabaseName());
        assertEquals("db_user_" + USERNAME, newData.getUserName());
        assertEquals(PASSWORD, newData.getPassword());
    }
}
