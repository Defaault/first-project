package math.education.com.impl;

import math.education.com.constant.PartMathematicsEnum;
import math.education.com.constant.TopicEnum;
import math.education.com.container.Container;
import math.education.com.container.TopicsContainer;
import math.education.com.mathModelFactory.Factory;
import math.education.com.service.impl.EducationServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class EducationServiceImplTest {

    @Mock
    private TopicsContainer topicsContainer;

    @InjectMocks
    private EducationServiceImpl educationServiceImpl;

    @Mock
    private Container container;

    @Mock
    private Factory factory;

    @Test
    public void getFactoriesWithFullContainersTest() {
        doReturn(container).when(topicsContainer).getContainerFactories(TopicEnum.CIRCLES);

        doReturn(1).when(container).size();

        doReturn(factory).when(container).getFactory(1);

        List<Factory> actualFactories = educationServiceImpl.getFactories(TopicEnum.CIRCLES);

        assertEquals(1, actualFactories.size());
        assertEquals(factory, actualFactories.get(0));
    }

    @Test
    public void getFactoriesWithEmptyContainersTest() {
        doReturn(container).when(topicsContainer).getContainerFactories(TopicEnum.CIRCLES);

        doReturn(0).when(container).size();

        List<Factory> actualFactories = educationServiceImpl.getFactories(TopicEnum.CIRCLES);

        verify(container, never()).getFactory(ArgumentMatchers.anyInt());
        assertEquals(0, actualFactories.size());
    }

    @Test
    public void getTopicsNamesWithPredictionTrueTest() {
        Set<TopicEnum> algebraTopics = new HashSet<>();
        Set<TopicEnum> geometryTopics = new HashSet<>();
        algebraTopics.add(TopicEnum.CIRCLES);
        geometryTopics.add(TopicEnum.CIRCLES);
        Map<PartMathematicsEnum, Set<TopicEnum>> topicsName = new HashMap<>();
        topicsName.put(PartMathematicsEnum.ALGEBRA, algebraTopics);
        topicsName.put(PartMathematicsEnum.GEOMETRY, geometryTopics);

        doReturn(topicsName).when(topicsContainer).getTopicsName();

        doReturn(container).when(topicsContainer).getContainerFactories(TopicEnum.CIRCLES);

        doReturn(0).when(container).size();

        Map<PartMathematicsEnum, Set<TopicEnum>> topicsNames = educationServiceImpl.getTopicsNames();

        assertEquals(0, topicsNames.get(PartMathematicsEnum.ALGEBRA).size());
        assertEquals(0, topicsNames.get(PartMathematicsEnum.GEOMETRY).size());
    }

    @Test
    public void getTopicsNamesWithPredictionFalseTest() {
        Set<TopicEnum> algebraTopics = new HashSet<>();
        Set<TopicEnum> geometryTopics = new HashSet<>();
        algebraTopics.add(TopicEnum.CIRCLES);
        geometryTopics.add(TopicEnum.CIRCLES);
        Map<PartMathematicsEnum, Set<TopicEnum>> topicsName = new HashMap<>();
        topicsName.put(PartMathematicsEnum.ALGEBRA, algebraTopics);
        topicsName.put(PartMathematicsEnum.GEOMETRY, geometryTopics);

        doReturn(topicsName).when(topicsContainer).getTopicsName();

        doReturn(container).when(topicsContainer).getContainerFactories(TopicEnum.CIRCLES);

        doReturn(1).when(container).size();

        Map<PartMathematicsEnum, Set<TopicEnum>> topicsNames = educationServiceImpl.getTopicsNames();

        assertEquals(1, topicsNames.get(PartMathematicsEnum.ALGEBRA).size());
        assertEquals(1, topicsNames.get(PartMathematicsEnum.GEOMETRY).size());
    }

    @Test
    public void getTopicsNamesWithEmptyTopicsTest() {
        Set<TopicEnum> algebraTopics = new HashSet<>();
        Set<TopicEnum> geometryTopics = new HashSet<>();
        Map<PartMathematicsEnum, Set<TopicEnum>> topicsName = new HashMap<>();
        topicsName.put(PartMathematicsEnum.ALGEBRA, algebraTopics);
        topicsName.put(PartMathematicsEnum.GEOMETRY, geometryTopics);

        doReturn(topicsName).when(topicsContainer).getTopicsName();

        Map<PartMathematicsEnum, Set<TopicEnum>> topicsNames = educationServiceImpl.getTopicsNames();

        verify(topicsContainer, never()).getContainerFactories(TopicEnum.CIRCLES);
        assertEquals(0, topicsNames.get(PartMathematicsEnum.ALGEBRA).size());
        assertEquals(0, topicsNames.get(PartMathematicsEnum.GEOMETRY).size());
    }

}