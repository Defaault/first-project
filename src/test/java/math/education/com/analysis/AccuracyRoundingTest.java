package math.education.com.analysis;

import math.education.com.analysis.mistake.AccuracyRounding;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class AccuracyRoundingTest {

    @InjectMocks
    private AccuracyRounding accuracyRounding;

    @Test
    public void isMistakeWithNullActualAnswer() {

        boolean mistake = accuracyRounding.isMistake(null, "1.2");

        assertFalse(mistake);
    }

    @Test
    public void isMistakeWithNullCorrectAnswer() {

        boolean mistake = accuracyRounding.isMistake("1.2", null);

        assertFalse(mistake);
    }

    @Test
    public void isMistakeWithEqualAnswers() {

        boolean mistake = accuracyRounding.isMistake("1.2", "1.2");

        assertFalse(mistake);
    }

    @Test
    public void isMistakeWithEqualScales() {

        boolean mistake = accuracyRounding.isMistake("2.4", "1.2");

        assertFalse(mistake);
    }

    @Test
    public void isMistakeWithNonMatchExpression() {

        boolean mistake = accuracyRounding.isMistake("abc", "1.2");

        assertFalse(mistake);
    }

    @Test
    public void isMistakeWithCorrectScaleBiggerThanActualScaleTrue() {

        boolean mistake = accuracyRounding.isMistake("1.2", "1.23");

        assertTrue(mistake);
    }

    @Test
    public void isMistakeWithCorrectScaleBiggerThanActualScaleFalse() {

        boolean mistake = accuracyRounding.isMistake("1.2", "1.34");

        assertFalse(mistake);
    }

    @Test
    public void isMistakeWithActualScaleBiggerThanCorrectScaleTrue() {

        boolean mistake = accuracyRounding.isMistake("1.23", "1.2");

        assertTrue(mistake);
    }

    @Test
    public void isMistakeWithActualScaleBiggerThanCorrectScaleFalse() {

        boolean mistake = accuracyRounding.isMistake("1.34", "1.2");

        assertFalse(mistake);
    }


}
