package math.education.com.analysis;

import math.education.com.analysis.mistake.MissedSign;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class MissedSignTest {

    @InjectMocks
    private MissedSign missedSign;

    @Test
    public void isMistakeWithNullActualAnswer() {

        boolean mistake = missedSign.isMistake(null, "1.2");

        assertFalse(mistake);
    }

    @Test
    public void isMistakeWithNullCorrectAnswer() {

        boolean mistake = missedSign.isMistake("1.2", null);

        assertFalse(mistake);
    }

    @Test
    public void isMistakeWithEqualAnswers() {

        boolean mistake = missedSign.isMistake("1.2", "1.2");

        assertFalse(mistake);
    }

    @Test
    public void isMistakeWithNonMatchExpression() {

        boolean mistake = missedSign.isMistake("abc", "1.2");

        assertFalse(mistake);
    }

    @Test
    public void isMistakeWithZeroActualAnswer() {

        boolean mistake = missedSign.isMistake("0", "1");

        assertFalse(mistake);
    }

    @Test
    public void isMistakeWithZeroCorrectAnswer() {

        boolean mistake = missedSign.isMistake("1", "0");

        assertFalse(mistake);
    }

    @Test
    public void isMistakePredicationFalse() {

        boolean mistake = missedSign.isMistake("1", "1");

        assertFalse(mistake);
    }

    @Test
    public void isMistakePredicationTrue() {

        boolean mistake = missedSign.isMistake("1", "-1");

        assertTrue(mistake);
    }
}
