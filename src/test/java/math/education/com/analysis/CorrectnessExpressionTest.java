package math.education.com.analysis;

import math.education.com.analysis.mistake.CorrectnessExpression;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class CorrectnessExpressionTest {

    @InjectMocks
    private CorrectnessExpression correctnessExpression;

    @Test
    public void isMistakeWithNullActualAnswer() {

        boolean mistake = correctnessExpression.isMistake(null, "1.2");

        assertFalse(mistake);
    }

    @Test
    public void isMistakeWithNullCorrectAnswer() {

        boolean mistake = correctnessExpression.isMistake("1.2", null);

        assertFalse(mistake);
    }

    @Test
    public void isMistakeWithEqualAnswers() {

        boolean mistake = correctnessExpression.isMistake("1.2", "1.2");

        assertFalse(mistake);
    }

    @Test
    public void isMistakeWithDifferentLength() {

        boolean mistake = correctnessExpression.isMistake("qwer", "qwe");

        assertFalse(mistake);
    }

    @Test
    public void isMistakeWithQuantityMistakeBiggerOne() {

        boolean mistake = correctnessExpression.isMistake("qew", "qwe");

        assertFalse(mistake);
    }

    @Test
    public void isMistakeWithQuantityMistakeEqualOne() {

        boolean mistake = correctnessExpression.isMistake("qww", "qwe");

        assertTrue(mistake);
    }
}
