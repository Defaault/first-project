package math.education.com.analysis;

import math.education.com.analysis.mistake.CorrectnessRounding;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class CorrectnessRoundingTest {

    @InjectMocks
    private CorrectnessRounding correctnessRounding;

    @Test
    public void isMistakeWithNullActualAnswer() {

        boolean mistake = correctnessRounding.isMistake(null, "1.2");

        assertFalse(mistake);
    }

    @Test
    public void isMistakeWithNullCorrectAnswer() {

        boolean mistake = correctnessRounding.isMistake("1.2", null);

        assertFalse(mistake);
    }

    @Test
    public void isMistakeWithEqualAnswers() {

        boolean mistake = correctnessRounding.isMistake("1.2", "1.2");

        assertFalse(mistake);
    }

    @Test
    public void isMistakeWithNonMatchExpression() {

        boolean mistake = correctnessRounding.isMistake("abc", "1.2");

        assertFalse(mistake);
    }

    @Test
    public void isMistakeWithNonEqualScales() {

        boolean mistake = correctnessRounding.isMistake("1.12345", "1.234");

        assertFalse(mistake);
    }

    @Test
    public void isMistakeWithDifferentEqualOne() {

        boolean mistake = correctnessRounding.isMistake("1.123", "1.124");

        assertTrue(mistake);
    }

    @Test
    public void isMistakeWithDifferentNonEqualOne() {

        boolean mistake = correctnessRounding.isMistake("1.122", "1.124");

        assertFalse(mistake);
    }
}
