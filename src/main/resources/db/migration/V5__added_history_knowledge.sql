create table history_knowledge(
    id int primary key auto_increment,
    assessment double not null,
    date_education date not null,
    id_knowledge int not null,
	foreign key(id_knowledge) references knowledge(id)
) ENGINE=InnoDB CHARSET=utf8;