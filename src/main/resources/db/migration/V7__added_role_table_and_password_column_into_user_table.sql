create table roles(
    id int primary key auto_increment,
    role varchar(255) not null
) ENGINE=InnoDB CHARSET=utf8;

insert into roles values (default, 'USER');
insert into roles values (default, 'ADMIN');

alter table users add column password varchar(255) not null after email;
alter table users add column id_role int not null after password;
alter table users add foreign key(id_role) references roles(id);
