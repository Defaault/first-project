set SQL_SAFE_UPDATES = 0;

create table users(
    id int primary key auto_increment,
    email varchar(100) not null
) ENGINE=InnoDB CHARSET=utf8;

create table topics(
    id int primary key auto_increment,
    topic varchar(255) not null
) ENGINE=InnoDB CHARSET=utf8;

create table knowledge(
    id int primary key auto_increment,
    assessment double not null,
    id_user int not null,
    id_topic int not null,
	foreign key(id_user) references users(id),
	foreign key(id_topic) references topics(id)
) ENGINE=InnoDB CHARSET=utf8;

create table tasks(
    id int primary key auto_increment,
    task text(65535) not null,
    solution text(65535) not null,
    path_picture text(65535),
    actual_answer text(65535),
    correct_answer text(65535) not null,
    id_user int not null,
    id_topic int not null,
	foreign key(id_user) references users(id),
	foreign key(id_topic) references topics(id)
) ENGINE=InnoDB CHARSET=utf8;

insert into topics values (default, 'DEPENDENCIES');
insert into topics values (default, 'DERIVATIVES');
insert into topics values (default, 'EQUATIONS');
insert into topics values (default, 'EXPRESSIONS');
insert into topics values (default, 'INTEGRALS');
insert into topics values (default, 'NUMBERS');
insert into topics values (default, 'PROBABILITY');
insert into topics values (default, 'RATIO');
insert into topics values (default, 'SEQUENCES');

insert into topics values (default, 'CIRCLES');
insert into topics values (default, 'FIGURES');
insert into topics values (default, 'GEOMETRIC_QUANTITIES');
insert into topics values (default, 'POLYGONS');
insert into topics values (default, 'POLYHEDRONS');
insert into topics values (default, 'QUADRANGLES');
insert into topics values (default, 'TRIANGLES');
insert into topics values (default, 'VECTORS');

