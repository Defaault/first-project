create table mistakes(
    id int primary key auto_increment,
    mistake varchar(255) unique not null
) ENGINE=InnoDB CHARSET=utf8;

create table users_mistakes(
    id int primary key auto_increment,
    count_mistake int,
    id_mistake int not null,
    id_user int not null,
    foreign key(id_mistake) references mistakes(id),
    foreign key(id_user) references users(id)
) ENGINE=InnoDB CHARSET=utf8;

insert into mistakes values (default, 'CORRECTNESS_ROUNDING');
insert into mistakes values (default, 'ACCURACY_ROUNDING');
insert into mistakes values (default, 'MISSED_SIGN');
insert into mistakes values (default, 'CORRECTNESS_EXPRESSION');