package math.education.com.operations.impl;

import math.education.com.operations.Calculator;

import static java.lang.Math.sin;

public class Sine implements Calculator {

    @Override
    public double calculate(double... numbers) {
        return sin(numbers[0]);
    }

}
