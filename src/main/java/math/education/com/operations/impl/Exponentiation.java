package math.education.com.operations.impl;

import math.education.com.operations.Calculator;

import static java.lang.Math.pow;

public class Exponentiation implements Calculator {

    @Override
    public double calculate(double... numbers) {
        return pow(numbers[0], numbers[1]);
    }

}
