package math.education.com.operations.impl;

import math.education.com.operations.Calculator;

import static java.lang.Math.abs;

public class Absolute implements Calculator {

    @Override
    public double calculate(double... numbers) {
        return abs(numbers[0]);
    }

}
