package math.education.com.operations.impl;

import math.education.com.operations.Calculator;

public class Subtraction implements Calculator {

    @Override
    public double calculate(double... numbers) {
        return numbers[0] - numbers[1];
    }

}
