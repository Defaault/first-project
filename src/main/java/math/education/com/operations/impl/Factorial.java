package math.education.com.operations.impl;

import math.education.com.operations.Calculator;

public class Factorial implements Calculator {

    @Override
    public double calculate(double... numbers) {
        double result = 1;

        for (int i = 1; i <= numbers[0]; i++) {
            result *= i;
        }

        return result;
    }

}
