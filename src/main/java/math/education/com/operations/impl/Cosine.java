package math.education.com.operations.impl;

import math.education.com.operations.Calculator;

import static java.lang.Math.cos;

public class Cosine implements Calculator {

    @Override
    public double calculate(double... numbers) {
        return cos(numbers[0]);
    }

}
