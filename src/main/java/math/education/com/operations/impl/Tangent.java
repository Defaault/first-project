package math.education.com.operations.impl;

import math.education.com.operations.Calculator;

import static java.lang.Math.tan;

public class Tangent implements Calculator {

    @Override
    public double calculate(double... numbers) {
        return tan(numbers[0]);
    }

}
