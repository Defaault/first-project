package math.education.com.operations.impl;

import math.education.com.operations.Calculator;

import static java.lang.Math.tan;

public class Cotangent implements Calculator {

    @Override
    public double calculate(double... numbers) {
        return 1.0 / tan(numbers[0]);
    }

}
