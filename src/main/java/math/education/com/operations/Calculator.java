package math.education.com.operations;

public interface Calculator {

    double calculate(double... numbers);

}
