package math.education.com.container.algebra;

import math.education.com.container.Container;
import math.education.com.mathModelFactory.algebra.Task9Factory;

import java.util.Random;

/**
 * Производная функции.
 */
public class DerivativesContainer extends Container {

    public DerivativesContainer(Random random) {
        containers.put(1, new Task9Factory(random));
    }

}
