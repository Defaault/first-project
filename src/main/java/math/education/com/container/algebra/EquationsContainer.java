package math.education.com.container.algebra;

import math.education.com.container.Container;
import math.education.com.mathModelFactory.algebra.*;

import java.util.Random;

/**
 * Уравнения и системы уравнений.
 */
public class EquationsContainer extends Container {

    public EquationsContainer(Random random) {
        containers.put(1, new Task2Factory(random));
        containers.put(2, new Task3Factory(random));
        containers.put(3, new Task5Factory(random));
        containers.put(4, new Task8Factory(random));
        containers.put(5, new Task18Factory(random));
        containers.put(6, new Task26Factory(random));
    }

}
