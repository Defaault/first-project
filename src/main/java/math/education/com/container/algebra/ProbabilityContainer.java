package math.education.com.container.algebra;

import math.education.com.container.Container;
import math.education.com.mathModelFactory.algebra.Task13Factory;

import java.util.Random;

/**
 * Теория вероятности.
 */
public class ProbabilityContainer extends Container {

    public ProbabilityContainer(Random random) {
        containers.put(1, new Task13Factory(random));
    }

}
