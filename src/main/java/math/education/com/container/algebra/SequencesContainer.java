package math.education.com.container.algebra;

import math.education.com.container.Container;
import math.education.com.mathModelFactory.algebra.Task12Factory;
import math.education.com.mathModelFactory.algebra.Task20Factory;

import java.util.Random;

/**
 * Числовые последовательности.
 */
public class SequencesContainer extends Container {

    public SequencesContainer(Random random) {
        containers.put(1, new Task12Factory(random));
        containers.put(2, new Task20Factory(random));
    }

}
