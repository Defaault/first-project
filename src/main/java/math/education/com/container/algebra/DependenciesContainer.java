package math.education.com.container.algebra;

import math.education.com.container.Container;
import math.education.com.mathModelFactory.algebra.Task16Factory;
import math.education.com.mathModelFactory.algebra.Task19Factory;

import java.util.Random;

/**
 * Функциональные зависимости.
 */
public class DependenciesContainer extends Container {

    public DependenciesContainer(Random random) {
        containers.put(1, new Task16Factory(random));
        containers.put(2, new Task19Factory(random));
    }

}
