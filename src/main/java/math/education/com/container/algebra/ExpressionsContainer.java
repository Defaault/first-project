package math.education.com.container.algebra;

import math.education.com.container.Container;
import math.education.com.mathModelFactory.algebra.Task1Factory;
import math.education.com.mathModelFactory.algebra.Task22Factory;
import math.education.com.mathModelFactory.algebra.Task25Factory;
import math.education.com.mathModelFactory.algebra.Task7Factory;

import java.util.Random;

/**
 * Выражения.
 */
public class ExpressionsContainer extends Container {

    public ExpressionsContainer(Random random) {
        containers.put(1, new Task1Factory(random));
        containers.put(2, new Task7Factory(random));
        containers.put(3, new Task22Factory(random));
        containers.put(4, new Task25Factory(random));
    }

}
