package math.education.com.container.algebra;

import math.education.com.container.Container;
import math.education.com.mathModelFactory.algebra.Task23Factory;

import java.util.Random;

/**
 * Отношения и пропорции. Проценты.
 */
public class RatioContainer extends Container {

    public RatioContainer(Random random) {
        containers.put(1, new Task23Factory(random));
    }

}
