package math.education.com.container.algebra;

import math.education.com.container.Container;
import math.education.com.mathModelFactory.algebra.Task15Factory;
import math.education.com.mathModelFactory.algebra.Task28Factory;

import java.util.Random;

/**
 * Первостепенная и определенный интеграл.
 */
public class IntegralsContainer extends Container {

    public IntegralsContainer(Random random) {
        containers.put(1, new Task15Factory(random));
        containers.put(2, new Task28Factory(random));
    }

}
