package math.education.com.container;

import math.education.com.constant.PartMathematicsEnum;
import math.education.com.constant.TopicEnum;
import math.education.com.container.algebra.*;
import math.education.com.container.geometry.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.*;

@Component
public class TopicsContainer {

    private Map<TopicEnum, Container> topicsContainer = new HashMap<>();
    private Map<PartMathematicsEnum, Set<TopicEnum>> topicNames = new HashMap<>();

    @Autowired
    private Random random;

    @PostConstruct
    public void init() {
        Set<TopicEnum> algebraTopicsNames = new HashSet<>();

        topicsContainer.put(TopicEnum.DEPENDENCIES, new DependenciesContainer(random));
        algebraTopicsNames.add(TopicEnum.DEPENDENCIES);
        topicsContainer.put(TopicEnum.DERIVATIVES, new DerivativesContainer(random));
        algebraTopicsNames.add(TopicEnum.DERIVATIVES);
        topicsContainer.put(TopicEnum.EQUATIONS, new EquationsContainer(random));
        algebraTopicsNames.add(TopicEnum.EQUATIONS);
        topicsContainer.put(TopicEnum.EXPRESSIONS, new ExpressionsContainer(random));
        algebraTopicsNames.add(TopicEnum.EXPRESSIONS);
        topicsContainer.put(TopicEnum.INTEGRALS, new IntegralsContainer(random));
        algebraTopicsNames.add(TopicEnum.INTEGRALS);
        topicsContainer.put(TopicEnum.NUMBERS, new NumbersContainer(random));
        algebraTopicsNames.add(TopicEnum.NUMBERS);
        topicsContainer.put(TopicEnum.PROBABILITY, new ProbabilityContainer(random));
        algebraTopicsNames.add(TopicEnum.PROBABILITY);
        topicsContainer.put(TopicEnum.RATIO, new RatioContainer(random));
        algebraTopicsNames.add(TopicEnum.RATIO);
        topicsContainer.put(TopicEnum.SEQUENCES, new SequencesContainer(random));
        algebraTopicsNames.add(TopicEnum.SEQUENCES);

        topicNames.put(PartMathematicsEnum.ALGEBRA, algebraTopicsNames);

        Set<TopicEnum> geometryTopicsNames = new HashSet<>();

        topicsContainer.put(TopicEnum.CIRCLES, new CirclesContainer(random));
        geometryTopicsNames.add(TopicEnum.CIRCLES);
        topicsContainer.put(TopicEnum.SHAPES, new ShapesContainer(random));
        geometryTopicsNames.add(TopicEnum.SHAPES);
        topicsContainer.put(TopicEnum.GEOMETRIC_QUANTITIES, new GeometricQuantitiesContainer(random));
        geometryTopicsNames.add(TopicEnum.GEOMETRIC_QUANTITIES);
        topicsContainer.put(TopicEnum.POLYGONS, new PolygonsContainer(random));
        geometryTopicsNames.add(TopicEnum.POLYGONS);
        topicsContainer.put(TopicEnum.POLYHEDRONS, new PolyhedronsContainer(random));
        geometryTopicsNames.add(TopicEnum.POLYHEDRONS);
        topicsContainer.put(TopicEnum.QUADRANGLES, new QuadranglesContainer(random));
        geometryTopicsNames.add(TopicEnum.QUADRANGLES);
        topicsContainer.put(TopicEnum.TRIANGLES, new TrianglesContainer(random));
        geometryTopicsNames.add(TopicEnum.TRIANGLES);
        topicsContainer.put(TopicEnum.VECTORS, new VectorsContainer(random));
        geometryTopicsNames.add(TopicEnum.VECTORS);

        topicNames.put(PartMathematicsEnum.GEOMETRY, geometryTopicsNames);
    }

    public Container getContainerFactories(TopicEnum topicEnum) {
        return topicsContainer.get(topicEnum);
    }

    public Map<PartMathematicsEnum, Set<TopicEnum>> getTopicsName() {
        return topicNames;
    }

}
