package math.education.com.container;

import math.education.com.analysis.Analysis;
import math.education.com.analysis.mistake.*;
import math.education.com.constant.MistakeEnum;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

@Component
public class MistakeContainer {

    private Map<MistakeEnum, Analysis> mistakeContainer = new HashMap<>();

    @PostConstruct
    public void init() {
        mistakeContainer.put(MistakeEnum.CORRECTNESS_ROUNDING, new CorrectnessRounding());
        mistakeContainer.put(MistakeEnum.ACCURACY_ROUNDING, new AccuracyRounding());
        mistakeContainer.put(MistakeEnum.MISSED_SIGN, new MissedSign());
        mistakeContainer.put(MistakeEnum.CORRECTNESS_EXPRESSION, new CorrectnessExpression());
    }

    public Map<MistakeEnum, Analysis> getMistakeContainer() {
        return mistakeContainer;
    }

}
