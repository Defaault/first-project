package math.education.com.container;

import math.education.com.entity.dto.DataSign;
import math.education.com.operations.impl.*;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.*;

import static org.hibernate.validator.internal.util.CollectionHelper.asSet;

@Component
public class OperationsContainer {

    private static final String OPEN_BRACKET = "(";
    private static final String CLOSE_BRACKET = ")";

    private Map<String, DataSign> operations;
    private Set<String> signs;

    @PostConstruct
    public void init() {
        operations = new HashMap<>();

        operations.put("+", new DataSign(1, new Addition()));
        operations.put("-", new DataSign(1, new Subtraction()));
        operations.put("/", new DataSign(2, new Division()));
        operations.put("*", new DataSign(2, new Multiplication()));
        operations.put("^", new DataSign(3, new Exponentiation()));
        operations.put("!", new DataSign(4, new Factorial()));
        operations.put("sin", new DataSign(4, new Sine()));
        operations.put("cos", new DataSign(4, new Cosine()));
        operations.put("tan", new DataSign(4, new Tangent()));
        operations.put("ctg", new DataSign(4, new Cotangent()));
        operations.put("abs", new DataSign(5, new Absolute()));

        Set<String> operationsSigns = operations.keySet();
        Set<String> bracketsSigns = asSet(OPEN_BRACKET, CLOSE_BRACKET);

        signs = new HashSet<>();
        signs.addAll(operationsSigns);
        signs.addAll(bracketsSigns);
    }

    public Set<String> getSigns() {
        return signs;
    }

    public String getOpenBracket() {
        return OPEN_BRACKET;
    }

    public String getCloseBracket() {
        return CLOSE_BRACKET;
    }

    public DataSign getDataSign(String sign) {
        return operations.get(sign);
    }

    public int getMaxPriority() {
        return operations.values()
                .stream()
                .map(DataSign::getPriority)
                .max(Comparator.naturalOrder())
                .orElse(Integer.MAX_VALUE);
    }

}
