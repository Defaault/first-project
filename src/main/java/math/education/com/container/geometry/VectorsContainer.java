package math.education.com.container.geometry;

import math.education.com.container.Container;
import math.education.com.mathModelFactory.geometry.Task14Factory;
import math.education.com.mathModelFactory.geometry.Task27Factory;

import java.util.Random;

/**
 * Вектора и координаты.
 */
public class VectorsContainer extends Container {

    public VectorsContainer(Random random) {
        containers.put(1, new Task14Factory(random));
        containers.put(2, new Task27Factory(random));
    }

}
