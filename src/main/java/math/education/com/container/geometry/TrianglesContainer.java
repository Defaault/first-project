package math.education.com.container.geometry;

import math.education.com.container.Container;
import math.education.com.mathModelFactory.geometry.Task11Factory;

import java.util.Random;

/**
 * Треугольники.
 */
public class TrianglesContainer extends Container {

    public TrianglesContainer(Random random) {
        containers.put(1, new Task11Factory(random));
    }

}
