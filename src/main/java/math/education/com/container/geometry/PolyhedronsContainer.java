package math.education.com.container.geometry;

import math.education.com.container.Container;
import math.education.com.mathModelFactory.geometry.Task21Factory;
import math.education.com.mathModelFactory.geometry.Task6Factory;

import java.util.Random;

/**
 * Многогранники.
 */
public class PolyhedronsContainer extends Container {

    public PolyhedronsContainer(Random random) {
        containers.put(1, new Task6Factory(random));
        containers.put(2, new Task21Factory(random));
    }

}
