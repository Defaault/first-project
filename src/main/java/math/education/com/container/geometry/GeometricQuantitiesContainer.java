package math.education.com.container.geometry;

import math.education.com.container.Container;
import math.education.com.mathModelFactory.geometry.Task10Factory;

import java.util.Random;

/**
 * Геометрические величины и их измерения.
 */
public class GeometricQuantitiesContainer extends Container {

    public GeometricQuantitiesContainer(Random random) {
        containers.put(1, new Task10Factory(random));
    }

}
