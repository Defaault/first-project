package math.education.com.container.geometry;

import math.education.com.container.Container;
import math.education.com.mathModelFactory.geometry.Task24Factory;

import java.util.Random;

/**
 * Четырехугольники.
 */
public class QuadranglesContainer extends Container {

    public QuadranglesContainer(Random random) {
        containers.put(1, new Task24Factory(random));
    }

}
