package math.education.com.container.geometry;

import math.education.com.container.Container;
import math.education.com.mathModelFactory.geometry.Task17Factory;
import math.education.com.mathModelFactory.geometry.Task4Factory;

import java.util.Random;

/**
 * Простые геометрические фигуры.
 */
public class ShapesContainer extends Container {

    public ShapesContainer(Random random) {
        containers.put(1, new Task4Factory(random));
        containers.put(2, new Task17Factory(random));
    }

}
