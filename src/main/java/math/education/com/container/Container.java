package math.education.com.container;

import math.education.com.mathModelFactory.Factory;

import java.util.HashMap;
import java.util.Map;

public abstract class Container {

    protected Map<Integer, Factory> containers = new HashMap<>();

    public Factory getFactory(int key) {
        return containers.get(key);
    }

    public int size() {
        return containers.size();
    }

}
