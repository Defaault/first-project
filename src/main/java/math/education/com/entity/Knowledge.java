package math.education.com.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import math.education.com.converter.BigDecimalConverter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "knowledge")
@Getter
@Setter
@NoArgsConstructor
public class Knowledge {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Convert(converter = BigDecimalConverter.class)
    private BigDecimal assessment = new BigDecimal(0.5);

    @ManyToOne
    @JoinColumn(name = "id_user", nullable = false)
    @JsonIgnoreProperties(value = {"knowledge", "tasks", "usersMistakes"})
    private User user;

    @ManyToOne
    @JoinColumn(name = "id_topic", nullable = false)
    @JsonIgnoreProperties(value = {"knowledge", "tasks"})
    private Topic topic;

    @OneToMany(mappedBy = "knowledge", fetch = FetchType.EAGER)
    private Set<HistoryKnowledge> historyKnowledge = new HashSet<>();

    @JsonIgnore
    public BigDecimal getAssessment() {
        return assessment;
    }

    @JsonIgnore
    public void setAssessment(BigDecimal assessment) {
        this.assessment = assessment;
    }

    @JsonProperty("assessment")
    public double getAssessmentDouble() {
        return assessment.setScale(2, RoundingMode.HALF_EVEN).doubleValue();
    }

    @JsonProperty("assessment")
    public void setAssessmentDouble(double assessment) {
        this.assessment = new BigDecimal(assessment);
    }

    public void addHistoryKnowledge(HistoryKnowledge historyKnowledge) {
        this.historyKnowledge.add(historyKnowledge);
        historyKnowledge.setKnowledge(this);
    }

    public void removeHistoryKnowledge(HistoryKnowledge historyKnowledge) {
        this.historyKnowledge.remove(historyKnowledge);
        historyKnowledge.setKnowledge(null);
    }

}
