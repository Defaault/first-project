package math.education.com.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import math.education.com.constant.TopicEnum;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "topics")
@Getter
@Setter
@NoArgsConstructor
public class Topic {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, length = 65535)
    private TopicEnum topic;

    @OneToMany(mappedBy = "topic", fetch = FetchType.EAGER)
    private Set<Knowledge> knowledge = new HashSet<>();

    @OneToMany(mappedBy = "topic", fetch = FetchType.EAGER)
    private Set<Task> tasks = new HashSet<>();

    public void addKnowledge(Knowledge knowledge) {
        this.knowledge.add(knowledge);
        knowledge.setTopic(this);
    }

    public void removeKnowledge(Knowledge knowledge) {
        this.knowledge.remove(knowledge);
        knowledge.setTopic(null);
    }

    public void addTask(Task task) {
        tasks.add(task);
        task.setTopic(this);
    }

    public void removeTask(Task task) {
        tasks.remove(task);
        task.setTopic(null);
    }

}


