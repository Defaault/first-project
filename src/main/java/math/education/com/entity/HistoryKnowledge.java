package math.education.com.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "history_knowledge")
@Getter
@Setter
@NoArgsConstructor
public class HistoryKnowledge {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = false)
    private double assessment;

    @Type(type = "org.hibernate.type.LocalDateType")
    @Column(name = "date_education", nullable = false)
    private LocalDate dateEducation;

    @ManyToOne
    @JoinColumn(name = "id_knowledge", nullable = false)
    @JsonIgnoreProperties(value = {"user", "topic", "historyKnowledge"})
    private Knowledge knowledge;
}
