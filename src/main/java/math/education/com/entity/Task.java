package math.education.com.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "tasks")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = false, length = 65535, columnDefinition = "mediumtext")
    private String task;

    @Column(nullable = false, length = 65535, columnDefinition = "mediumtext")
    private String solution;

    @Column(length = 65535, columnDefinition = "mediumtext")
    private String pathPicture;

    @Column(name = "actual_answer", length = 65535, columnDefinition = "mediumtext")
    private String actualAnswer;

    @Column(name = "correct_answer", length = 65535, nullable = false, columnDefinition = "mediumtext")
    private String correctAnswer;

    @Type(type = "org.hibernate.type.LocalDateType")
    @Column(name = "date_task_education")
    private LocalDate dateTaskEducation;

    @ManyToOne
    @JoinColumn(name = "id_user", nullable = false)
    @JsonIgnoreProperties(value = {"knowledge", "tasks", "usersMistakes"})
    private User user;

    @ManyToOne
    @JoinColumn(name = "id_topic", nullable = false)
    @JsonIgnoreProperties(value = {"knowledge", "tasks"})
    private Topic topic;
}
