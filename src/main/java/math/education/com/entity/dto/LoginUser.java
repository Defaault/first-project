package math.education.com.entity.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoginUser {

    private String sessionId;

    private String email;

    private String password;
}
