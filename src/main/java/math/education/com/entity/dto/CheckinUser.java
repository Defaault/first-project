package math.education.com.entity.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CheckinUser {

    private String sessionId;

    private String email;

    private String password;

    private String confirmPassword;

    private String name;

    private String captcha;

}
