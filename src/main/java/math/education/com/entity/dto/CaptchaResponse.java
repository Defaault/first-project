package math.education.com.entity.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
public class CaptchaResponse {

    private boolean success;

    @JsonProperty("challenge_ts")
    private Date timestamp;

    private String hostname;

    @JsonProperty("error-codes")
    private List<String> errorCodes;

}
