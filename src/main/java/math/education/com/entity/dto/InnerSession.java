package math.education.com.entity.dto;

import java.util.*;

import static math.education.com.util.KeyGenerator.generateKey;

public class InnerSession {

    private static final Object INNER_SESSION_MONITOR = new Object();

    private String id;
    private long lastUpdateTime;
    private Map<String, Object> attributes;

    private InnerSession(Random random, Set<String> ids) {
        id = createId(random);
        while (ids.contains(id)) {
            id = createId(random);
        }
        lastUpdateTime = new Date().getTime();
        attributes = new HashMap<>();
    }

    public static InnerSession createInnerSession(Random random, Set<String> ids) {
        return new InnerSession(random, ids);
    }

    private String createId(Random random) {
        return generateKey(random, 16);
    }

    public String getId() {
        updateLastTime();
        return id;
    }

    public long getLastUpdateTime() {
        updateLastTime();
        return lastUpdateTime;
    }

    public void setAttribute(String key, Object attribute) {
        synchronized (INNER_SESSION_MONITOR) {
            attributes.put(key, attribute);
        }
        updateLastTime();

    }

    public Object getAttribute(String key) {
        updateLastTime();
        synchronized (INNER_SESSION_MONITOR) {
            return attributes.get(key);
        }
    }

    public void removeAttribute(String key) {
        synchronized (INNER_SESSION_MONITOR) {
            attributes.remove(key);
        }
        updateLastTime();
    }

    private void updateLastTime() {
        this.lastUpdateTime = new Date().getTime();
    }

}
