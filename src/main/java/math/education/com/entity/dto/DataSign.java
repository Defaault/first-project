package math.education.com.entity.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import math.education.com.operations.Calculator;

@Getter
@AllArgsConstructor
public class DataSign {

    private int priority;
    private Calculator calculator;

}
