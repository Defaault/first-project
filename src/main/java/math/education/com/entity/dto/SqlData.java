package math.education.com.entity.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

import static java.lang.System.lineSeparator;

@Getter
@AllArgsConstructor
public class SqlData {

    private String databaseName;
    private String userName;
    private String password;

    @Override
    public String toString() {
        return "SqlData{" + lineSeparator()
                + "    databaseName: " + databaseName + "," + lineSeparator()
                + "    userName: " + userName + "," + lineSeparator()
                + "    password: " + password + lineSeparator()
                + "}";
    }
}
