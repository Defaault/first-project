package math.education.com.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import static java.lang.System.lineSeparator;

@Entity
@Table(name = "users")
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false, length = 100)
    private String email;

    @Column(nullable = false)
    private String password;

    @ManyToOne
    @JoinColumn(name = "id_role", nullable = false)
    @JsonIgnoreProperties("users")
    private Role role;

    @Builder.Default
    @OneToMany(mappedBy = "user", fetch = FetchType.EAGER)
    private Set<Knowledge> knowledge = new HashSet<>();

    @Builder.Default
    @OneToMany(mappedBy = "user", fetch = FetchType.EAGER)
    private Set<Task> tasks = new HashSet<>();

    @Builder.Default
    @OneToMany(mappedBy = "user", fetch = FetchType.EAGER)
    private Set<UserMistake> usersMistakes = new HashSet<>();

    public void addKnowledge(Knowledge knowledge) {
        this.knowledge.add(knowledge);
        knowledge.setUser(this);
    }

    public void removeKnowledge(Knowledge knowledge) {
        this.knowledge.remove(knowledge);
        knowledge.setUser(null);
    }

    public void addTask(Task task) {
        tasks.add(task);
        task.setUser(this);
    }

    public void removeTask(Task task) {
        tasks.remove(task);
        task.setUser(null);
    }

    public void addUserMistake(UserMistake userMistake) {
        usersMistakes.add(userMistake);
        userMistake.setUser(this);
    }

    public void removeUserMistake(UserMistake userMistake) {
        usersMistakes.remove(userMistake);
        userMistake.setUser(null);
    }

    @Override
    public String toString() {
        return "User{" + lineSeparator()
                + "    id: " + id + "," + lineSeparator()
                + "    email: " + email + "," + lineSeparator()
                + "    name: " + name + lineSeparator()
                + "}";
    }
}
