package math.education.com.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import math.education.com.constant.MistakeEnum;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "mistakes")
@Getter
@Setter
@NoArgsConstructor
public class Mistake {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Enumerated(EnumType.STRING)
    @Column(name = "mistake", unique = true, nullable = false)
    private MistakeEnum mistake;

    @OneToMany(mappedBy = "mistake", fetch = FetchType.EAGER)
    private Set<UserMistake> usersMistakes = new HashSet<>();

    public void addUserMistake(UserMistake userMistake) {
        usersMistakes.add(userMistake);
        userMistake.setMistake(this);
    }

    public void removeUserMistake(UserMistake userMistake) {
        usersMistakes.remove(userMistake);
        userMistake.setMistake(null);
    }
}
