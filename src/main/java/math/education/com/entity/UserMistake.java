package math.education.com.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "users_mistakes")
@Getter
@Setter
@NoArgsConstructor
public class UserMistake {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "count_mistake")
    private int countMistake;

    @ManyToOne
    @JoinColumn(name = "id_mistake", nullable = false)
    private Mistake mistake;

    @ManyToOne
    @JoinColumn(name = "id_user", nullable = false)
    @JsonIgnoreProperties(value = {"knowledge", "tasks", "usersMistakes"})
    private User user;
}
