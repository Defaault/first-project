package math.education.com.controller;

import lombok.extern.slf4j.Slf4j;
import math.education.com.constant.RoleEnum;
import math.education.com.constant.TopicEnum;
import math.education.com.entity.Role;
import math.education.com.entity.Task;
import math.education.com.entity.Topic;
import math.education.com.entity.User;
import math.education.com.entity.dto.CheckinUser;
import math.education.com.entity.dto.InnerSession;
import math.education.com.entity.dto.LoginUser;
import math.education.com.exception.ValidationException;
import math.education.com.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.util.*;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static math.education.com.builder.ResultBuilder.builder;
import static math.education.com.validator.UserValidator.validate;

@Slf4j
@RestController
@RequestMapping("/user")
public class UserController {

    private static final String USER = "user";

    @Autowired
    private SessionService sessionService;

    @Autowired
    private UserService userService;

    @Autowired
    private TaskService taskService;

    @Autowired
    private TopicService topicService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private Random random;

    @Autowired
    private CaptchaService captchaService;

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> login(@RequestBody LoginUser loginUser) {

        InnerSession session = sessionService.getInnerSession(loginUser.getSessionId());

        if (isNull(session)) {
            session = InnerSession.createInnerSession(random, sessionService.getIds());
            sessionService.addInnerSession(session);
        }

        Map<String, String> errors = validate(loginUser);

        if (errors.size() != 0) {
            log.error("User is invalid");
            throw new ValidationException("Not valid the user entity", errors);
        }

        User user = userService.getUser(loginUser.getEmail());

        if (isNull(user)) {
            log.error("User doesn't exist");
            errors.put("user", "User doesn't exist");
            throw new ValidationException("User doesn't exist", errors);
        }

        if (!passwordEncoder.matches(loginUser.getPassword(), user.getPassword())) {
            log.error("Authorization error");
            errors.put("password", "Wrong password");
            throw new ValidationException("Authorization error", errors);
        }

        session.setAttribute(USER, user);

        Map<String, Object> result = new HashMap<>();
        result.put("sessionId", session.getId());

        return ResponseEntity.ok(result);
    }

    @RequestMapping(value = "/logout", method = RequestMethod.POST)
    public ResponseEntity<?> logout(@RequestParam String sessionId) {

        InnerSession session = sessionService.getInnerSession(sessionId);

        session.removeAttribute(USER);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/checkin", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> checkin(@RequestBody CheckinUser checkinUser,
                                                       HttpServletRequest request) {

        InnerSession session = sessionService.getInnerSession(checkinUser.getSessionId());

        if (isNull(session)) {
            session = InnerSession.createInnerSession(random, sessionService.getIds());
            sessionService.addInnerSession(session);
        }

        Map<String, String> errors = validate(
                checkinUser,
                request.getRemoteAddr(),
                captchaService);

        if (errors.size() != 0) {
            log.error("User is invalid");
            throw new ValidationException("Not valid the user entity", errors);
        }

        String password = checkinUser.getPassword();

        if (nonNull(password) && !password.equals(checkinUser.getConfirmPassword())) {
            log.error("Passwords do not match.");
            errors.put("user", "Passwords do not match.");
            throw new ValidationException("Passwords do not match.", errors);
        }

        User user = userService.getUser(checkinUser.getEmail());

        if (nonNull(user)) {
            log.error("User with provided email already exist");
            errors.put("user", "User with provided email already exist");
            throw new ValidationException("User already exist", errors);
        }

        Role role = roleService.getRole(RoleEnum.USER);
        user = User.builder()
                .email(checkinUser.getEmail())
                .name(checkinUser.getName())
                .password(passwordEncoder.encode(checkinUser.getPassword()))
                .role(role)
                .build();

        userService.create(user);

        session.setAttribute(USER, user);

        Map<String, Object> result = new HashMap<>();
        result.put("sessionId", session.getId());

        return ResponseEntity.ok(result);
    }

    @RequestMapping(value = "/results", method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('USER') or hasAuthority('ADMIN')")
    public ResponseEntity<Map<String, Object>> getTasks(@RequestParam String sessionId,
                                                        @RequestParam int page,
                                                        @RequestParam(required = false, value = "topics") List<TopicEnum> topicEnums,
                                                        @RequestParam(required = false) Boolean isExistsAnswer,
                                                        @RequestParam(required = false) Boolean isNonExistsAnswer,
                                                        @RequestParam(required = false) Boolean isCorrectAnswer,
                                                        @RequestParam(required = false) Boolean isIncorrectAnswer,
                                                        @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
                                                                LocalDate fromDateTaskEducation,
                                                        @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
                                                                LocalDate toDateTaskEducation) {

        InnerSession innerSession = sessionService.getInnerSession(sessionId);
        User user = (User) innerSession.getAttribute(USER);
        int userId = user.getId();

        List<Topic> topics = getActiveTopics(topicEnums);

        Page<Task> taskPage = taskService.findAllTasksByPredicate(
                userId,
                page,
                topics,
                isExistsAnswer,
                isNonExistsAnswer,
                isCorrectAnswer,
                isIncorrectAnswer,
                fromDateTaskEducation,
                toDateTaskEducation);

        List<Topic> allTopics = topicService.getAllTopics();

        Map<String, Object> result = builder()
                .paginationData(taskPage)
                .activeTopics(topics)
                .allTopics(allTopics)
                .existsAnswer(isExistsAnswer)
                .nonExistsAnswer(isNonExistsAnswer)
                .correctAnswer(isCorrectAnswer)
                .incorrectAnswer(isIncorrectAnswer)
                .fromDateTaskEducation(fromDateTaskEducation)
                .toDateTaskEducation(toDateTaskEducation)
                .build();

        result.put(USER, user);

        return ResponseEntity.ok(result);
    }

    private List<Topic> getActiveTopics(List<TopicEnum> topicEnums) {
        return isNull(topicEnums) || topicEnums.isEmpty()
                ? new ArrayList<>()
                : topicService.getTopicsByTopicName(topicEnums);
    }

}
