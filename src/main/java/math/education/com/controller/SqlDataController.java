package math.education.com.controller;

import math.education.com.entity.dto.SqlData;
import math.education.com.service.SqlDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
public class SqlDataController {

    @Autowired
    private SqlDataService sqlDataService;

    @RequestMapping(value = "/sql", method = RequestMethod.POST)
    public ResponseEntity<SqlData> createSqlData() throws IOException {
        return new ResponseEntity<>(sqlDataService.createNewData(), HttpStatus.CREATED);
    }
}
