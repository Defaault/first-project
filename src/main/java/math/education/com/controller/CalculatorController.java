package math.education.com.controller;

import math.education.com.service.CalculatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/calculator")
public class CalculatorController {

    @Autowired
    private CalculatorService calculatorService;

    @RequestMapping(value = "/calculate", method = RequestMethod.GET)
    public ResponseEntity<Double> result(@RequestParam String formula) {
        double result = calculatorService.calculate(formula);

        return ResponseEntity.ok(result);
    }

}
