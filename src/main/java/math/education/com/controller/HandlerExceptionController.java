package math.education.com.controller;

import lombok.extern.slf4j.Slf4j;
import math.education.com.exception.RefreshException;
import math.education.com.exception.ValidationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@ControllerAdvice
public class HandlerExceptionController {

    private static final String STATUS = "status";
    private static final String MESSAGE = "message";

    @ExceptionHandler(ValidationException.class)
    public ResponseEntity<Map<String, Object>> exceptionHandler(ValidationException exception) {
        log.error(exception.getMessage());
        Map<String, Object> responseException = new HashMap<>();
        responseException.put(STATUS, HttpStatus.UNPROCESSABLE_ENTITY.value());
        responseException.put("errors", exception.getErrors());
        responseException.put(MESSAGE, exception.getMessage());
        return new ResponseEntity<>(responseException, HttpStatus.UNPROCESSABLE_ENTITY);
    }

    @ExceptionHandler(RefreshException.class)
    public ResponseEntity<Map<String, Object>> exceptionHandler(RefreshException exception) {
        log.error(exception.getMessage());
        Map<String, Object> responseException = new HashMap<>();
        responseException.put(STATUS, HttpStatus.UNPROCESSABLE_ENTITY.value());
        responseException.put("cause", exception.getCause());
        responseException.put("localizationMessage", exception.getLocalizedMessage());
        responseException.put(MESSAGE, exception.getMessage());
        responseException.put("stackTrace", exception.getStackTrace());
        responseException.put("suppressed", exception.getSuppressed());
        return new ResponseEntity<>(responseException, HttpStatus.UNPROCESSABLE_ENTITY);
    }

}
