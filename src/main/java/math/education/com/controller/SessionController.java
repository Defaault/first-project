package math.education.com.controller;

import math.education.com.entity.dto.InnerSession;
import math.education.com.service.SessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

@RestController
@RequestMapping("/session")
public class SessionController {

    @Autowired
    private SessionService sessionService;

    @Autowired
    private Random random;

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Map<String, String>> createSession() {
        InnerSession session = InnerSession.createInnerSession(random, sessionService.getIds());
        sessionService.addInnerSession(session);

        Map<String, String> response = new HashMap<>();
        response.put("sessionId", session.getId());

        return ResponseEntity.ok(response);
    }

}
