package math.education.com.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainController {

    @RequestMapping("/")
    public ResponseEntity<String> execute() {
        return ResponseEntity.ok("You are welcomed by the backend part of the site https://math-education-frontend.herokuapp.com!");
    }

}
