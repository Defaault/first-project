package math.education.com.controller;

import math.education.com.constant.TopicEnum;
import math.education.com.entity.HistoryKnowledge;
import math.education.com.entity.dto.InnerSession;
import math.education.com.entity.User;
import math.education.com.service.EducationService;
import math.education.com.service.HistoryKnowledgeService;
import math.education.com.service.SessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/analytics")
@PreAuthorize("hasAuthority('USER') or hasAuthority('ADMIN')")
public class AnalyticController {

    private static final String USER = "user";

    @Autowired
    private EducationService educationService;

    @Autowired
    private HistoryKnowledgeService historyKnowledgeService;

    @Autowired
    private SessionService sessionService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<Map<String, Object>> getHistoryKnowledge(
            @RequestParam String sessionId,
            @RequestParam TopicEnum topic,
            @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate fromDateEducation,
            @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate toDateEducation
    ) {
        Map<String, Object> data = new HashMap<>();

        InnerSession session = sessionService.getInnerSession(sessionId);
        User user = (User) session.getAttribute(USER);

        List<HistoryKnowledge> historyKnowledgeList = historyKnowledgeService
                .getHistoryKnowledge(user.getId(), topic.name(), fromDateEducation, toDateEducation);

        List<TopicEnum> topicNames = new ArrayList<>();
        educationService.getTopicsNames().values().forEach(topicNames::addAll);

        data.put("topics", topicNames);
        data.put("historyKnowledge", historyKnowledgeList);
        data.put("topic", topic.name());
        data.put("startDate", fromDateEducation);
        data.put("finishDate", toDateEducation);
        return ResponseEntity.ok(data);
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public ResponseEntity<Map<String, Object>> getHistoryKnowledge(@RequestParam String sessionId) {
        Map<String, Object> data = new HashMap<>();

        InnerSession session = sessionService.getInnerSession(sessionId);
        User user = (User) session.getAttribute(USER);

        List<TopicEnum> topicNames = new ArrayList<>();
        educationService.getTopicsNames().values().forEach(topicNames::addAll);

        data.put("topics", topicNames);
        data.put("currentKnowledge", user.getKnowledge());
        data.put("topic", "ALL");
        return ResponseEntity.ok(data);
    }

}
