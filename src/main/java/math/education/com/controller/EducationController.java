package math.education.com.controller;

import lombok.extern.slf4j.Slf4j;
import math.education.com.constant.TopicEnum;
import math.education.com.entity.*;
import math.education.com.entity.dto.InnerSession;
import math.education.com.exception.RefreshException;
import math.education.com.mathModel.Model;
import math.education.com.mathModelFactory.Factory;
import math.education.com.service.*;
import math.education.com.service.impl.AnalysisMistakeServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static java.util.stream.Collectors.toList;
import static math.education.com.util.KeyGenerator.generateKey;

@Slf4j
@RestController
@RequestMapping("/education")
@PreAuthorize("hasAuthority('USER') or hasAuthority('ADMIN')")
public class EducationController {

    private static final String INDEX_TASK = "indexTask";
    private static final String TASKS = "tasks";
    private static final String KNOWLEDGE = "knowledge";
    private static final String USER = "user";
    private static final String TOPIC = "topic";
    private static final String EMPTY = "";
    private static final String WHITE_SPACE = " ";
    private static final String TOPICS_NAME = "topicsName";
    private static final String IS_NEXT_TASK = "isNextTask";
    private static final String EDUCATION_KEY = "educationKey";

    @Autowired
    private Random random;

    @Autowired
    private KnowledgeService knowledgeService;

    @Autowired
    private TaskService taskService;

    @Autowired
    private EducationService educationService;

    @Autowired
    private TopicService topicService;

    @Autowired
    private SessionService sessionService;

    @Autowired
    private HistoryKnowledgeService historyKnowledgeService;

    @Autowired
    private AnalysisMistakeServiceImpl analysisMistakeService;

    private ExecutorService mistakesExecutorService;

    @PostConstruct
    public void init() {
        mistakesExecutorService = Executors.newFixedThreadPool(10);
    }

    @RequestMapping(value = "/topics", method = RequestMethod.GET)
    public ResponseEntity<Map<String, Object>> getTopics(@RequestParam String sessionId) {

        InnerSession session = sessionService.getInnerSession(sessionId);
        User user = (User) session.getAttribute(USER);

        Map<String, Object> data = new HashMap<>();

        data.put(KNOWLEDGE, user.getKnowledge());
        data.put(TOPICS_NAME, educationService.getTopicsNames());
        data.put("sessionId", sessionId);

        return ResponseEntity.ok(data);
    }

    @RequestMapping(value = "/test", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> startTest(@RequestParam String sessionId,
                                                         @RequestParam(TOPIC) TopicEnum topicEnum) {

        InnerSession session = sessionService.getInnerSession(sessionId);
        User user = (User) session.getAttribute(USER);
        Topic topic = topicService.getTopic(topicEnum);

        Knowledge knowledge = user.getKnowledge().stream()
                .filter(item -> item.getTopic().getTopic() == topic.getTopic())
                .findFirst()
                .orElse(null);

        if (isNull(knowledge)) {
            knowledge = new Knowledge();

            user.addKnowledge(knowledge);
            topic.addKnowledge(knowledge);
        }

        session.setAttribute(KNOWLEDGE, knowledge);

        List<Factory> factories = educationService.getFactories(topicEnum);

        Collections.shuffle(factories);

        List<Model> tasks = factories.stream()
                .map(Factory::createModel)
                .collect(toList());

        String educationKey = generateKey(random, 8);

        session.setAttribute(TOPIC, topic);
        session.setAttribute(KNOWLEDGE, knowledge);
        session.setAttribute(INDEX_TASK, 0);
        session.setAttribute(TASKS, tasks);
        session.setAttribute(EDUCATION_KEY, educationKey);

        Map<String, Object> attributes = new HashMap<>();
        attributes.put(EDUCATION_KEY, educationKey);

        return new ResponseEntity<>(attributes, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/task", method = RequestMethod.GET)
    public ResponseEntity<Map<String, Object>> getModel(@RequestParam String sessionId,
                                                        @RequestParam(required = false) String educationKey) {

        InnerSession session = sessionService.getInnerSession(sessionId);
        String actualEducationKey = (String) session.getAttribute(EDUCATION_KEY);

        verifyReloadPage(educationKey, actualEducationKey);

        List<Model> tasks = (List<Model>) session.getAttribute(TASKS);
        int indexTask = (int) session.getAttribute(INDEX_TASK);

        Model model = tasks.get(indexTask);

        Map<String, Object> attributes = new HashMap();
        attributes.put("numberTask", indexTask + 1);
        attributes.put("task", model);
        attributes.put("allQuestions", tasks.size());

        return ResponseEntity.ok(attributes);
    }

    @RequestMapping(value = "/picture", method = RequestMethod.GET)
    public void getPicture(HttpServletResponse response,
                           @RequestParam String pathPicture,
                           @RequestParam String sessionId,
                           @RequestParam(required = false) String educationKey) throws IOException {

        InnerSession session = sessionService.getInnerSession(sessionId);
        String actualEducationKey = (String) session.getAttribute(EDUCATION_KEY);

        verifyReloadPage(educationKey, actualEducationKey);

        InputStream inputStream = getClass().getResourceAsStream("/image/" + pathPicture);
        BufferedImage image = ImageIO.read(inputStream);

        response.setContentType("image/png");

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        ImageIO.write(image, "png", byteArrayOutputStream);

        byte[] bytes = byteArrayOutputStream.toByteArray();
        ServletOutputStream servletOutputStream = response.getOutputStream();

        servletOutputStream.write(bytes);

        byteArrayOutputStream.close();
        inputStream.close();

    }

    @RequestMapping(value = "/solution", method = RequestMethod.GET)
    public ResponseEntity<Map<String, Object>> getSolution(@RequestParam String sessionId,
                                                           @RequestParam(required = false) String educationKey) {


        InnerSession session = sessionService.getInnerSession(sessionId);
        String actualEducationKey = (String) session.getAttribute(EDUCATION_KEY);

        verifyReloadPage(educationKey, actualEducationKey);

        int indexTask = (int) session.getAttribute(INDEX_TASK);
        List<Model> models = (List<Model>) session.getAttribute(TASKS);

        Model task = models.get(indexTask);

        Map<String, Object> attributes = new HashMap<>();
        attributes.put("solution", task.getSolution());

        return ResponseEntity.ok(attributes);
    }

    @RequestMapping(value = "/answer", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> answer(@RequestParam String sessionId,
                                                      @RequestParam(value = "answer", required = false) String actualAnswer,
                                                      @RequestParam(required = false) String educationKey) {

        InnerSession session = sessionService.getInnerSession(sessionId);
        String actualEducationKey = (String) session.getAttribute(EDUCATION_KEY);

        verifyReloadPage(educationKey, actualEducationKey);

        int indexTask = (int) session.getAttribute(INDEX_TASK);
        List<Model> models = (List<Model>) session.getAttribute(TASKS);

        Model task = models.get(indexTask);
        task.setActualAnswer(actualAnswer);

        LocalDate dateTaskEducation = LocalDate.now();
        task.setDateTaskEducation(dateTaskEducation);

        Map<String, Object> attributes = new HashMap<>();

        if (indexTask + 1 == models.size()) {
            attributes.put(IS_NEXT_TASK, false);
        } else {
            indexTask = ++indexTask;
            session.setAttribute(INDEX_TASK, indexTask);
            attributes.put(IS_NEXT_TASK, true);
        }

        return ResponseEntity.ok(attributes);
    }

    @RequestMapping(value = "/results", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> answer(@RequestParam String sessionId,
                                                      @RequestParam(required = false) String educationKey) {

        InnerSession session = sessionService.getInnerSession(sessionId);
        String actualEducationKey = (String) session.getAttribute(EDUCATION_KEY);

        verifyReloadPage(educationKey, actualEducationKey);

        User user = (User) session.getAttribute(USER);
        Knowledge knowledge = (Knowledge) session.getAttribute(KNOWLEDGE);
        Topic topic = (Topic) session.getAttribute(TOPIC);
        List<Model> models = (List<Model>) session.getAttribute(TASKS);

        session.removeAttribute(KNOWLEDGE);
        session.removeAttribute(TASKS);
        session.removeAttribute(INDEX_TASK);
        session.removeAttribute(TOPIC);
        session.removeAttribute(EDUCATION_KEY);

        return ResponseEntity.ok(saveResult(user, knowledge, models, topic));
    }

    private void verifyReloadPage(String educationKey, String actualEducationKey) {
        if (isNull(educationKey) || !educationKey.equals(actualEducationKey)) {
            throw new RefreshException("Page with task was reloaded");
        }
    }

    private Map<String, Object> saveResult(User user, Knowledge knowledge, List<Model> models, Topic topic) {

        int[] countCorrectAnswers = {0};

        List<Task> tasks = new ArrayList<>(models.size());

        models.forEach(model -> {
            calculateAnswer(model, countCorrectAnswers, knowledge);
            saveTask(tasks, model, user, topic);
        });

        mistakesExecutorService.execute(() -> models.forEach(model -> {
            analysisMistakeService.analysisMistakes(model.getActualAnswer(), model.getCorrectAnswer(), user);
        }));

        if (knowledge.getId() == 0) {
            knowledgeService.create(knowledge);
        } else {
            knowledgeService.update(knowledge);
        }

        saveHistoryKnowledge(knowledge);

        return generateResult(countCorrectAnswers[0], tasks, user);
    }

    private void calculateAnswer(Model model, int[] countCorrectAnswers, Knowledge knowledge) {
        String actualAnswer = model.getActualAnswer();
        BigDecimal oldKnowledge = knowledge.getAssessment();
        if (nonNull(actualAnswer) && model.getCorrectAnswer().replaceAll(WHITE_SPACE, EMPTY)
                .equals(model.getActualAnswer().replaceAll(WHITE_SPACE, EMPTY))) {

            countCorrectAnswers[0] = countCorrectAnswers[0] + 1;

            BigDecimal currentKnowledge = new BigDecimal((1 - oldKnowledge.doubleValue()) / 10);
            BigDecimal newKnowledge = new BigDecimal(oldKnowledge.doubleValue() + currentKnowledge.doubleValue());

            knowledge.setAssessment(newKnowledge.doubleValue() > 1 ? new BigDecimal(1) : newKnowledge);
        } else {
            BigDecimal currentKnowledge = new BigDecimal(oldKnowledge.doubleValue() / 10);
            BigDecimal newKnowledge = new BigDecimal(oldKnowledge.doubleValue() - currentKnowledge.doubleValue());

            knowledge.setAssessment(newKnowledge.doubleValue() < 0 ? new BigDecimal(0) : newKnowledge);
        }
    }

    private void saveTask(List<Task> tasks, Model model, User user, Topic topic) {
        Task task = Task.builder()
                .task(model.getTask())
                .solution(model.getSolution())
                .actualAnswer(model.getActualAnswer())
                .correctAnswer(model.getCorrectAnswer())
                .pathPicture(model.getPathPicture())
                .dateTaskEducation(model.getDateTaskEducation())
                .build();
        user.addTask(task);
        topic.addTask(task);
        taskService.create(task);
        tasks.add(task);
    }

    private Map<String, Object> generateResult(int countCorrectAnswers, List<Task> tasks, User user) {
        Map<String, Object> attributes = new HashMap<>();
        attributes.put(USER, user);
        attributes.put("correctAnswers", countCorrectAnswers);
        attributes.put("results", tasks);
        return attributes;
    }

    private void saveHistoryKnowledge(Knowledge knowledge) {
        HistoryKnowledge historyKnowledge = new HistoryKnowledge();
        historyKnowledge.setAssessment(knowledge.getAssessmentDouble());
        historyKnowledge.setDateEducation(LocalDate.now());

        knowledge.addHistoryKnowledge(historyKnowledge);

        historyKnowledgeService.create(historyKnowledge);
    }

    @PreDestroy
    public void onDestroy() throws InterruptedException {
        log.info("Waiting for finishing all active threads. Max time is 30 seconds.");
        mistakesExecutorService.shutdown();
        mistakesExecutorService.awaitTermination(30L, TimeUnit.SECONDS);
        log.info("Finished waiting all active threads.");
    }

}
