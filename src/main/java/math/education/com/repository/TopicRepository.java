package math.education.com.repository;

import math.education.com.constant.TopicEnum;
import math.education.com.entity.Topic;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TopicRepository extends CrudRepository<Topic, Integer> {

    Topic findByTopic(TopicEnum topic);

    List<Topic> findByTopicIn(List<TopicEnum> topics);

}
