package math.education.com.repository;

import math.education.com.constant.MistakeEnum;
import math.education.com.entity.Mistake;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MistakeRepository extends CrudRepository<Mistake, Integer> {

    Mistake findByMistake(MistakeEnum mistakeEnum);
}
