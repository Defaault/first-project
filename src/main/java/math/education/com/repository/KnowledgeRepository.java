package math.education.com.repository;

import math.education.com.entity.Knowledge;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface KnowledgeRepository extends CrudRepository<Knowledge, Integer> {
}
