package math.education.com.repository;

import math.education.com.entity.HistoryKnowledge;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HistoryKnowledgeRepository extends CrudRepository<HistoryKnowledge, Integer> {

    @Query(
            nativeQuery = true,
            value = "select * from history_knowledge where id_knowledge in " +
                    "(select id from knowledge where id_user = " +
                    "(select id from users where id = :id_user) " +
                    "and id_topic = " +
                    "(select id from topics where topic = :topic)) " +
                    "and date_education >= :from_date " +
                    "and date_education <= :to_date"
    )
    List<HistoryKnowledge> getHistoryKnowledge(
            @Param("id_user") int idUser,
            @Param("topic") String topic,
            @Param("from_date") String fromDate,
            @Param("to_date") String toDate
    );

}
