package math.education.com.repository;

import math.education.com.constant.RoleEnum;
import math.education.com.entity.Role;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends CrudRepository<Role, Integer> {

    Role findByRole(RoleEnum role);

}
