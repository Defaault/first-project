package math.education.com.repository;

import math.education.com.entity.UserMistake;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserMistakeRepository extends CrudRepository<UserMistake, Integer> {
}
