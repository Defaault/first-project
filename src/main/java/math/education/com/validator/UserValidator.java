package math.education.com.validator;

import math.education.com.entity.dto.CheckinUser;
import math.education.com.entity.dto.LoginUser;
import math.education.com.service.CaptchaService;

import java.util.HashMap;
import java.util.Map;

import static java.util.Objects.isNull;
import static java.util.regex.Pattern.matches;

public class UserValidator {

    private UserValidator() {

    }

    public static Map<String, String> validate(LoginUser user) {
        Map<String, String> errors = new HashMap<>();
        if (isNull(user.getEmail()) || !matches("(.+)@(.+)\\.(.+)", user.getEmail())) {
            errors.put("email", "Email address must contain @ and .");
        }
        if (isNull(user.getPassword()) || !matches("(\\w){6,32}", user.getPassword())) {
            errors.put("password", "The length of the password must be from 6 to 32 Latin characters or numbers.");
        }
        return errors;
    }

    public static Map<String, String> validate(CheckinUser user,
                                               String clientIp,
                                               CaptchaService captchaService) {
        Map<String, String> errors = new HashMap<>();
        if (isNull(user.getEmail()) || !matches("(.+)@(.+)\\.(.+)", user.getEmail())) {
            errors.put("email", "Email address must contain @ and .");
        }
        if (isNull(user.getPassword()) || !matches("(\\w){6,32}", user.getPassword())) {
            errors.put("password", "The length of the password must be from 6 to 32 Latin characters or numbers.");
        }
        if (isNull(user.getConfirmPassword()) || !matches("(\\w){6,32}", user.getConfirmPassword())) {
            errors.put("confirmPassword", "The length of the password must be from 6 to 32 Latin characters or numbers.");
        }
        if (isNull(user.getName()) || !matches("[a-zA-Z]{1,255}", user.getName())) {
            errors.put("name", "The length of the name must be from 1 to 255 Latin characters.");
        }
        if (!captchaService.verifyCaptcha(clientIp, user.getCaptcha())) {
            errors.put("captcha", "Captcha is invalid.");
        }

        return errors;
    }

}
