package math.education.com.mathModelFactory.geometry;

import math.education.com.mathModel.Model;
import math.education.com.mathModel.geometry.Task10;
import math.education.com.mathModelFactory.Factory;

import java.util.Random;

public class Task10Factory extends Factory {

    public Task10Factory(Random random) {
        super(random);
    }

    @Override
    public Model createModel() {
        Model task10 = new Task10(random);
        task10.generateTask();
        return task10;
    }

}
