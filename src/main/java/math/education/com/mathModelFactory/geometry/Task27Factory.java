package math.education.com.mathModelFactory.geometry;

import math.education.com.mathModel.Model;
import math.education.com.mathModel.geometry.Task27;
import math.education.com.mathModelFactory.Factory;

import java.util.Random;

public class Task27Factory extends Factory {

    public Task27Factory(Random random) {
        super(random);
    }

    @Override
    public Model createModel() {
        Model task27 = new Task27(random);
        task27.generateTask();
        return task27;
    }

}
