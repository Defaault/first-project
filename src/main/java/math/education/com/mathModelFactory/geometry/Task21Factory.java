package math.education.com.mathModelFactory.geometry;

import math.education.com.mathModel.Model;
import math.education.com.mathModel.geometry.Task21;
import math.education.com.mathModelFactory.Factory;

import java.util.Random;

public class Task21Factory extends Factory {

    public Task21Factory(Random random) {
        super(random);
    }

    @Override
    public Model createModel() {
        Model task21 = new Task21(random);
        task21.generateTask();
        return task21;
    }

}
