package math.education.com.mathModelFactory.geometry;

import math.education.com.mathModel.Model;
import math.education.com.mathModel.geometry.Task17;
import math.education.com.mathModelFactory.Factory;

import java.util.Random;

public class Task17Factory extends Factory {

    public Task17Factory(Random random) {
        super(random);
    }

    @Override
    public Model createModel() {
        Model task17 = new Task17(random);
        task17.generateTask();
        return task17;
    }

}
