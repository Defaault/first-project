package math.education.com.mathModelFactory.geometry;

import math.education.com.mathModel.Model;
import math.education.com.mathModel.geometry.Task11;
import math.education.com.mathModelFactory.Factory;

import java.util.Random;

public class Task11Factory extends Factory {

    public Task11Factory(Random random) {
        super(random);
    }

    @Override
    public Model createModel() {
        Model task11 = new Task11(random);
        task11.generateTask();
        return task11;
    }

}
