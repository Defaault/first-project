package math.education.com.mathModelFactory.geometry;

import math.education.com.mathModel.Model;
import math.education.com.mathModel.geometry.Task6;
import math.education.com.mathModelFactory.Factory;

import java.util.Random;

public class Task6Factory extends Factory {

    public Task6Factory(Random random) {
        super(random);
    }

    @Override
    public Model createModel() {
        Model task6 = new Task6(random);
        task6.generateTask();
        return task6;
    }

}
