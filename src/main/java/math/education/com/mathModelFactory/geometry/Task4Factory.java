package math.education.com.mathModelFactory.geometry;

import math.education.com.mathModel.Model;
import math.education.com.mathModel.geometry.Task4;
import math.education.com.mathModelFactory.Factory;

import java.util.Random;

public class Task4Factory extends Factory {

    public Task4Factory(Random random) {
        super(random);
    }

    @Override
    public Model createModel() {
        Model task4 = new Task4(random);
        task4.generateTask();
        return task4;
    }

}
