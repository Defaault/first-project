package math.education.com.mathModelFactory.geometry;

import math.education.com.mathModel.Model;
import math.education.com.mathModel.geometry.Task14;
import math.education.com.mathModelFactory.Factory;

import java.util.Random;

public class Task14Factory extends Factory {

    public Task14Factory(Random random) {
        super(random);
    }

    @Override
    public Model createModel() {
        Model task14 = new Task14(random);
        task14.generateTask();
        return task14;
    }

}
