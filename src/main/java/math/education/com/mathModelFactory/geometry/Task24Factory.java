package math.education.com.mathModelFactory.geometry;

import math.education.com.mathModel.Model;
import math.education.com.mathModel.geometry.Task24;
import math.education.com.mathModelFactory.Factory;

import java.util.Random;

public class Task24Factory extends Factory {

    public Task24Factory(Random random) {
        super(random);
    }

    @Override
    public Model createModel() {
        Model task24 = new Task24(random);
        task24.generateTask();
        return task24;
    }

}
