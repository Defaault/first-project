package math.education.com.mathModelFactory;

import math.education.com.mathModel.Model;

import java.util.Random;

public abstract class Factory {

    protected Random random;

    public Factory(Random random) {
        this.random = random;
    }

    public abstract Model createModel();

}
