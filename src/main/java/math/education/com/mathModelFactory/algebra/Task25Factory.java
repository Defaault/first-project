package math.education.com.mathModelFactory.algebra;

import math.education.com.mathModel.Model;
import math.education.com.mathModel.algebra.Task25;
import math.education.com.mathModelFactory.Factory;

import java.util.Random;

public class Task25Factory extends Factory {

    public Task25Factory(Random random) {
        super(random);
    }

    @Override
    public Model createModel() {
        Model task25 = new Task25(random);
        task25.generateTask();
        return task25;
    }

}
