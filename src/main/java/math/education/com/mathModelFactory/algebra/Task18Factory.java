package math.education.com.mathModelFactory.algebra;

import math.education.com.mathModel.Model;
import math.education.com.mathModel.algebra.Task18;
import math.education.com.mathModelFactory.Factory;

import java.util.Random;

public class Task18Factory extends Factory {

    public Task18Factory(Random random) {
        super(random);
    }

    @Override
    public Model createModel() {
        Model task18 = new Task18(random);
        task18.generateTask();
        return task18;
    }

}
