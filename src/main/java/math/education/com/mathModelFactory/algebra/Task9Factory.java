package math.education.com.mathModelFactory.algebra;

import math.education.com.mathModel.Model;
import math.education.com.mathModel.algebra.Task9;
import math.education.com.mathModelFactory.Factory;

import java.util.Random;

public class Task9Factory extends Factory {

    public Task9Factory(Random random) {
        super(random);
    }

    @Override
    public Model createModel() {
        Model task9 = new Task9(random);
        task9.generateTask();
        return task9;
    }

}
