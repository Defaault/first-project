package math.education.com.mathModelFactory.algebra;

import math.education.com.mathModel.Model;
import math.education.com.mathModel.algebra.Task1;
import math.education.com.mathModelFactory.Factory;

import java.util.Random;

public class Task1Factory extends Factory {

    public Task1Factory(Random random) {
        super(random);
    }

    @Override
    public Model createModel() {
        Model task1 = new Task1(random);
        task1.generateTask();
        return task1;
    }

}
