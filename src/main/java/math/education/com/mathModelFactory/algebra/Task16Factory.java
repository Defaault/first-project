package math.education.com.mathModelFactory.algebra;

import math.education.com.mathModel.Model;
import math.education.com.mathModel.algebra.Task16;
import math.education.com.mathModelFactory.Factory;

import java.util.Random;

public class Task16Factory extends Factory {

    public Task16Factory(Random random) {
        super(random);
    }

    @Override
    public Model createModel() {
        Model task16 = new Task16(random);
        task16.generateTask();
        return task16;
    }

}
