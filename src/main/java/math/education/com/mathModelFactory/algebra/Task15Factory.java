package math.education.com.mathModelFactory.algebra;

import math.education.com.mathModel.Model;
import math.education.com.mathModel.algebra.Task15;
import math.education.com.mathModelFactory.Factory;

import java.util.Random;

public class Task15Factory extends Factory {

    public Task15Factory(Random random) {
        super(random);
    }

    @Override
    public Model createModel() {
        Model task15 = new Task15(random);
        task15.generateTask();
        return task15;
    }

}
