package math.education.com.mathModelFactory.algebra;

import math.education.com.mathModel.Model;
import math.education.com.mathModel.algebra.Task23;
import math.education.com.mathModelFactory.Factory;

import java.util.Random;

public class Task23Factory extends Factory {

    public Task23Factory(Random random) {
        super(random);
    }

    @Override
    public Model createModel() {
        Model task23 = new Task23(random);
        task23.generateTask();
        return task23;
    }

}
