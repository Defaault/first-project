package math.education.com.mathModelFactory.algebra;

import math.education.com.mathModel.Model;
import math.education.com.mathModel.algebra.Task22;
import math.education.com.mathModelFactory.Factory;

import java.util.Random;

public class Task22Factory extends Factory {

    public Task22Factory(Random random) {
        super(random);
    }

    @Override
    public Model createModel() {
        Model task22 = new Task22(random);
        task22.generateTask();
        return task22;
    }

}
