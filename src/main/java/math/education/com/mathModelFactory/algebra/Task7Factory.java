package math.education.com.mathModelFactory.algebra;

import math.education.com.mathModel.Model;
import math.education.com.mathModel.algebra.Task7;
import math.education.com.mathModelFactory.Factory;

import java.util.Random;

public class Task7Factory extends Factory {

    public Task7Factory(Random random) {
        super(random);
    }

    @Override
    public Model createModel() {
        Model task7 = new Task7(random);
        task7.generateTask();
        return task7;
    }

}
