package math.education.com.mathModelFactory.algebra;

import math.education.com.mathModel.Model;
import math.education.com.mathModel.algebra.Task19;
import math.education.com.mathModelFactory.Factory;

import java.util.Random;

public class Task19Factory extends Factory {

    public Task19Factory(Random random) {
        super(random);
    }

    @Override
    public Model createModel() {
        Model task19 = new Task19(random);
        task19.generateTask();
        return task19;
    }

}
