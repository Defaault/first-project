package math.education.com.mathModelFactory.algebra;

import math.education.com.mathModel.Model;
import math.education.com.mathModel.algebra.Task28;
import math.education.com.mathModelFactory.Factory;

import java.util.Random;

public class Task28Factory extends Factory {

    public Task28Factory(Random random) {
        super(random);
    }

    @Override
    public Model createModel() {
        Model task28 = new Task28(random);
        task28.generateTask();
        return task28;
    }

}
