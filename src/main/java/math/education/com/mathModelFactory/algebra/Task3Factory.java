package math.education.com.mathModelFactory.algebra;

import math.education.com.mathModel.Model;
import math.education.com.mathModel.algebra.Task3;
import math.education.com.mathModelFactory.Factory;

import java.util.Random;

public class Task3Factory extends Factory {

    public Task3Factory(Random random) {
        super(random);
    }

    @Override
    public Model createModel() {
        Model task3 = new Task3(random);
        task3.generateTask();
        return task3;
    }

}
