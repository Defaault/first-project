package math.education.com.mathModelFactory.algebra;

import math.education.com.mathModel.Model;
import math.education.com.mathModel.algebra.Task26;
import math.education.com.mathModelFactory.Factory;

import java.util.Random;

public class Task26Factory extends Factory {

    public Task26Factory(Random random) {
        super(random);
    }

    @Override
    public Model createModel() {
        Model task26 = new Task26(random);
        task26.generateTask();
        return task26;
    }

}
