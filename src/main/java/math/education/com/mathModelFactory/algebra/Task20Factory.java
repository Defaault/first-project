package math.education.com.mathModelFactory.algebra;

import math.education.com.mathModel.Model;
import math.education.com.mathModel.algebra.Task20;
import math.education.com.mathModelFactory.Factory;

import java.util.Random;

public class Task20Factory extends Factory {

    public Task20Factory(Random random) {
        super(random);
    }

    @Override
    public Model createModel() {
        Model task20 = new Task20(random);
        task20.generateTask();
        return task20;
    }

}
