package math.education.com.mathModelFactory.algebra;

import math.education.com.mathModel.Model;
import math.education.com.mathModel.algebra.Task13;
import math.education.com.mathModelFactory.Factory;

import java.util.Random;

public class Task13Factory extends Factory {

    public Task13Factory(Random random) {
        super(random);
    }

    @Override
    public Model createModel() {
        Model task13 = new Task13(random);
        task13.generateTask();
        return task13;
    }

}
