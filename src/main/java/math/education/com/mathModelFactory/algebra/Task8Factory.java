package math.education.com.mathModelFactory.algebra;

import math.education.com.mathModel.Model;
import math.education.com.mathModel.algebra.Task8;
import math.education.com.mathModelFactory.Factory;

import java.util.Random;

public class Task8Factory extends Factory {

    public Task8Factory(Random random) {
        super(random);
    }

    @Override
    public Model createModel() {
        Model task8 = new Task8(random);
        task8.generateTask();
        return task8;
    }

}
