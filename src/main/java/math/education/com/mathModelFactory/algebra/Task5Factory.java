package math.education.com.mathModelFactory.algebra;

import math.education.com.mathModel.Model;
import math.education.com.mathModel.algebra.Task5;
import math.education.com.mathModelFactory.Factory;
import java.util.Random;

public class Task5Factory extends Factory {

    public Task5Factory(Random random) {
        super(random);
    }

    @Override
    public Model createModel() {
        Model task5 = new Task5(random);
        task5.generateTask();
        return task5;
    }

}
