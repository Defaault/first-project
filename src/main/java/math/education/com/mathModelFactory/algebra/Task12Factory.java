package math.education.com.mathModelFactory.algebra;

import math.education.com.mathModel.Model;
import math.education.com.mathModel.algebra.Task12;
import math.education.com.mathModelFactory.Factory;

import java.util.Random;

public class Task12Factory extends Factory {

    public Task12Factory(Random random) {
        super(random);
    }

    @Override
    public Model createModel() {
        Model task12 = new Task12(random);
        task12.generateTask();
        return task12;
    }

}
