package math.education.com.mathModelFactory.algebra;

import math.education.com.mathModel.Model;
import math.education.com.mathModel.algebra.Task2;
import math.education.com.mathModelFactory.Factory;

import java.util.Random;

public class Task2Factory extends Factory {

    public Task2Factory(Random random) {
        super(random);
    }

    @Override
    public Model createModel() {
        Model task2 = new Task2(random);
        task2.generateTask();
        return task2;
    }

}
