package math.education.com.constant;

public enum MistakeEnum {

    CORRECTNESS_ROUNDING, ACCURACY_ROUNDING, MISSED_SIGN, CORRECTNESS_EXPRESSION

}
