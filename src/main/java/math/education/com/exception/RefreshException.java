package math.education.com.exception;

public class RefreshException extends RuntimeException {

    public RefreshException(String message) {
        super(message);
    }

}
