package math.education.com.parser;

import math.education.com.container.OperationsContainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static java.util.Arrays.asList;

@Component
public class FormulaParser {

    @Autowired
    private OperationsContainer operationsContainer;

    private String signsRegexp;

    @PostConstruct
    public void init() {
        StringBuilder regexpBuilder = new StringBuilder();

        Set<String> signs = operationsContainer.getSigns();
        signs.forEach(sign -> {
            String shielding = sign.length() > 1 ? "" : "\\";

            regexpBuilder
                    .append("(?<=")
                    .append(shielding)
                    .append(sign)
                    .append(")|(?=")
                    .append(shielding)
                    .append(sign)
                    .append(")|");
        });
        regexpBuilder.deleteCharAt(regexpBuilder.length() - 1);

        signsRegexp = regexpBuilder.toString();
    }

    public List<String> parse(String formula) {
        String[] elements = formula.split(signsRegexp);

        return new ArrayList<>(asList(elements));
    }

}
