package math.education.com.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.Random;

@Configuration
@EnableScheduling
public class ApplicationConfig {

    @Bean
    public Random random() {
        return new Random();
    }

    @Bean
    public ObjectMapper mapper() {
        return new ObjectMapper();
    }

}
