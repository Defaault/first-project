package math.education.com.aop;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;

import static java.util.Objects.nonNull;
import static java.util.stream.Collectors.joining;

@Slf4j
@Aspect
@Component
public class LoggerAspect {

    private static final String DELIMITER = ", ";
    private static final String PREFIX = "[";
    private static final String SUFFIX = "]";

    @Autowired
    private HttpServletRequest request;

    @Before("@annotation(org.springframework.web.bind.annotation.RequestMapping)")
    public void beforeLogger(JoinPoint joinPoint) {

        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
        RequestMapping requestMappingAnnotation = methodSignature
                .getMethod()
                .getAnnotation(RequestMapping.class);

        String requestLog = new LoggerBuilder("Request")
                .buildControllerName(methodSignature)
                .buildMethodsName(requestMappingAnnotation)
                .buildMappingsName(requestMappingAnnotation)
                .buildCurrentUri()
                .buildArguments(methodSignature, joinPoint)
                .build();

        log.info(requestLog);
    }

    @AfterReturning(value = "@annotation(org.springframework.web.bind.annotation.RequestMapping)", returning = "result")
    public void afterLogger(JoinPoint joinPoint, Object result) {

        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
        RequestMapping requestMappingAnnotation = methodSignature
                .getMethod()
                .getAnnotation(RequestMapping.class);
        ResponseEntity responseEntity = (ResponseEntity) result;

        String responseLog = new LoggerBuilder("Response")
                .buildControllerName(methodSignature)
                .buildMethodsName(requestMappingAnnotation)
                .buildMappingsName(requestMappingAnnotation)
                .buildCurrentUri()
                .buildBody(responseEntity)
                .buildStatus(responseEntity)
                .build();

        log.info(responseLog);

    }

    class LoggerBuilder {

        private StringBuilder builder;

        LoggerBuilder(String initial) {
            builder = new StringBuilder(initial).append(": [");
        }

        LoggerBuilder buildControllerName(MethodSignature methodSignature) {
            builder.append("controller: ")
                    .append(methodSignature.getDeclaringType().getSimpleName())
                    .append(DELIMITER);

            return this;
        }

        LoggerBuilder buildMethodsName(RequestMapping requestMappingAnnotation) {
            builder.append("methods: ")
                    .append(Arrays.stream(requestMappingAnnotation.method())
                            .map(Enum::name)
                            .collect(joining(DELIMITER, PREFIX, SUFFIX)))
                    .append(DELIMITER);

            return this;
        }

        LoggerBuilder buildMappingsName(RequestMapping requestMappingAnnotation) {
            builder.append("mappings: ")
                    .append(Arrays.stream(requestMappingAnnotation.value())
                            .collect(joining(DELIMITER, PREFIX, SUFFIX)))
                    .append(DELIMITER);

            return this;
        }

        LoggerBuilder buildCurrentUri() {
            builder.append("current uri: [")
                    .append(request.getRequestURI())
                    .append("], ");

            return this;
        }

        LoggerBuilder buildArguments(MethodSignature methodSignature, JoinPoint joinPoint) {
            String[] namesArguments = methodSignature.getParameterNames();
            Object[] arguments = joinPoint.getArgs();

            StringBuilder builder = new StringBuilder("arguments: [");

            for (int i = 0; i < namesArguments.length; i++) {
                builder.append(namesArguments[i])
                        .append(": ")
                        .append(nonNull(arguments[i]) ? arguments[i].toString() : null)
                        .append(DELIMITER);
            }

            if (namesArguments.length != 0) {
                builder.delete(builder.length() - 2, builder.length());
            }

            builder.append(SUFFIX);

            this.builder.append(builder.toString())
                    .append(DELIMITER);

            return this;
        }

        LoggerBuilder buildBody(ResponseEntity responseEntity) {
            builder.append("body: ")
                    .append(nonNull(responseEntity) ? responseEntity.getBody() : null)
                    .append(DELIMITER);

            return this;
        }

        LoggerBuilder buildStatus(ResponseEntity responseEntity) {
            builder.append("status: ")
                    .append(nonNull(responseEntity) ? responseEntity.getStatusCode() : null)
                    .append(DELIMITER);

            return this;
        }

        String build() {
            if (DELIMITER.equals(builder.substring(builder.length() - 2, builder.length()))) {
                builder.delete(builder.length() - 2, builder.length());
            }

            return builder.append(SUFFIX).toString();
        }

    }

}
