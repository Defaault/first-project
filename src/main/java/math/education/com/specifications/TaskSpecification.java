package math.education.com.specifications;

import math.education.com.entity.Task;
import math.education.com.entity.Topic;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

public class TaskSpecification implements Specification<Task> {

    private static final String ID = "id";
    private static final String ACTUAL_ANSWER = "actualAnswer";
    private static final String CORRECT_ANSWER = "correctAnswer";
    private static final String DATE_TASK_EDUCATION = "dateTaskEducation";
    private static final String REPLACE = "replace";
    private static final String WHITE_SPACE = " ";
    private static final String EMPTY = "";

    private int userId;
    private List<Topic> topics;
    private Boolean isExistsAnswer;
    private Boolean isNonExistsAnswer;
    private Boolean isCorrectAnswer;
    private Boolean isIncorrectAnswer;
    private LocalDate fromDateTaskEducation;
    private LocalDate toDateTaskEducation;

    public TaskSpecification(int userId,
                             List<Topic> topics,
                             Boolean isExistsAnswer,
                             Boolean isNonExistsAnswer,
                             Boolean isCorrectAnswer,
                             Boolean isIncorrectAnswer,
                             LocalDate fromDateTaskEducation,
                             LocalDate toDateTaskEducation) {
        this.userId = userId;
        this.topics = topics;
        this.isExistsAnswer = isExistsAnswer;
        this.isNonExistsAnswer = isNonExistsAnswer;
        this.isCorrectAnswer = isCorrectAnswer;
        this.isIncorrectAnswer = isIncorrectAnswer;
        this.fromDateTaskEducation = fromDateTaskEducation;
        this.toDateTaskEducation = toDateTaskEducation;
    }

    @Override
    public Predicate toPredicate(Root<Task> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        return new TaskPredicateBuilder(criteriaBuilder, root)
                .user(userId)
                .topics(topics)
                .existsAnswer(isExistsAnswer, isNonExistsAnswer)
                .correctAnswer(isCorrectAnswer, isIncorrectAnswer)
                .timeTaskEducation(fromDateTaskEducation, toDateTaskEducation)
                .build();
    }

    class TaskPredicateBuilder {

        private List<Predicate> predicates;
        private CriteriaBuilder criteriaBuilder;
        private Root<Task> root;

        TaskPredicateBuilder(CriteriaBuilder criteriaBuilder, Root<Task> root) {
            this.predicates = new ArrayList<>();
            this.criteriaBuilder = criteriaBuilder;
            this.root = root;
        }

        TaskPredicateBuilder user(int id) {
            predicates.add(criteriaBuilder.equal(root.get("user").get(ID), id));

            return this;
        }

        TaskPredicateBuilder topics(List<Topic> topics) {

            if (nonNull(topics) && !topics.isEmpty()) {
                Predicate[] topicPredicates = new Predicate[topics.size()];

                for (int i = 0; i < topics.size(); i++) {
                    topicPredicates[i] = criteriaBuilder.equal(root.get("topic").get(ID), topics.get(i).getId());
                }

                predicates.add(criteriaBuilder.or(topicPredicates));
            }

            return this;
        }

        TaskPredicateBuilder existsAnswer(Boolean isExistsAnswer, Boolean isNonExistsAnswer) {

            if (nonNull(isExistsAnswer) && isExistsAnswer &&
                    (isNull(isNonExistsAnswer) || !isNonExistsAnswer)) {
                predicates.add(criteriaBuilder.isNotNull(root.get(ACTUAL_ANSWER)));
            }

            if (nonNull(isNonExistsAnswer) && isNonExistsAnswer &&
                    (isNull(isExistsAnswer) || !isExistsAnswer)) {
                predicates.add(criteriaBuilder.isNull(root.get(ACTUAL_ANSWER)));
            }

            return this;
        }

        TaskPredicateBuilder correctAnswer(Boolean isCorrectAnswer, Boolean isIncorrectAnswer) {
            Expression<String> replaceActualAnswerFunction = criteriaBuilder.function(REPLACE, String.class, root.get(ACTUAL_ANSWER),
                    criteriaBuilder.literal(WHITE_SPACE), criteriaBuilder.literal(EMPTY));

            Expression<String> replaceCorrectAnswerFunction = criteriaBuilder.function(REPLACE, String.class, root.get(CORRECT_ANSWER),
                    criteriaBuilder.literal(WHITE_SPACE), criteriaBuilder.literal(EMPTY));

            if (nonNull(isCorrectAnswer) && isCorrectAnswer &&
                    (isNull(isIncorrectAnswer) || !isIncorrectAnswer)) {
                predicates.add(criteriaBuilder.equal(replaceActualAnswerFunction, replaceCorrectAnswerFunction));
            }

            if (nonNull(isIncorrectAnswer) && isIncorrectAnswer &&
                    (isNull(isCorrectAnswer) || !isCorrectAnswer)) {
                Predicate[] correctionPredicates = new Predicate[2];
                correctionPredicates[0] = criteriaBuilder.notEqual(replaceActualAnswerFunction, replaceCorrectAnswerFunction);
                correctionPredicates[1] = criteriaBuilder.isNull(root.get(ACTUAL_ANSWER));
                predicates.add(criteriaBuilder.or(correctionPredicates));
            }

            return this;
        }

        TaskPredicateBuilder timeTaskEducation(LocalDate fromDateTaskEducation, LocalDate toDateTaskEducation) {

            if (nonNull(fromDateTaskEducation)) {
                predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get(DATE_TASK_EDUCATION), criteriaBuilder.literal(fromDateTaskEducation)));
            }

            if (nonNull(toDateTaskEducation)) {
                predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get(DATE_TASK_EDUCATION), criteriaBuilder.literal(toDateTaskEducation)));
            }

            return this;
        }

        @SuppressWarnings("all")
        Predicate build() {
            return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
        }

    }

}
