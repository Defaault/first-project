package math.education.com.service;

import math.education.com.entity.User;

public interface UserService {

    User create(User user);

    User getUser(String email);

}
