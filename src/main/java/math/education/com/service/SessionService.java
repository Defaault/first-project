package math.education.com.service;

import math.education.com.entity.dto.InnerSession;

import java.util.Set;

public interface SessionService {

    InnerSession getInnerSession(String key);

    void addInnerSession(InnerSession innerSession);

    Set<String> getIds();

    void deleteInnerSession(String key);

}
