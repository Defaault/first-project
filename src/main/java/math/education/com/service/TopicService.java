package math.education.com.service;


import math.education.com.constant.TopicEnum;
import math.education.com.entity.Topic;

import java.util.List;

public interface TopicService {

    Topic getTopic(TopicEnum topic);

    List<Topic> getTopicsByTopicName(List<TopicEnum> topicEnums);

    List<Topic> getAllTopics();

}
