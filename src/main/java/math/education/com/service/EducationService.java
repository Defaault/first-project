package math.education.com.service;

import math.education.com.constant.PartMathematicsEnum;
import math.education.com.constant.TopicEnum;
import math.education.com.mathModelFactory.Factory;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface EducationService {

    List<Factory> getFactories(TopicEnum topic);

    Map<PartMathematicsEnum, Set<TopicEnum>> getTopicsNames();

}
