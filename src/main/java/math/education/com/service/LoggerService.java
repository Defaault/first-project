package math.education.com.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

import static java.util.Objects.nonNull;

@Slf4j
@Service
public class LoggerService {

    private static final String SRC = "src";
    private static final String[] PATH_LOG_FILE = {"main", "resources", "logging.log"};

    private File logFile;

    @PostConstruct
    public void init() {
        Path path = Paths.get(SRC, PATH_LOG_FILE);
        logFile = new File(path.toUri().getPath());

        if (logFile.exists()) {
            log.info("Log logFile " + logFile.getPath() + " was created");
        }

    }

    @SuppressWarnings("all")
    @PreDestroy
    public void onDestroy() {

        if (nonNull(logFile) && logFile.exists()) {
            logFile.delete();
            log.info("Log logFile " + logFile.getPath() + " was removed");
        }

    }
}
