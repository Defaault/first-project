package math.education.com.service;

import math.education.com.entity.HistoryKnowledge;

import java.time.LocalDate;
import java.util.List;

public interface HistoryKnowledgeService {

    HistoryKnowledge create(HistoryKnowledge historyKnowledge);

    List<HistoryKnowledge> getHistoryKnowledge(int idUser, String topic, LocalDate fromDate, LocalDate toDate);

}
