package math.education.com.service;

import math.education.com.entity.Task;
import math.education.com.entity.Topic;
import org.springframework.data.domain.Page;

import java.time.LocalDate;
import java.util.List;

public interface TaskService {

    Task create(Task task);

    Page<Task> findAllTasksByPredicate(int userId,
                                       int page,
                                       List<Topic> topic,
                                       Boolean isExistsAnswer,
                                       Boolean isNonExistsAnswer,
                                       Boolean isCorrectAnswer,
                                       Boolean isIncorrectAnswer,
                                       LocalDate fromDateTaskEducation,
                                       LocalDate toDateTaskEducation);

}
