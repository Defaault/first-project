package math.education.com.service;

import math.education.com.entity.Knowledge;

public interface KnowledgeService {

    Knowledge create(Knowledge knowledge);

    Knowledge update(Knowledge knowledge);

}
