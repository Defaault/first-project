package math.education.com.service;

public interface CalculatorService {

    double calculate(String formula);

}
