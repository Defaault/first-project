package math.education.com.service;

import math.education.com.constant.RoleEnum;
import math.education.com.entity.Role;

public interface RoleService {

    Role getRole(RoleEnum role);

}
