package math.education.com.service;

import math.education.com.entity.User;

public interface AnalysisMistakeService {

    void analysisMistakes(String actualAnswer, String correctAnswer, User user);
}
