package math.education.com.service;

import math.education.com.entity.dto.SqlData;

import java.io.IOException;

public interface SqlDataService {

    SqlData createNewData() throws IOException;
}
