package math.education.com.service;

public interface EmailService {

    void sendMessage(String sessionId, String userMail, String userName);
}
