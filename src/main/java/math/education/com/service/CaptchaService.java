package math.education.com.service;

public interface CaptchaService {

    boolean verifyCaptcha(String ip, String captchaResponse);

}
