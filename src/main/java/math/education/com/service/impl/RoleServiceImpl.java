package math.education.com.service.impl;

import math.education.com.constant.RoleEnum;
import math.education.com.entity.Role;
import math.education.com.repository.RoleRepository;
import math.education.com.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleRepository roleRepository;

    @Override
    public Role getRole(RoleEnum role) {
        return roleRepository.findByRole(role);
    }

}
