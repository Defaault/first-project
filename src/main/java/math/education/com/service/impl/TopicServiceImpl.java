package math.education.com.service.impl;

import math.education.com.constant.TopicEnum;
import math.education.com.entity.Topic;
import math.education.com.repository.TopicRepository;
import math.education.com.service.TopicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TopicServiceImpl implements TopicService {

    @Autowired
    private TopicRepository topicRepository;

    @Override
    public Topic getTopic(TopicEnum topic) {
        return topicRepository.findByTopic(topic);
    }

    @Override
    public List<Topic> getTopicsByTopicName(List<TopicEnum> topicEnums) {
        return topicRepository.findByTopicIn(topicEnums);
    }

    @Override
    public List<Topic> getAllTopics() {
        return (List<Topic>) topicRepository.findAll();
    }

}
