package math.education.com.service.impl;

import math.education.com.entity.dto.SqlData;
import math.education.com.service.SqlDataService;
import math.education.com.util.ReadFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Random;

import static java.lang.System.lineSeparator;
import static math.education.com.util.KeyGenerator.generateKey;

@Service
public class SqlDataServiceImpl implements SqlDataService {

    private static final String PATH = "/application-prod.properties";
    private static final String URL_EQUALS = "url=";
    private static final String USERNAME_EQUALS = "username=";
    private static final String PASSWORD_EQUALS = "password=";
    private static final String REGEX_URL = URL_EQUALS + ".+";
    private static final String REGEX_USERNAME = USERNAME_EQUALS + ".+";
    private static final String REGEX_PASSWORD = PASSWORD_EQUALS + ".+";
    private static final String FIRST_PART_URL = "jdbc:mysql://db4free.net:3306/";
    private static final String SECOND_PART_URL = "?useSSL=false&useUnicode=yes&characterEncoding=UTF-8";

    @Autowired
    private Random random;

    @Override
    public SqlData createNewData() throws IOException {
        String databaseName = "db_math_" + generateKey(random, 8);
        String userName = "db_user_" + generateKey(random, 8);
        String password = generateKey(random, 16);

        List<String> lines = ReadFile.readFile(PATH);

        for (int i = 0; i < lines.size(); i++) {
            lines.set(i, lines.get(i).replaceFirst(REGEX_URL, URL_EQUALS + FIRST_PART_URL + databaseName + SECOND_PART_URL));
            lines.set(i, lines.get(i).replaceFirst(REGEX_USERNAME, USERNAME_EQUALS + userName));
            lines.set(i, lines.get(i).replaceFirst(REGEX_PASSWORD, PASSWORD_EQUALS + password));
        }

        writeFile(lines);

        return new SqlData(databaseName, userName, password);
    }

    private void writeFile(List<String> lines) throws IOException {

        FileWriter fileWriter = new FileWriter("src/main/resources" + PATH);
        BufferedWriter writer = new BufferedWriter(fileWriter);

        for (String line : lines) {
            writer.append(line).append(lineSeparator());
        }

        writer.close();
    }
}
