package math.education.com.service.impl;

import math.education.com.container.MistakeContainer;
import math.education.com.entity.Mistake;
import math.education.com.entity.User;
import math.education.com.entity.UserMistake;
import math.education.com.repository.MistakeRepository;
import math.education.com.repository.UserMistakeRepository;
import math.education.com.service.AnalysisMistakeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static java.util.Objects.isNull;

@Service
public class AnalysisMistakeServiceImpl implements AnalysisMistakeService {

    @Autowired
    private MistakeContainer mistakeContainer;

    @Autowired
    private UserMistakeRepository userMistakeRepository;

    @Autowired
    private MistakeRepository mistakeRepository;

    @Override
    @Transactional
    public void analysisMistakes(String actualAnswer, String correctAnswer, User user) {

        mistakeContainer.getMistakeContainer().forEach((key, mistake) -> {

            if (mistake.isMistake(actualAnswer, correctAnswer)) {

                UserMistake existsUserMistake = user.getUsersMistakes().stream()
                        .filter(userMistake -> key == userMistake.getMistake().getMistake())
                        .findFirst()
                        .orElse(null);

                if (isNull(existsUserMistake)) {
                    Mistake existsMistake = mistakeRepository.findByMistake(key);
                    UserMistake userMistake = new UserMistake();
                    userMistake.setCountMistake(1);
                    existsMistake.addUserMistake(userMistake);
                    user.addUserMistake(userMistake);
                    existsUserMistake = userMistake;
                } else {
                    existsUserMistake.setCountMistake(existsUserMistake.getCountMistake() + 1);
                }

                userMistakeRepository.save(existsUserMistake);
            }

        });

    }

}
