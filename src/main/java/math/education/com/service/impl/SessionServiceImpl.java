package math.education.com.service.impl;

import math.education.com.entity.dto.InnerSession;
import math.education.com.service.SessionService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.*;

@Service
public class SessionServiceImpl implements SessionService {

    private static final Object MONITOR = new Object();
    private static final long HALF_HOUR = 30 * 60 * 1000;
    private static final long MINUTE = 60 * 1000;

    private Map<String, InnerSession> sessionStorage;

    @PostConstruct
    public void init() {
        sessionStorage = new HashMap<>();
    }

    @Override
    public InnerSession getInnerSession(String key) {
        synchronized (MONITOR) {
            return sessionStorage.get(key);
        }
    }

    @Override
    public void addInnerSession(InnerSession innerSession) {
        synchronized (MONITOR) {
            sessionStorage.put(innerSession.getId(), innerSession);
        }
    }

    @Override
    public Set<String> getIds() {
        return sessionStorage.keySet();
    }

    @Override
    public void deleteInnerSession(String key) {
        synchronized (MONITOR) {
            sessionStorage.remove(key);
        }
    }

    @Scheduled(fixedRate = MINUTE)
    public void removeInnerSession() {
        for (Iterator<Map.Entry<String, InnerSession>> iterator = sessionStorage.entrySet().iterator(); iterator.hasNext(); ) {
            Map.Entry<String, InnerSession> entry = iterator.next();
            if (new Date().getTime() - entry.getValue().getLastUpdateTime() >= HALF_HOUR) {
                synchronized (MONITOR) {
                    iterator.remove();
                }
            }
        }
    }

}
