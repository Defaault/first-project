package math.education.com.service.impl;

import lombok.extern.slf4j.Slf4j;
import math.education.com.service.EmailService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Properties;

import static java.lang.System.lineSeparator;

@Slf4j
@Service
public class EmailServiceImpl implements EmailService {

    private JavaMailSenderImpl javaMailSender;

    @Value("${math.education.sender.mail}")
    private String senderMail;

    @Value("${math.education.sender.password}")
    private String senderPassword;

    @Value("${math.education.domain}")
    private String domain;

    @PostConstruct
    public void init() {

        javaMailSender = new JavaMailSenderImpl();
        javaMailSender.setHost("smtp.gmail.com");
        javaMailSender.setPort(587);

        javaMailSender.setUsername(senderMail);
        javaMailSender.setPassword(senderPassword);

        Properties properties = javaMailSender.getJavaMailProperties();
        properties.put("mail.transport.protocol", "smtp");
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");

    }

    @Override
    public void sendMessage(String sessionId, String userMail, String userName) {

        String url = domain + "/topics?sessionId=" + sessionId;

        log.info(url);

        StringBuilder stringBuilder = new StringBuilder("Hello, ");
        stringBuilder
                .append(userName)
                .append(lineSeparator())
                .append("Thank you, for registration on our site.")
                .append(lineSeparator())
                .append("Please, confirm your E-mail click on the confirmation link below")
                .append(lineSeparator())
                .append(url)
                .append(lineSeparator())
                .append(lineSeparator())
                .append(lineSeparator())
                .append(lineSeparator())
                .append("Regard, Dream Team");

        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(userMail);
        message.setSubject("Authorization for math education test");
        message.setText(String.valueOf(stringBuilder));
        javaMailSender.send(message);

    }
}
