package math.education.com.service.impl;

import math.education.com.entity.Task;
import math.education.com.entity.Topic;
import math.education.com.repository.TaskRepository;
import math.education.com.service.TaskService;
import math.education.com.specifications.TaskSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;

@Service
public class TaskServiceImpl implements TaskService {

    private static final int PAGE_SIZE = 10;

    @Autowired
    private TaskRepository taskRepository;

    @Override
    @Transactional
    public Task create(Task task) {
        return taskRepository.save(task);
    }

    @Override
    public Page<Task> findAllTasksByPredicate(int userId,
                                              int page,
                                              List<Topic> topics,
                                              Boolean isExistsAnswer,
                                              Boolean isNonExistsAnswer,
                                              Boolean isCorrectAnswer,
                                              Boolean isIncorrectAnswer,
                                              LocalDate fromDateTaskEducation,
                                              LocalDate toDateTaskEducation) {

        PageRequest pageRequest = PageRequest.of(page, PAGE_SIZE);

        return taskRepository
                .findAll(
                        new TaskSpecification(
                                userId,
                                topics,
                                isExistsAnswer,
                                isNonExistsAnswer,
                                isCorrectAnswer,
                                isIncorrectAnswer,
                                fromDateTaskEducation,
                                toDateTaskEducation
                        ),
                        pageRequest
                );
    }

}
