package math.education.com.service.impl;

import math.education.com.constant.PartMathematicsEnum;
import math.education.com.constant.TopicEnum;
import math.education.com.container.Container;
import math.education.com.container.TopicsContainer;
import math.education.com.mathModelFactory.Factory;
import math.education.com.service.EducationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Service
public class EducationServiceImpl implements EducationService {

    @Autowired
    private TopicsContainer topicsContainer;

    @Override
    public List<Factory> getFactories(TopicEnum topic) {

        Container containerFactories = topicsContainer.getContainerFactories(topic);

        List<Factory> factories = new ArrayList<>();

        for (int i = 1; i <= containerFactories.size(); i++) {
            factories.add(containerFactories.getFactory(i));
        }

        return factories;
    }

    @Override
    public Map<PartMathematicsEnum, Set<TopicEnum>> getTopicsNames() {
        Map<PartMathematicsEnum, Set<TopicEnum>> topicsName = topicsContainer.getTopicsName();
        Set<TopicEnum> algebraTopics = topicsName.get(PartMathematicsEnum.ALGEBRA);
        algebraTopics.removeIf(topicEnum -> topicsContainer.getContainerFactories(topicEnum).size() == 0);
        Set<TopicEnum> geometryTopics = topicsName.get(PartMathematicsEnum.GEOMETRY);
        geometryTopics.removeIf(topicEnum -> topicsContainer.getContainerFactories(topicEnum).size() == 0);
        return topicsName;
    }

}
