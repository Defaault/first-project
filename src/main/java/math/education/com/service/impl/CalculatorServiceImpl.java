package math.education.com.service.impl;

import math.education.com.container.OperationsContainer;
import math.education.com.entity.dto.DataSign;
import math.education.com.expression.NodeExpression;
import math.education.com.expression.impl.Number;
import math.education.com.expression.impl.Sign;
import math.education.com.parser.FormulaParser;
import math.education.com.service.CalculatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.Objects.nonNull;

@Service
public class CalculatorServiceImpl implements CalculatorService {

    @Autowired
    private FormulaParser formulaParser;

    @Autowired
    private OperationsContainer operationsContainer;


    @Override
    public double calculate(String formula) {
        List<String> elements = formulaParser.parse(formula);

        NodeExpression expression = getTree(elements);

        return expression.getValue();
    }

    private NodeExpression getTree(List<String> elements) {
        int index = getSplitIndex(elements);

        if (operationsContainer.getOpenBracket().equals(elements.get(0))
                && operationsContainer.getCloseBracket().equals(elements.get(elements.size() - 1))
                && index == -1) {
            elements.remove(0);
            elements.remove(elements.size() - 1);

            return getTree(elements);
        }

        if (index == -1) {
            return new Number(Double.valueOf(elements.get(0)));
        } else if (index == 0) {
            NodeExpression sign = new Sign(elements.get(index), operationsContainer);
            sign.addNode(getTree(elements.subList(index + 1, elements.size())));

            return sign;
        } else if (index == elements.size() - 1) {
            NodeExpression sign = new Sign(elements.get(index), operationsContainer);
            sign.addNode(getTree(elements.subList(0, index)));

            return sign;
        } else {
            NodeExpression sign = new Sign(elements.get(index), operationsContainer);
            sign.addNode(getTree(elements.subList(index + 1, elements.size())));
            sign.addNode(getTree(elements.subList(0, index)));

            return sign;
        }
    }

    private int getSplitIndex(List<String> elements) {
        int index = -1;
        int priority = operationsContainer.getMaxPriority() + 1;

        int openedBracketsCount = 0;

        for (int i = 0; i < elements.size(); i++) {

            String element = elements.get(i);

            if (operationsContainer.getOpenBracket().equals(element)) {
                openedBracketsCount++;
            } else if (operationsContainer.getCloseBracket().equals(element)) {
                openedBracketsCount--;
            }

            if (openedBracketsCount == 0) {
                DataSign dataSign = operationsContainer.getDataSign(element);

                if (nonNull(dataSign) && dataSign.getPriority() <= priority) {
                    index = i;
                    priority = dataSign.getPriority();
                }

            }

        }

        return index;
    }

}
