package math.education.com.service.impl;

import math.education.com.entity.Knowledge;
import math.education.com.repository.KnowledgeRepository;
import math.education.com.service.KnowledgeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class KnowledgeServiceImpl implements KnowledgeService {

    @Autowired
    private KnowledgeRepository knowledgeRepository;

    @Override
    @Transactional
    public Knowledge create(Knowledge knowledge) {
        return knowledgeRepository.save(knowledge);
    }

    @Override
    @Transactional
    public Knowledge update(Knowledge knowledge) {
        return knowledgeRepository.save(knowledge);
    }

}
