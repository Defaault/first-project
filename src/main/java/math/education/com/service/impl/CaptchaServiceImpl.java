package math.education.com.service.impl;

import math.education.com.entity.dto.CaptchaResponse;
import math.education.com.service.CaptchaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class CaptchaServiceImpl implements CaptchaService {

    @Value("${google.recaptcha.secret}")
    private String captchaSecret;

    @Value("${google.recaptcha.url}")
    private String captchaUrl;

    @Autowired
    private RestTemplateBuilder restTemplateBuilder;

    @Override
    public boolean verifyCaptcha(String ip, String captchaResponse) {
        Map<String, String> body = new HashMap<>();
        body.put("secret", captchaSecret);
        body.put("response", captchaResponse);
        body.put("remoteip", ip);

        CaptchaResponse captchaResponseEntity =
                restTemplateBuilder.build()
                        .postForObject(
                                captchaUrl + "?secret={secret}&response={response}&remoteip={remoteip}",
                                body,
                                CaptchaResponse.class,
                                body
                        );

        return captchaResponseEntity.isSuccess();
    }

}
