package math.education.com.service.impl;

import math.education.com.entity.HistoryKnowledge;
import math.education.com.repository.HistoryKnowledgeRepository;
import math.education.com.service.HistoryKnowledgeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

@Service
public class HistoryKnowledgeServiceImpl implements HistoryKnowledgeService {

    @Autowired
    private HistoryKnowledgeRepository historyKnowledgeRepository;

    @Override
    @Transactional
    public HistoryKnowledge create(HistoryKnowledge historyKnowledge) {
        return historyKnowledgeRepository.save(historyKnowledge);
    }

    @Override
    public List<HistoryKnowledge> getHistoryKnowledge(int idUser, String topic, LocalDate fromDate, LocalDate toDate) {
        return historyKnowledgeRepository.getHistoryKnowledge(idUser, topic, fromDate.toString(), toDate.toString());
    }

}
