package math.education.com.mathModel.geometry;

import math.education.com.mathModel.Model;

import java.util.Random;

import static java.lang.System.lineSeparator;
import static math.education.com.util.CalculatorUtil.getRandomIntFromTo;
import static math.education.com.util.CalculatorUtil.roundNumber;

public class Task27 extends Model {

    public Task27(Random random) {
        super(random);
    }

    @Override
    public void generateTask() {
        int a = getRandomIntFromTo(random, -10, 10);
        int b = getRandomIntFromTo(random, -10, 10);
        int c = getRandomIntFromTo(random, -10, 10);
        int d = getRandomIntFromTo(random, -10, 10);
        int e = getRandomIntFromTo(random, -10, 10);

        while ((a + b * e == 0) || (((double) ((a * c + b * d) * 10)) % (a + b * e) != 0)) {
            a = getRandomIntFromTo(random, -10, 10);
            b = getRandomIntFromTo(random, -10, 10);
            c = getRandomIntFromTo(random, -10, 10);
            d = getRandomIntFromTo(random, -10, 10);
            e = getRandomIntFromTo(random, -10, 10);
        }

        task = "In a rectangular coordinate system, the perpendicular vectors AB and a (" + a + "; " + b + ") are given." + lineSeparator()
                + "Determine the abscissa of point B if A (" + c + "; " + d + ") and the point B lies on the line y = " + e + " * x."
                + lineSeparator()
                + "Round the answer to tenths";
        correctAnswer = String.valueOf((roundNumber(((double) (a * c + b * d)) / (a + b * e), 1)));
        solution = "Let the point B have the coordinates (x; y)." + lineSeparator()
                + "Then the AB (x" + (c > 0 ? " - " : " + ") + Math.abs(c) + "; y" + (d > 0 ? " - " : " + ") + Math.abs(d) + ")." + lineSeparator()
                + "A scalar product of the vectors AB and a is equal to " + a + " * (x" + (c < 0 ? " + " : " - ") + Math.abs(c) + ")"
                + (b > 0 ? " + " : " - ") + Math.abs(b) + " * (y" + (d < 0 ? " + " : " - ") + Math.abs(d) + ") = 0" + lineSeparator()
                + "Open the brackets " + a + " * x" + (a * c < 0 ? " + " : " - ") + Math.abs(a * c) + (b > 0 ? " + " : " - ") + Math.abs(b) + " * y"
                + (b * d < 0 ? " + " : " - ") + Math.abs(b * d) + " = 0" + lineSeparator()
                + "We also have the second equation y = " + e + " * x" + lineSeparator()
                + "We substitute y into the first equation " + a + " * x" + (a * c < 0 ? " + " : " - ") + Math.abs(a * c) + (b > 0 ? " + " : " - ")
                + Math.abs(b) + " * (" + e + " * x)" + (b * d < 0 ? " + " : " - ") + Math.abs(b * d) + " = 0" + lineSeparator()
                + "We simplify equation " + (a + b * e) + " * x = " + (a * c + b * d) + lineSeparator()
                + "Answer: " + correctAnswer;
        double f = roundNumber(((double) getRandomIntFromTo(random, 10, 100)) / 10, 1);
        while (correctAnswer.equals(String.valueOf(f))) {
            f = roundNumber(((double) getRandomIntFromTo(random, 10, 100)) / 10, 1);
        }
        answerExample = "-" + f;
    }

}
