package math.education.com.mathModel.geometry;

import math.education.com.mathModel.Model;

import java.util.Random;

import static java.lang.System.lineSeparator;
import static math.education.com.util.CalculatorUtil.getRandomIntFromTo;
import static math.education.com.util.CalculatorUtil.roundNumber;

public class Task4 extends Model {

    private static final String PATH_PICTURE = "task4.png";

    public Task4(Random random) {
        super(random);
    }

    @Override
    public void generateTask() {
        pathPicture = PATH_PICTURE;
        int c = getRandomIntFromTo(random, 1, 10);
        int b = getRandomIntFromTo(random, 2, 10);
        int a = getRandomIntFromTo(random, 1, b - 1);
        task = "The figure shows parallel lines a and b, as well as a secant CD." + lineSeparator()
                + "Find the distance between lines a and b if CK = " + c + " cm and KD = " + b + " cm." + lineSeparator()
                + "The distance from the point K to the straight line a is " + a + " сm." + lineSeparator()
                + "Round the answer to tenths. Answer write down in centimeters.";
        correctAnswer = String.valueOf(roundNumber((a * (c + b)) / ((double) b), 1));
        solution = "If we drop two perpendiculars CA and KB on line a from points C and K respectively,"
                + "we get two similar triangles ACD and BKD." + lineSeparator()
                + "ТSince they are similar, you can write down the aspect ratio of the sides CA / KB = CD / KD." + lineSeparator()
                + "The CD side is equal to CK + KD = " + c + " + " + b + " = " + (c + b) + "." + lineSeparator()
                + "Substituting the known ones into the equation and obtaining CA / " + a + " = " + (c + b) + " / " + b + "." + lineSeparator()
                + "We take out all known in one direction, and not known in another CA = " + a + " * " + (c + b) + " / " + b + "." + lineSeparator()
                + "Answer: " + correctAnswer;
        String d = String.valueOf(roundNumber(((double) getRandomIntFromTo(random, 10, 100)) / 10, 1));
        while (correctAnswer.equals(d)) {
            d = String.valueOf(roundNumber(((double) getRandomIntFromTo(random, 10, 100)) / 10, 1));
        }
        answerExample = d;
    }

}
