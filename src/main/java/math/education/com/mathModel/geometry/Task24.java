package math.education.com.mathModel.geometry;

import math.education.com.mathModel.Model;

import java.util.Random;

import static java.lang.System.lineSeparator;
import static math.education.com.util.CalculatorUtil.getRandomIntFromTo;

public class Task24 extends Model {

    private static final String PATH_PICTURE = "task24.png";

    public Task24(Random random) {
        super(random);
    }

    @Override
    public void generateTask() {
        pathPicture = PATH_PICTURE;
        int a = getRandomIntFromTo(random, 2, 50);
        while (Math.sqrt(2 * a) % 1 != 0) {
            a = getRandomIntFromTo(random, 2, 50);
        }
        task = "The figure shows the isosceles trapezoid ABCD and the square KBCM." + lineSeparator()
                + "The points K and M are the centers of the diagonals AC and BD of the trapezium, respectively." + lineSeparator()
                + "The square of the square of KBCM is " + a + " cm of square." + lineSeparator()
                + "Determine the length of the diagonal AC (in cm).";
        correctAnswer = String.valueOf(2 * ((int) Math.sqrt(2 * a)));
        solution = "The square of the square is S = (d ^ 2) / 2, where d is the diagonal of the square." + lineSeparator()
                + "We express from this formula the diagonal of the square d = sqrt (2 * S) = sqrt (2 * " + a + ") = " + ((int) Math.sqrt(2 * a))
                + "." + lineSeparator()
                + "Correspondingly, the diagonal of the trapezoid is 2 * d." + lineSeparator()
                + "Answer: " + correctAnswer;
        String b = String.valueOf(getRandomIntFromTo(random, 1, 10));
        while (correctAnswer.equals(b)) {
            b = String.valueOf(getRandomIntFromTo(random, 1, 10));
        }
        answerExample = b;
    }

}
