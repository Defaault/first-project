package math.education.com.mathModel.geometry;

import math.education.com.mathModel.Model;

import java.util.Random;

import static java.lang.System.lineSeparator;
import static math.education.com.util.CalculatorUtil.getRandomIntFromTo;
import static math.education.com.util.CalculatorUtil.roundNumber;

public class Task6 extends Model {

    private static final String PATH_PICTURE = "task6.png";

    public Task6(Random random) {
        super(random);
    }

    @Override
    public void generateTask() {
        pathPicture = PATH_PICTURE;
        int b = getRandomIntFromTo(random, 1, 20);
        while (b % 3 != 0) {
            b = getRandomIntFromTo(random, 1, 20);
        }
        int a = getRandomIntFromTo(random, 1, 100);
        while (a - (4 * b / 3) < 0 || (a - (4 * b / 3)) % 6 != 0) {
            a = getRandomIntFromTo(random, 1, 100);
        }
        task = "The figure shows the correct unfolded triangular prism." + lineSeparator()
                + "Determine the area of the lateral surface of this prism"
                + " if the perimeter of the sweep is " + a + " cm and the perimeter of the base of the prism is " + b + " cm." + lineSeparator()
                + "Answer write down in square centimeters.";
        correctAnswer = String.valueOf((int) roundNumber((3 * a * b - 4 * Math.pow(b, 2)) / 18, 1));
        solution = "Since the perimeter of the base is " + b + " cm, the smaller side is " + (b / 3) + " cm." + lineSeparator()
                + "In the perimeter of the beam, we see four segments of the smaller side and six segments of the larger side." + lineSeparator()
                + "Therefore, we can write the equation for finding the larger side 4 * " + (b / 3) + " + 6 * x = " + a + "." + lineSeparator()
                + "We derive x = (" + a + " - (" + 4 + " * " + (b / 3) + ")) / 6" + "." + lineSeparator()
                + "As a result, knowing both sides, calculate the area of the lateral surface  S = 3 * " + (b / 3) + " * " + ((a - 4 * b / 3) / 6)
                + "." + lineSeparator()
                + "Answer: " + correctAnswer;
        String c = String.valueOf(getRandomIntFromTo(random, 1, 10));
        while (correctAnswer.equals(c)) {
            c = String.valueOf(getRandomIntFromTo(random, 1, 10));
        }
        answerExample = c;
    }

}
