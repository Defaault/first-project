package math.education.com.mathModel.geometry;

import math.education.com.mathModel.Model;

import java.util.Random;

import static java.lang.System.lineSeparator;
import static math.education.com.util.CalculatorUtil.getRandomIntFromTo;

public class Task11 extends Model {

    private static final String PATH_PICTURE = "task11.png";

    public Task11(Random random) {
        super(random);
    }

    @Override
    public void generateTask() {
        pathPicture = PATH_PICTURE;
        int a = getRandomIntFromTo(random, 1, 10);
        int b = getRandomIntFromTo(random, 1, 10);
        task = "In the right-angled triangle ABC (∠C = 90 °), the distances from the midpoint of the median BM to the legs of AC and BC are "
                + a + " cm and " + b + " cm, respectively." + lineSeparator()
                + "Determine the length of the leg of AC (in cm).";
        correctAnswer = String.valueOf(b * 4);
        solution = "Suppose the middle of the median is point K, and the segment that connects the middle of the median and the side of BC is KL."
                + lineSeparator()
                + "In the triangle MBC, KL is the middle line." + lineSeparator()
                + "Therefore, MC is equal to 2 * KL = 2 * " + b + " = " + (2 * b) + "." + lineSeparator()
                + "AC = 2 * MC = 2 * " + (2 * b) + " = " + (4 * b) + "." + lineSeparator()
                + "Answer: " + correctAnswer;
        int c = getRandomIntFromTo(random, 1, 10);
        while (c == b) {
            c = getRandomIntFromTo(random, 1, 10);
        }
        answerExample = String.valueOf(c * 4);
    }

}
