package math.education.com.mathModel.geometry;

import math.education.com.mathModel.Model;

import java.util.Random;

import static java.lang.System.lineSeparator;
import static math.education.com.util.CalculatorUtil.getRandomIntFromTo;

public class Task14 extends Model {

    public Task14(Random random) {
        super(random);
    }

    @Override
    public void generateTask() {
        int a = getRandomIntFromTo(random, -10, 10);
        int b = getRandomIntFromTo(random, -10, 10);
        int c = getRandomIntFromTo(random, -10, 10);
        int d = getRandomIntFromTo(random, -10, 10);
        int e = getRandomIntFromTo(random, -10, 10);
        while ((a * e - a * d + b * c) % b != 0) {
            a = getRandomIntFromTo(random, -10, 10);
            b = getRandomIntFromTo(random, -10, 10);
            c = getRandomIntFromTo(random, -10, 10);
            d = getRandomIntFromTo(random, -10, 10);
            e = getRandomIntFromTo(random, -10, 10);
        }
        task = "In a rectangular coordinate system, the collinear vectors AB and a (" + a + "; " + b + ")." + lineSeparator()
                + "Determine the abscissa of point B if A (" + c + ", " + d + ") and point B lies on the line y = " + e + ".";
        correctAnswer = String.valueOf((a * e - a * d + b * c) / b);
        solution = "Let the vector AB have the coordinates (x; y)." + lineSeparator()
                + "As vectors are collinear, then we can write ratio of vectors (x" + (c > 0 ? " - " : " + ") + Math.abs(c) + ") / "
                + (a > 0 ? a : "(" + a + ")") + " = (y" + (d > 0 ? " - " : " + ") + Math.abs(d) + ") / " + (b > 0 ? b : "(" + b + ")") + "."
                + lineSeparator()
                + "We also have the second equation y = " + e + "." + lineSeparator()
                + "Substitute into the first equation (x" + (c > 0 ? " - " : " + ") + Math.abs(c) + ") / " + (a > 0 ? a : "(" + a + ")") + " = ("
                + e + (d > 0 ? " - " : " + ") + Math.abs(d) + ") / " + (b > 0 ? b : "(" + b + ")") + "." + lineSeparator()
                + "Convert a proportion (x" + (c > 0 ? " - " : " + ") + Math.abs(c) + ") * " + (b > 0 ? b : "(" + b + ")") + " = " + (e - d) + " * "
                + (a > 0 ? a : "(" + a + ")") + "." + lineSeparator()
                + "Open the brackets " + b + " * x" + (c * b > 0 ? " - " : " + ") + Math.abs(c * b) + " = " + (e - d) * a + "." + lineSeparator()
                + "We simplify equation x = (" + ((e - d) * a > 0 ? (e - d) * a + (c * b > 0 ? " + " : " - ")
                + Math.abs(c * b) : c * b + ((e - d) * a > 0 ? " + " : " - ") + Math.abs((e - d) * a)) + ") / " + (b > 0 ? b : "(" + b + ")") + "."
                + lineSeparator()
                + "Answer: " + correctAnswer;
        int f = getRandomIntFromTo(random, 1, 10);
        while (correctAnswer.equals(String.valueOf(f))) {
            f = getRandomIntFromTo(random, 1, 10);
        }
        answerExample = String.valueOf(f);
    }

}
