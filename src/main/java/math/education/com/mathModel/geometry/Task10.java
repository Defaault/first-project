package math.education.com.mathModel.geometry;

import math.education.com.mathModel.Model;

import java.util.Random;

import static java.lang.System.lineSeparator;
import static math.education.com.util.CalculatorUtil.getRandomIntFromTo;

public class Task10 extends Model {

    private static final String PATH_PICTURE = "task10.png";

    public Task10(Random random) {
        super(random);
    }

    @Override
    public void generateTask() {
        pathPicture = PATH_PICTURE;
        int c = getRandomIntFromTo(random, 1, 10);
        int b = getRandomIntFromTo(random, 1, 10);
        int a = getRandomIntFromTo(random, b + 2 * c + 8, 100);
        while ((a - b - 2 * c) % 8 != 0) {
            a = getRandomIntFromTo(random, b + 2 * c + 8, 100);
        }
        task = "To determine the width of the motorway hmag (in m)"
                + " which has 4 identical traffic lanes in both directions"
                + " using the formula hmag = 8 * b + r + 2 * Δ"
                + " where:" + lineSeparator()
                + "b - width of one traffic lane;" + lineSeparator()
                + "r - width of the separation strip between directions of traffic;" + lineSeparator()
                + "Δ - the width of the securing strip between the extreme lane and the curb." + lineSeparator()
                + "Determine the width b (in m) of one strip if hmag = " + a + " m, r = " + b + " m, Δ = " + c + " m." + lineSeparator()
                + "Answer write down in meters";
        correctAnswer = String.valueOf((a - b - 2 * c) / 8);
        solution = "We express from the general formula the width of the strip of motion b = (hmag - r - 2 * Δ) / 8." + lineSeparator()
                + "We substitute the values and get b equal to (" + a + " - " + b + " - " + "2 * " + c + ") / 8" + "." + lineSeparator()
                + "Answer: " + correctAnswer;
        String d = String.valueOf(getRandomIntFromTo(random, 1, 10));
        while (correctAnswer.equals(d)) {
            d = String.valueOf(getRandomIntFromTo(random, 1, 10));
        }
        answerExample = d;
    }

}
