package math.education.com.mathModel.geometry;

import math.education.com.mathModel.Model;

import java.util.Random;

import static java.lang.System.lineSeparator;
import static math.education.com.util.CalculatorUtil.getRandomIntFromTo;

public class Task17 extends Model {

    public Task17(Random random) {
        super(random);
    }

    @Override
    public void generateTask() {
        int a = getRandomIntFromTo(random, 1, 20);
        task = "On the segment AB, the point M is chosen so that the length of the segment AM is three times larger than the length MB."
                + lineSeparator()
                + "Determine the length of the segment AB if MB = " + a + " cm." + lineSeparator()
                + "Answer write down in centimeters.";
        correctAnswer = String.valueOf(a * 4);
        solution = "Since AM is three times larger than MB, then AM = 3 * MB." + lineSeparator()
                + "Let's compose the equation AB = AM + MB = 3 * MB + MB = 4 * MB = 4 * " + a + "." + lineSeparator()
                + "Answer: " + correctAnswer;
        int b = getRandomIntFromTo(random, 1, 10);
        while (a == b) {
            b = getRandomIntFromTo(random, 1, 10);
        }
        answerExample = String.valueOf(b * 4);
    }

}
