package math.education.com.mathModel.geometry;

import math.education.com.mathModel.Model;

import java.util.Random;

import static java.lang.System.lineSeparator;
import static math.education.com.util.CalculatorUtil.getRandomIntFromTo;

public class Task21 extends Model {

    private static final int[][] PYTHAGOREAN_TROIKA = {
            {3, 4, 5},
            {5, 12, 13},
            {8, 15, 17},
            {7, 24, 25},
            {20, 21, 29},
            {12, 35, 37},
            {9, 40, 41}
    };

    public Task21(Random random) {
        super(random);
    }

    @Override
    public void generateTask() {
        int[] number = PYTHAGOREAN_TROIKA[random.nextInt(PYTHAGOREAN_TROIKA.length)];
        int a = number[0] * 8;
        task = "The perimeter of the base of the right quadrilateral pyramid is " + a + " cm." + lineSeparator()
                + "Determine the height of the pyramid if its apothem is " + number[2] + "." + lineSeparator()
                + "Answer write down in centimeters.";
        correctAnswer = String.valueOf(number[1]);
        solution = "Knowing the perimeter of the base, one can find one side a = P / 4 = " + a + " / 4 = " + (number[0] * 2) + "." + lineSeparator()
                + "Divide the side in half, you can find the shortest distance from either side to the middle of the base b = "
                + (number[0] * 2) + " / 2 = " + number[0] + "." + lineSeparator()
                + "As a result, it remains only to find the height of the pyramid from the theorem of Pythagoras h = sqrt("
                + number[2] + " ^ 2 - " + number[0] + " ^ 2)." + lineSeparator()
                + "Answer: " + correctAnswer;
        String b = String.valueOf(getRandomIntFromTo(random, 1, 10));
        while (correctAnswer.equals(b)) {
            b = String.valueOf(getRandomIntFromTo(random, 1, 10));
        }
        answerExample = b;
    }

}
