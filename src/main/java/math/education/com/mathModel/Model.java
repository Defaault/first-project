package math.education.com.mathModel;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.time.LocalDate;
import java.util.Random;

public abstract class Model {

    @JsonIgnore
    protected Random random;
    @JsonIgnore
    protected String correctAnswer;
    protected String actualAnswer;
    protected String task;
    protected String pathPicture;
    @JsonIgnore
    protected String solution;
    protected String answerExample;
    protected LocalDate timeTaskEducation;

    public Model(Random random) {
        this.random = random;
    }

    public String getCorrectAnswer() {
        return correctAnswer;
    }

    public void setCorrectAnswer(String correctAnswer) {
        this.correctAnswer = correctAnswer;
    }

    public String getActualAnswer() {
        return actualAnswer;
    }

    public void setActualAnswer(String actualAnswer) {
        this.actualAnswer = actualAnswer;
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public abstract void generateTask();

    public String getPathPicture() {
        return pathPicture;
    }

    public void setPathPicture(String pathPicture) {
        this.pathPicture = pathPicture;
    }

    public String getSolution() {
        return solution;
    }

    public void setSolution(String solution) {
        this.solution = solution;
    }

    public String getAnswerExample() {
        return answerExample;
    }

    public void setAnswerExample(String answerExample) {
        this.answerExample = answerExample;
    }

    public LocalDate getDateTaskEducation() {
        return timeTaskEducation;
    }

    public void setDateTaskEducation(LocalDate timeTaskEducation) {
        this.timeTaskEducation = timeTaskEducation;
    }
}
