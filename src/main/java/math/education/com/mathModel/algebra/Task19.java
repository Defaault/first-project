package math.education.com.mathModel.algebra;

import math.education.com.mathModel.Model;

import java.util.Random;

import static java.lang.System.lineSeparator;
import static math.education.com.util.CalculatorUtil.getRandomIntFromTo;

public class Task19 extends Model {

    public Task19(Random random) {
        super(random);
    }

    @Override
    public void generateTask() {
        int a = getRandomIntFromTo(random, 1, 10);
        int b = getRandomIntFromTo(random, 2, 20);
        while (b % a != 0) {
            a = getRandomIntFromTo(random, 1, 10);
            b = getRandomIntFromTo(random, 2, 20);
        }
        task = "Determine the intersection point of the graph of the function y = " + a + " * x - " + b + " with the x axis." + lineSeparator()
                + "If the answer is (x; y), then write down the sum x + y in the answer.";
        correctAnswer = String.valueOf(b / a);
        solution = "Since the graph intersects x, we can compose the second equation y = 0." + lineSeparator()
                + "The left-hand sides of the equations are equal, we can equate them and obtain the linear equation " + a + " * x - " + b + " = 0."
                + lineSeparator()
                + "We will carry all the unknowns in one direction, and the unknowns in the opposite direction x = " + b + " / " + a + "."
                + lineSeparator()
                + "Answer: " + correctAnswer;
        String c = String.valueOf(getRandomIntFromTo(random, 1, 10));
        while (correctAnswer.equals(c)) {
            c = String.valueOf(getRandomIntFromTo(random, 1, 10));
        }
        answerExample = c;
    }

}
