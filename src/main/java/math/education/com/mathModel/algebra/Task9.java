package math.education.com.mathModel.algebra;

import math.education.com.mathModel.Model;

import java.util.Random;

import static java.lang.System.lineSeparator;
import static math.education.com.util.CalculatorUtil.getRandomIntFromTo;

public class Task9 extends Model {

    public Task9(Random random) {
        super(random);
    }

    @Override
    public void generateTask() {
        int a = getRandomIntFromTo(random, 1, 10);
        int b = getRandomIntFromTo(random, 1, 10);

        task = "Find the derivative of the function f(x) = x * (x ^ " + a + " + " + b + ").";
        correctAnswer = (a + 1) + " * x ^ " + a + " + " + b;
        solution = "Open parenthesis f(x) = x ^ " + (a + 1) + " + " + b + " * x" + "." + lineSeparator()
                + "The derivative of x ^ " + (a + 1) + " equals " + (a + 1) + " * x ^ (" + (a + 1) + " - 1)" + "." + lineSeparator()
                + "The derivative of " + b + " * x equals " + b + "." + lineSeparator()
                + "Answer: " + correctAnswer;
        int c = getRandomIntFromTo(random, 1, 10);
        int d = getRandomIntFromTo(random, 1, 10);
        int e = getRandomIntFromTo(random, 1, 10);
        while (correctAnswer.equals((c + 1) + " * x ^ " + d + " + " + e)) {
            c = getRandomIntFromTo(random, 1, 10);
            d = getRandomIntFromTo(random, 1, 10);
            e = getRandomIntFromTo(random, 1, 10);
        }
        answerExample = (c + 1) + " * x ^ " + d + " + " + e;
    }

}
