package math.education.com.mathModel.algebra;

import math.education.com.mathModel.Model;

import java.util.Random;

import static java.lang.System.lineSeparator;
import static math.education.com.util.CalculatorUtil.getRandomIntFromTo;

public class Task7 extends Model {

    public Task7(Random random) {
        super(random);
    }

    @Override
    public void generateTask() {
        int a = getRandomIntFromTo(random, 2, 5);
        int answer = getRandomIntFromTo(random, 1, 5);

        int var = (int) Math.pow(a, answer);

        int b = getRandomIntFromTo(random, 1, var);
        while (var % b != 0) {
            b = getRandomIntFromTo(random, 1, var);
        }

        int c = var / b;

        var = getRandomIntFromTo(random, 1, 5);

        int d = c * var;
        c = var;

        task = "Compute the expressions log" + a + "(" + b + ") + log" + a + "(" + d + ") - log" + a + "(" + c + ")";
        correctAnswer = String.valueOf(answer);
        solution = "If one logarithm is added to another and the exponents are equal,"
                + " then the logarithms should be multiplied accordingly"
                + " log" + a + "(" + b + ") + log" + a + "(" + d + ") = log" + a + "(" + b + " * " + d + ")" + "." + lineSeparator()
                + "Hence the expression takes the form log" + a + "(" + (b * d) + ") - log" + a + "(" + c + ")" + "." + lineSeparator()
                + "Similarly to addition, there is a property of subtracting logarithms "
                + " log" + a + "(" + (b * d) + ") - log" + a + "(" + c + ") = log" + a + "(" + (b * d) + " / " + c + ") = log"
                + a + "(" + (b * d / c) + ")." + lineSeparator()
                + "Answer: " + correctAnswer;
        int e = getRandomIntFromTo(random, 1, 10);
        while (answer == e) {
            e = getRandomIntFromTo(random, 1, 10);
        }
        answerExample = String.valueOf(e);
    }

}
