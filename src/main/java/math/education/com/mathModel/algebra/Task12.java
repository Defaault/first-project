package math.education.com.mathModel.algebra;

import math.education.com.mathModel.Model;

import java.util.Random;

import static java.lang.System.lineSeparator;
import static math.education.com.util.CalculatorUtil.getRandomIntFromTo;

public class Task12 extends Model {

    public Task12(Random random) {
        super(random);
    }

    @Override
    public void generateTask() {
        int a = getRandomIntFromTo(random, 2, 5);
        int b = getRandomIntFromTo(random, 1, 10);
        int c = b * (1 - (int) Math.pow(a, 4)) / (1 - a);
        task = "The denominator of the geometric progression is " + a + ","
                + " and the sum of the first four members is " + c + "." + lineSeparator()
                + "Find the first term of the geometric progression.";
        correctAnswer = String.valueOf(b);
        solution = "The sum of the geometric progression S = b1 * (1 - q ^ n) / (1 - q)."
                + " We express from the formula the first term b1 = S * (1 - q) / (1 - q ^ n)."
                + " We substitute the values b1 = " + c + " * (1 - " + a + ") / (1 - " + a + " ^ 4)." + lineSeparator()
                + "Answer: " + correctAnswer;
        int d = getRandomIntFromTo(random, 1, 10);
        while (d == b) {
            d = getRandomIntFromTo(random, 1, 10);
        }
        answerExample = String.valueOf(d);
    }

}
