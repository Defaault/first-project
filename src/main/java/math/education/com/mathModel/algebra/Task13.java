package math.education.com.mathModel.algebra;

import math.education.com.mathModel.Model;

import java.util.Random;

import static java.lang.System.lineSeparator;
import static math.education.com.util.CalculatorUtil.getRandomIntFromTo;

public class Task13 extends Model {

    public Task13(Random random) {
        super(random);
    }

    @Override
    public void generateTask() {
        int a = getRandomIntFromTo(random, 2, 10);
        int b = getRandomIntFromTo(random, 1, a);
        int c = getRandomIntFromTo(random, 2, 10);
        int d = getRandomIntFromTo(random, 1, c);

        int aFactorial = 1;
        for (int i = a; i > (a - b); i--) {
            aFactorial *= i;
        }

        int bFactorial = 1;
        for (int i = 1; i <= b; i++) {
            bFactorial *= i;
        }

        int cFactorial = 1;
        for (int i = c; i > (c - d); i--) {
            cFactorial *= i;
        }

        int dFactorial = 1;
        for (int i = 1; i <= d; i++) {
            dFactorial *= i;
        }

        StringBuilder aBuilder = new StringBuilder("(");
        StringBuilder bBuilder = new StringBuilder("(");
        StringBuilder cBuilder = new StringBuilder("(");
        StringBuilder dBuilder = new StringBuilder("(");

        for (int i = a; i > (a - b); i--) {
            aBuilder.append(i).append(" * ");
        }
        aBuilder.delete(aBuilder.length() - 3, aBuilder.length()).append(")");

        for (int i = 1; i <= b; i++) {
            bBuilder.append(i).append(" * ");
        }
        bBuilder.delete(bBuilder.length() - 3, bBuilder.length()).append(")");

        for (int i = c; i > (c - d); i--) {
            cBuilder.append(i).append(" * ");
        }
        cBuilder.delete(cBuilder.length() - 3, cBuilder.length()).append(")");

        for (int i = 1; i <= d; i++) {
            dBuilder.append(i).append(" * ");
        }
        dBuilder.delete(dBuilder.length() - 3, dBuilder.length()).append(")");

        task = "Alena has " + a + " different photos with her images and " + c + " different photos of her class." + lineSeparator()
                + "How many total ways does she have to choose from them "
                + b + " photos with her own image for a personal page in a social network and "
                + d + " photos of her class for the school's website?";
        correctAnswer = String.valueOf((aFactorial / bFactorial) * (cFactorial / dFactorial));
        solution = "There is a formula for subtraction of probability C = (" + aBuilder.toString() + " * " + cBuilder.toString() + ") / ("
                + bBuilder.toString() + " * " + dBuilder.toString() + ")." + lineSeparator()
                + "Answer: " + correctAnswer;
        int e = getRandomIntFromTo(random, 1, 10);
        while (correctAnswer.equals(String.valueOf(e))) {
            e = getRandomIntFromTo(random, 1, 10);
        }
        answerExample = String.valueOf(e);
    }

}
