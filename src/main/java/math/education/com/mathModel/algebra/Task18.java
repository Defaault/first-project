package math.education.com.mathModel.algebra;

import math.education.com.mathModel.Model;

import java.util.Random;

import static java.lang.System.lineSeparator;
import static math.education.com.util.CalculatorUtil.getRandomIntFromTo;

public class Task18 extends Model {

    public Task18(Random random) {
        super(random);
    }

    @Override
    public void generateTask() {
        int a = getRandomIntFromTo(random, 2, 10);
        int b = getRandomIntFromTo(random, 2, 10);
        int c = getRandomIntFromTo(random, 2, 10);
        task = "Solve the equation " + a + " ^ (" + b + " * x) = 1 / " + a + " ^ " + (b * c);
        correctAnswer = "-" + c;
        solution = "By the power-law property a ^ n = 1 / a ^ (-n)." + lineSeparator()
                + "Consequently a ^ (-n) = 1 / a ^ n." + lineSeparator()
                + "After the transformations, the equation " + a + " ^ (" + b + " * x) = " + a + " ^ (-" + (b * c) + ") is obtained."
                + lineSeparator()
                + "Since the bases are equal, the equation acquires the form " + b + " * x = -" + (b * c) + "." + lineSeparator()
                + "We take all the unknown parts of the equation in one direction, and the known parts of the equation in the other direction x = -"
                + (b * c) + " / " + b + "." + lineSeparator()
                + "Answer: " + correctAnswer;
        int d = getRandomIntFromTo(random, 1, 10);
        while (c == d) {
            d = getRandomIntFromTo(random, 1, 10);
        }
        answerExample = "-" + d;
    }

}
