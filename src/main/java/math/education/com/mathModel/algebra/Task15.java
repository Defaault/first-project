package math.education.com.mathModel.algebra;

import math.education.com.mathModel.Model;

import java.util.Random;

import static java.lang.System.lineSeparator;
import static math.education.com.util.CalculatorUtil.getRandomIntFromTo;

public class Task15 extends Model {

    public Task15(Random random) {
        super(random);
    }

    @Override
    public void generateTask() {
        int b = getRandomIntFromTo(random, 2, 10);
        int c = (int) Math.pow(b, 2);
        while (c % 4 != 0) {
            b = getRandomIntFromTo(random, 2, 10);
            c = (int) Math.pow(b, 2);
        }
        task = "Given functions f(x) = x ^ 3 and g(x) = " + c + " * |x|." + lineSeparator()
                + "Calculate the area of the figure bounded by the graphs of the functions f and g." + lineSeparator()
                + "Answer write down in square units.";
        correctAnswer = String.valueOf(((int) Math.pow(c, 2)) / 4);
        solution = "In order to find the limits of integration, it is necessary to solve the system of equations." + lineSeparator()
                + "y = x ^ 3" + lineSeparator()
                + "y = " + c + " * |x|." + lineSeparator()
                + "Solving this system, we obtain the limits of integration 0 and " + ((int) Math.sqrt(c)) + "." + lineSeparator()
                + "In order to find the area, we need to calculate the integral g(x) - f(x) from 0 to " + ((int) Math.sqrt(c)) + "."
                + lineSeparator()
                + "As a result, the integral " + c + " * x - x ^ 3 will be equal to (" + c + " * x ^ 2) / 2 - (x ^ 4) / 4 from 0 to "
                + ((int) Math.sqrt(c)) + "." + lineSeparator()
                + "Substituting the limits, we get the final expression (" + c + " * " + ((int) Math.sqrt(c)) + " ^ 2) / 2 - ("
                + ((int) Math.sqrt(c)) + " ^ 4) / 4." + lineSeparator()
                + "Answer: " + correctAnswer;
        String d = String.valueOf(getRandomIntFromTo(random, 1, 10));
        while (correctAnswer.equals(d)) {
            d = String.valueOf(getRandomIntFromTo(random, 1, 10));
        }
        answerExample = d;
    }

}
