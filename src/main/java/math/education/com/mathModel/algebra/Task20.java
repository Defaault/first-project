package math.education.com.mathModel.algebra;

import math.education.com.mathModel.Model;

import java.util.Random;

import static java.lang.System.lineSeparator;
import static math.education.com.util.CalculatorUtil.getRandomIntFromTo;

public class Task20 extends Model {

    public Task20(Random random) {
        super(random);
    }

    @Override
    public void generateTask() {
        int a = getRandomIntFromTo(random, -10, 10);
        int b = getRandomIntFromTo(random, 1, 10);
        task = "In the arithmetic progression, (an): a{1} = " + a + ", a{5} = a{4} + " + b + "." + lineSeparator()
                + "Determine the tenth member a {10} of this progression.";
        correctAnswer = String.valueOf(a + 9 * b);
        solution = "In the arithmetic progression, a{n+1} = a{n} + d." + lineSeparator()
                + "Consequently d = " + b + "." + lineSeparator()
                + "Similarly, in the arithmetic progression  a{n} = a{1} + d * (n - 1)." + lineSeparator()
                + "Put the values in the expression and get a{10} = " + a + " + " + b + " * (10 - 1)." + lineSeparator()
                + "Answer: " + correctAnswer;
        int c = getRandomIntFromTo(random, -10, 10);
        while (correctAnswer.equals(String.valueOf(c))) {
            c = getRandomIntFromTo(random, -10, 10);
        }
        answerExample = String.valueOf(c);
    }

}
