package math.education.com.mathModel.algebra;

import math.education.com.mathModel.Model;

import java.util.Random;

import static java.lang.System.lineSeparator;
import static math.education.com.util.CalculatorUtil.getRandomIntFromTo;
import static math.education.com.util.CalculatorUtil.roundNumber;

public class Task25 extends Model {

    public Task25(Random random) {
        super(random);
    }

    @Override
    public void generateTask() {
        int a = getRandomIntFromTo(random, 1, 10);
        int b = getRandomIntFromTo(random, 10, 100);
        int c = getRandomIntFromTo(random, 1, 10);

        while ((b / c == 10 && (b % c == 0)) || b / c < 10) {
            b = getRandomIntFromTo(random, 10, 100);
            c = getRandomIntFromTo(random, 1, 10);
        }

        task = "Find the domain of the function y = " + a + " / sqrt(" + b + " - " + c + " * x)." + lineSeparator()
                + "In answer, write down the largest integer two-digit number that belongs to the domain of definition of this function.";
        correctAnswer = b % c == 0 ? String.valueOf(b / c - 1) : String.valueOf(b / c);
        solution = "Since the denominator has a square root, the domain of definition of the function " + b + " - " + c + " * x > 0."
                + lineSeparator()
                + "We transfer all the unknowns in one direction, known to the other x < " + b + " / " + c + " = "
                + roundNumber(((double) b) / c, 2) + "." + lineSeparator()
                + "Answer: " + correctAnswer;
        int d = getRandomIntFromTo(random, 10, 99);
        while (correctAnswer.equals(String.valueOf(d))) {
            d = getRandomIntFromTo(random, 10, 99);
        }
        answerExample = String.valueOf(d);
    }

}
