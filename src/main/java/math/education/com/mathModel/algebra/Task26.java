package math.education.com.mathModel.algebra;

import math.education.com.mathModel.Model;

import java.util.Random;

import static java.lang.System.lineSeparator;
import static math.education.com.util.CalculatorUtil.getRandomIntFromTo;
import static math.education.com.util.CalculatorUtil.roundNumber;

public class Task26 extends Model {

    public Task26(Random random) {
        super(random);
    }

    @Override
    public void generateTask() {
        int a = getRandomIntFromTo(random, 100, 200);
        int b1 = getRandomIntFromTo(random, 2, 10);
        int b2 = getRandomIntFromTo(random, 1, b1 - 1);
        int c1 = getRandomIntFromTo(random, 1, 10);
        int c2 = getRandomIntFromTo(random, 1, 10);

        while (((double) ((c1 * b1)) * 10) % (c2 * (b1 - b2)) != 0) {
            b1 = getRandomIntFromTo(random, 2, 10);
            b2 = getRandomIntFromTo(random, 1, b1 - 1);
            c1 = getRandomIntFromTo(random, 1, 10);
            c2 = getRandomIntFromTo(random, 1, 10);
        }

        task = "The bus left town A to town B, the distance between which is " + a + " km." + lineSeparator()
                + "After " + c1 + " / " + c2 + " hour from point A to point B along the same road left the car,"
                + "the speed of which is " + b1 + " / " + b2 + " times the speed of the bus." + lineSeparator()
                + "How much time (in hours) was spent on the road from point A to point B car, "
                + " if he came to point B simultaneously with the bus?" + lineSeparator()
                + "Consider that the bus and car were traveling at constant speeds." + lineSeparator()
                + "Round the answer to tenths. Write down the answer in hours.";
        correctAnswer = String.valueOf(roundNumber(((double) (c1 * b1)) / (c2 * (b1 - b2)) - ((double) c1 / c2), 1));
        solution = "Let the bus speed be V, then the car (" + b1 + " / " + b2 + ") * V." + lineSeparator()
                + "Accordingly, the time in the road of the bus will be t and the car's t - " + c1 + " / " + c2 + "." + lineSeparator()
                + "Therefore, we can formulate a system of equations:" + lineSeparator()
                + "(" + b1 + " / " + b2 + ") * V * (t - " + c1 + " / " + c2 + ")" + " = " + a + lineSeparator()
                + "V * t = " + a + lineSeparator()
                + "We divide one equation into the other and obtain (" + b1 + " * (t - " + c1 + " / " + c2 + ")) / (" + b2 + " * t) = 1."
                + lineSeparator()
                + "We expand the brackets and multiply both sides of the equation by " + b2 + " * t." + lineSeparator()
                + "We obtain the equation " + b1 + " * t - (" + b1 + " * " + c1 + ") / " + c2 + " = " + b2 + " * t." + lineSeparator()
                + "We multiply both sides by " + c2 + " and obtain " + b1 + " * " + c2 + " * t - " + (b1 * c1) + " = " + b2 + " * " + c2 + " * t."
                + lineSeparator()
                + "We transfer the known ones to one side, the unknowns to the other t = " + (b1 * c1) + " / (" + (b1 * c2) + " - " + (b2 * c2) + ")"
                + lineSeparator()
                + "consequently you can get car time on the road " + (b1 * c1) + " / " + (b1 * c2 - b2 * c2) + " - " + c1 + " / " + c2
                + lineSeparator()
                + "Answer: " + correctAnswer;
        String d = String.valueOf(roundNumber(((double) getRandomIntFromTo(random, 10, 100)) / 10, 1));
        while (correctAnswer.equals(d)) {
            d = String.valueOf(roundNumber(((double) getRandomIntFromTo(random, 10, 100)) / 10, 1));
        }
        answerExample = d;
    }

}
