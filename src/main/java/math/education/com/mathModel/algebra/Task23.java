package math.education.com.mathModel.algebra;

import math.education.com.mathModel.Model;

import java.util.Random;

import static java.lang.System.lineSeparator;
import static math.education.com.util.CalculatorUtil.getRandomIntFromTo;

public class Task23 extends Model {

    public Task23(Random random) {
        super(random);
    }

    @Override
    public void generateTask() {
        int a = getRandomIntFromTo(random, 2, 10);
        int b = getRandomIntFromTo(random, 100, 500);
        while (b % a != 0) {
            b = getRandomIntFromTo(random, 100, 500);
        }
        task = "To replenish the account of the phone, Andrew made a specific amount of money in the payment terminal.." + lineSeparator()
                + "With this amount, a commission payment of " + (b / 100) + " dollars " + (b % 100) + " cents was withdrawn,"
                + " which is " + a + " % of the amount deposited into the terminal." + lineSeparator()
                + "How much money (in dollars) did Andrey deposit in the payment terminal?";
        correctAnswer = String.valueOf(b / a);
        solution = "We will transfer the entire commission to a cents " + (b / 100) + " dollars " + (b % 100) + " cents = " + b + " cents."
                + lineSeparator()
                + "Let's compose the proportion to find the total sum x cents / 100 % = " + b + " cents / " + a + " %." + lineSeparator()
                + "Let's express from the proportion the amount contributed to the terminal x = (" + b + " * 100) / " + a + " = "
                + ((b * 100) / a) + "." + lineSeparator()
                + "It remains to transfer cents to dollars x = " + ((b * 100) / a) + " / 100." + lineSeparator()
                + "Answer: " + correctAnswer;
        String c = String.valueOf(getRandomIntFromTo(random, 1, 10));
        while (correctAnswer.equals(c)) {
            c = String.valueOf(getRandomIntFromTo(random, 1, 10));
        }
        answerExample = c;
    }

}
