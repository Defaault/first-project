package math.education.com.mathModel.algebra;

import math.education.com.mathModel.Model;

import java.util.Random;

import static java.lang.System.lineSeparator;
import static math.education.com.util.CalculatorUtil.getRandomIntFromTo;

public class Task8 extends Model {

    public Task8(Random random) {
        super(random);
    }

    @Override
    public void generateTask() {
        int a = getRandomIntFromTo(random, 2, 5);
        int b = getRandomIntFromTo(random, 1, 5);
        int c = getRandomIntFromTo(random, 1, 5);

        task = "Find the smallest integer solution " + a + " ^ x + " + a + " ^ (x + " + b + ") ≥ "
                + (int) Math.pow(a, c) * (1 + (int) Math.pow(a, b)) + ".";
        correctAnswer = String.valueOf(c);
        solution = "By the exponential property, the expression " + a + " ^ (x + " + b + ")  can be replaced by "
                + a + " ^ x * " + a + " ^ " + b + "." + lineSeparator()
                + "Now in the expression " + a + " ^ x + " + a + " ^ x * " + a + " ^ " + b + " we can take the expression "
                + a + " ^ x " + " out of the brackets." + lineSeparator()
                + "We obtain the expression " + a + " ^ x * (1 + " + a + " ^ " + b + ") ≥ " + (int) Math.pow(a, c) * (1 + (int) Math.pow(a, b)) + "."
                + lineSeparator()
                + "We transform the inequality " + a + " ^ x ≥ " + (int) Math.pow(a, c) * (1 + (int) Math.pow(a, b)) + " / "
                + (1 + (int) Math.pow(a, b)) + "." + lineSeparator()
                + "An expression of the form " + a + " ^ x ≥ " + (int) Math.pow(a, c) + "." + lineSeparator()
                + (int) Math.pow(a, c) + " is equivalent to " + a + " ^ " + c + "." + lineSeparator()
                + "Consequently x ≥ " + c + "." + lineSeparator()
                + "Answer: " + correctAnswer;

        int d = getRandomIntFromTo(random, 1, 10);
        while (d == c) {
            d = getRandomIntFromTo(random, 1, 10);
        }
        answerExample = String.valueOf(d);
    }

}
