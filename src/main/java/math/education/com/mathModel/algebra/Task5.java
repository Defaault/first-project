package math.education.com.mathModel.algebra;

import math.education.com.mathModel.Model;

import java.util.Random;

import static java.lang.System.lineSeparator;
import static math.education.com.util.CalculatorUtil.getRandomIntFromTo;
import static math.education.com.util.CalculatorUtil.roundNumber;

public class Task5 extends Model {

    public Task5(Random random) {
        super(random);
    }

    @Override
    public void generateTask() {
        int a = getRandomIntFromTo(random, 1, 10);
        int b = getRandomIntFromTo(random, 1, 10);
        int c = getRandomIntFromTo(random, 1, 10);
        int d = getRandomIntFromTo(random, 1, 10);
        task = "Solve the system of equations" + lineSeparator()
                + "x * y = -" + a + lineSeparator()
                + "x * (" + b + " * y - " + c + ") = -" + d + lineSeparator()
                + "If (x0; y0) is the solution of the system, then find x0." + lineSeparator()
                + "Round the answer to tenths.";
        correctAnswer = String.valueOf(roundNumber(((d - a * b) / (double) c), 1));
        solution = "From the first equation, we express y in terms of x y = -(" + a + " / x)." + lineSeparator()
                + "We substitute into the second equation y x * (-((" + a + " * " + b + ") / x) - " + c + ") = - " + d + "." + lineSeparator()
                + "We open the brackets -" + (a * b) + " - " + c + " * x = -" + d + "." + lineSeparator()
                + "We take out the unknown parts of the equation in one direction, and the known parts of the equation in the other direction x = ("
                + d + " - " + (a * b) + ") / " + c + "." + lineSeparator()
                + "Answer: " + correctAnswer;
        int e = getRandomIntFromTo(random, 10, 100);
        while (correctAnswer.equals(String.valueOf(roundNumber((double) e / 10, 1)))) {
            e = getRandomIntFromTo(random, 10, 100);
        }
        answerExample = "-" + roundNumber((double) e / 10, 1);
    }

}
