package math.education.com.mathModel.algebra;

import math.education.com.mathModel.Model;

import java.util.Random;

import static java.lang.System.lineSeparator;
import static math.education.com.util.CalculatorUtil.getRandomIntFromTo;

public class Task22 extends Model {

    public Task22(Random random) {
        super(random);
    }

    @Override
    public void generateTask() {
        int a = getRandomIntFromTo(random, 1, 10);
        int b = getRandomIntFromTo(random, 1, 10);
        task = "If a < " + a + ", then " + b + " + |a - " + a + "| =";
        correctAnswer = (b + a) + " - a";
        solution = "As a < " + a + ", the module is opened with the opposite sign " + b + " - a + " + a + "." + lineSeparator()
                + "Answer: " + correctAnswer;
        int c = getRandomIntFromTo(random, 1, 10);
        while (correctAnswer.equals(c + " - a")) {
            c = getRandomIntFromTo(random, 1, 10);
        }
        answerExample = c + " - a";
    }

}
