package math.education.com.mathModel.algebra;

import math.education.com.mathModel.Model;

import java.util.Random;

import static java.lang.System.lineSeparator;
import static math.education.com.util.CalculatorUtil.getRandomIntFromTo;

public class Task1 extends Model {

    public Task1(Random random) {
        super(random);
    }

    @Override
    public void generateTask() {
        int a = getRandomIntFromTo(random, 1, 10);
        int b = getRandomIntFromTo(random, 1, 10);
        task = "Simplify the expression (" + a + " * a + " + (a * b) + ") / " + a + ".";
        correctAnswer = "a + " + b;
        solution = "In this expression in the numerator, the number " + a + " can be taken out of brackets." + lineSeparator()
                + "As a result, we obtain expression " + a + " * (a + " + b + ") / " + a + "." + lineSeparator()
                + "You can see that the expression can be reduced " + a + " times." + lineSeparator()
                + "Answer: " + correctAnswer;
        int c = getRandomIntFromTo(random, 1, 10);
        while (c == b) {
            c = getRandomIntFromTo(random, 1, 10);
        }
        answerExample = "a + " + c;
    }

}
