package math.education.com.mathModel.algebra;

import math.education.com.mathModel.Model;

import java.util.Random;

import static java.lang.System.lineSeparator;
import static math.education.com.util.CalculatorUtil.getRandomIntFromTo;

public class Task2 extends Model {

    public Task2(Random random) {
        super(random);
    }

    @Override
    public void generateTask() {
        int a = getRandomIntFromTo(random, 1, 5);
        int b = getRandomIntFromTo(random, 1, 4);
        int c = getRandomIntFromTo(random, 1, 5);
        task = "Solve the equation log" + a + "(x - " + b + ") = " + c + ".";
        correctAnswer = String.valueOf((int) Math.pow(a, c) + b);
        solution = "This expression can be rewritten in the exponential form " + a + " ^ " + c + " = x - " + b + "." + lineSeparator()
                + "Next we need to solve equation " + ((int) Math.pow(a, c)) + " = x - " + b + "." + lineSeparator()
                + "As a result, x = " + ((int) Math.pow(a, c)) + " + " + b + "." + lineSeparator()
                + "Answer: " + correctAnswer;
        int d = getRandomIntFromTo(random, 1, 10);
        while (correctAnswer.equals(String.valueOf(d))) {
            d = getRandomIntFromTo(random, 1, 10);
        }
        answerExample = String.valueOf(d);
    }

}
