package math.education.com.mathModel.algebra;

import math.education.com.mathModel.Model;

import java.util.Random;

import static java.lang.System.lineSeparator;
import static math.education.com.util.CalculatorUtil.getRandomIntFromTo;

public class Task16 extends Model {

    public Task16(Random random) {
        super(random);
    }

    @Override
    public void generateTask() {
        int a = getRandomIntFromTo(random, 1, 10);
        int b = getRandomIntFromTo(random, 1, 10);
        task = "If the numbers x and y satisfy the condition " + a + " * y + " + b + " = x, then y = ";
        correctAnswer = "(x - " + b + ") / " + a;
        solution = "Initially, we move " + b + " to the right side " + a + " * y = x - " + b + "." + lineSeparator()
                + "After, divide both parts of the expression by " + a + " and get the answer." + lineSeparator()
                + "Answer: " + correctAnswer;
        int c = getRandomIntFromTo(random, 1, 10);
        int d = getRandomIntFromTo(random, 1, 10);
        while (correctAnswer.equals("(x - " + d + ") / " + c)) {
            c = getRandomIntFromTo(random, 1, 10);
            d = getRandomIntFromTo(random, 1, 10);
        }
        answerExample = "(x - " + d + ") / " + c;
    }

}
