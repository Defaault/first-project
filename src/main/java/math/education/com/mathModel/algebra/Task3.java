package math.education.com.mathModel.algebra;

import math.education.com.mathModel.Model;

import java.util.Random;

import static java.lang.System.lineSeparator;
import static math.education.com.util.CalculatorUtil.getRandomIntFromTo;

public class Task3 extends Model {

    public Task3(Random random) {
        super(random);
    }

    @Override
    public void generateTask() {
        int a = getRandomIntFromTo(random, 1, 10);
        task = "Solve the equation " + a + " * sqrt(x) = 1.";
        correctAnswer = "1 / " + String.valueOf((int) Math.pow(a, 2));
        solution = "We take out the unknown parts of the equation in one direction,"
                + " and the known parts of the equation in the other direction sqrt(x) = 1 / " + a + "." + lineSeparator()
                + "We lift both sides of the equation to the second power x = (1 / " + a + ") ^ 2." + lineSeparator()
                + "Answer: " + correctAnswer;
        int b = getRandomIntFromTo(random, 1, 10);
        while (correctAnswer.equals("1 / " + String.valueOf(b))) {
            b = getRandomIntFromTo(random, 1, 10);
        }
        answerExample = "1 / " + b;
    }

}
