package math.education.com.mathModel.algebra;

import math.education.com.mathModel.Model;

import java.util.Random;

import static java.lang.System.lineSeparator;
import static math.education.com.util.CalculatorUtil.getRandomIntFromTo;

public class Task28 extends Model {

    public Task28(Random random) {
        super(random);
    }

    @Override
    public void generateTask() {
        int a = random.nextDouble() < 0.5 ? 3 : random.nextDouble() < 0.5 ? 6 : 9;
        task = "Given a function f(x) = x ^ 2 - " + (2 * a) + " * x + " + ((int) Math.pow(a, 2)) + lineSeparator()
                + "Calculate the area of the figure bounded by the graph of the function f and the x and y axes." + lineSeparator()
                + "Answer write down in square units.";
        correctAnswer = String.valueOf(((int) Math.pow(a, 3)) / 3);
        solution = "Since the graph intersects the y-axis, the lower limit of integration is 0." + lineSeparator()
                + "We find the point of intersection of the graph with the x axis by the equation x ^ 2 - " + (2 * a) + " * x + "
                + ((int) Math.pow(a, 2)) + " = 0." + lineSeparator()
                + "The formula for the square of the sum x ^ 2 - 2 * a * x + a ^ 2 = (x - a) ^ 2." + lineSeparator()
                + "Consequently, the equation takes the form (x - " + a + ") ^ 2 = 0." + lineSeparator()
                + "Solving this equation, we get that the upper limit of integration " + a + "." + lineSeparator()
                + "To find the area of the figure, we need to solve the integral x ^ 2 - " + (2 * a) + " * x + " + ((int) Math.pow(a, 2))
                + " with limits from 0 to " + a + "." + lineSeparator()
                + "As a result, we obtain (x ^ 3) / 3 - " + a + " * x ^ 2 + " + ((int) Math.pow(a, 2)) + " * x." + lineSeparator()
                + "We substitute the limits and obtain (" + ((int) Math.pow(a, 3)) + " / 3 - " + ((int) Math.pow(a, 3)) + " + "
                + ((int) Math.pow(a, 3)) + ") - 0." + lineSeparator()
                + "Answer: " + correctAnswer;
        String b = String.valueOf(getRandomIntFromTo(random, 1, 10));
        while (correctAnswer.equals(b)) {
            b = String.valueOf(getRandomIntFromTo(random, 1, 10));
        }
        answerExample = b;
    }

}
