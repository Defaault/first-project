package math.education.com.analysis;

public interface Analysis {

    boolean isMistake(String actualAnswer, String correctAnswer);

}
