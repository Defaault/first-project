package math.education.com.analysis.mistake;

import math.education.com.analysis.Analysis;

import java.math.BigDecimal;

import static java.util.Objects.isNull;

public class MissedSign implements Analysis {

    private static final String NUMBER_REGEX = "(-)?(\\d)+(\\.\\d+)*";

    @Override
    public boolean isMistake(String actualAnswer, String correctAnswer) {

        BigDecimal actual = init(actualAnswer);
        BigDecimal correct = init(correctAnswer);

        if (actual == null || correct == null) {
            return false;
        }

        if (actual.compareTo(new BigDecimal(0)) == 0 || correct.compareTo(new BigDecimal(0)) == 0) {
            return false;
        }

        return correct.compareTo(actual.multiply(new BigDecimal(-1))) == 0;
    }

    private BigDecimal init(String answer) {

        if (isNull(answer)) {
            return null;
        }

        String parseAnswer = answer.replaceAll(" ", "");

        return parseAnswer.matches(NUMBER_REGEX)
                ? new BigDecimal(parseAnswer)
                : null;
    }

}
