package math.education.com.analysis.mistake;

import math.education.com.analysis.Analysis;

import static java.util.Objects.isNull;

public class CorrectnessExpression implements Analysis {

    private static final String NUMBER_REGEX = "(-)?(\\d)+(\\.\\d+)*";

    @Override
    public boolean isMistake(String actualAnswer, String correctAnswer) {

        String actual = init(actualAnswer);
        String correct = init(correctAnswer);
        int quantityMistake = 0;

        if (actual == null || correct == null || actual.length() != correct.length()) {
            return false;
        }

        for (int i = 0; i < correct.length(); i++) {

            if (quantityMistake > 1) {
                return false;
            }

            if (actual.charAt(i) != correct.charAt(i)) {
                quantityMistake++;
            }
        }

        return quantityMistake == 1;
    }

    private String init(String answer) {

        if (isNull(answer)) {
            return null;
        }

        String parseAnswer = answer.replaceAll(" ", "");
        parseAnswer = parseAnswer.replaceAll(NUMBER_REGEX, "");

        return parseAnswer;
    }

}
