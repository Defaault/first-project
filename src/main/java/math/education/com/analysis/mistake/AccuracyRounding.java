package math.education.com.analysis.mistake;

import math.education.com.analysis.Analysis;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static java.util.Objects.isNull;

public class AccuracyRounding implements Analysis {

    private static final String NUMBER_REGEX = "(-)?(\\d)+(\\.\\d+)*";

    @Override
    public boolean isMistake(String actualAnswer, String correctAnswer) {

        BigDecimal actual = init(actualAnswer);
        BigDecimal correct = init(correctAnswer);

        if (actual == null || correct == null) {
            return false;
        }

        int actualScale = actual.scale();
        int correctScale = correct.scale();

        if (actualScale == correctScale) {
            return false;
        }

        if (correctScale > actualScale) {
            BigDecimal newCorrect = correct.setScale(actual.scale(), RoundingMode.HALF_EVEN);
            return actual.compareTo(newCorrect) == 0;
        } else {
            BigDecimal newActual = actual.setScale(correct.scale(), RoundingMode.HALF_EVEN);
            return correct.compareTo(newActual) == 0;
        }
    }

    private BigDecimal init(String answer) {

        if (isNull(answer)) {
            return null;
        }

        String parseAnswer = answer.replaceAll(" ", "");

        return parseAnswer.matches(NUMBER_REGEX)
                ? new BigDecimal(parseAnswer)
                : null;

    }

}
