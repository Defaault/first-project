package math.education.com.util;

import java.util.Random;

public final class KeyGenerator {

    private KeyGenerator() {

    }

    public static String generateKey(Random random, int length) {
        StringBuilder id = new StringBuilder();
        for (int i = 0; i < length; i++) {
            if (random.nextDouble() < 0.5) {
                id.append(random.nextInt(10));
            } else {
                id.append((char) (97 + random.nextInt(26)));
            }
        }
        return id.toString();
    }

}
