package math.education.com.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Random;

public final class CalculatorUtil {

    private CalculatorUtil() {
    }

    public static int getRandomIntFromTo(Random random, int from, int to) {
        int number = (int) Math.round((to - from) * random.nextDouble()) + from;
        while (number == 0) {
            number = (int) Math.round((to - from) * random.nextDouble()) + from;
        }
        return number;
    }

    public static double roundNumber(double number, int round) {
        return new BigDecimal(number).setScale(round, RoundingMode.HALF_UP).doubleValue();
    }

}
