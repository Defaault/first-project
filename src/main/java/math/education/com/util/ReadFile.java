package math.education.com.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import static java.util.stream.Collectors.toList;

public final class ReadFile {

    private ReadFile() {
    }

    public static List<String> readFile(String path) throws IOException {

        InputStream inputStream = ReadFile.class.getResourceAsStream(path);
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

        List<String> lines = bufferedReader.lines().collect(toList());

        bufferedReader.close();
        inputStreamReader.close();
        inputStream.close();

        return lines;
    }
}
