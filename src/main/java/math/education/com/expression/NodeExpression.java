package math.education.com.expression;

import java.util.List;

public interface NodeExpression {

    NodeExpression getParent();

    void setParent(NodeExpression parent);

    double getValue();

    List<NodeExpression> getNodes();

    void addNode(NodeExpression node);

}
