package math.education.com.expression.impl;

import math.education.com.container.OperationsContainer;
import math.education.com.expression.NodeExpression;
import math.education.com.operations.Calculator;

import java.util.ArrayList;
import java.util.List;

public class Sign implements NodeExpression {

    private NodeExpression parent;
    private String sign;
    private List<NodeExpression> nodes;
    private OperationsContainer container;

    public Sign(String sign, OperationsContainer container) {
        this.sign = sign;
        this.container = container;
        this.nodes = new ArrayList<>();
    }

    @Override
    public NodeExpression getParent() {
        return parent;
    }

    @Override
    public void setParent(NodeExpression parent) {
        this.parent = parent;
    }

    @Override
    public double getValue() {
        Calculator operation = container.getDataSign(sign).getCalculator();
        double[] nodes = this.nodes
                .stream()
                .mapToDouble(NodeExpression::getValue)
                .toArray();

        return operation.calculate(nodes);
    }

    @Override
    public List<NodeExpression> getNodes() {
        return nodes;
    }

    @Override
    public void addNode(NodeExpression node) {
        nodes.add(0, node);
    }

}
