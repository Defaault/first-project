package math.education.com.expression.impl;

import math.education.com.expression.NodeExpression;

import java.util.ArrayList;
import java.util.List;

public class Number implements NodeExpression {

    private NodeExpression parent;
    private double value;
    private List<NodeExpression> nodes;

    public Number(double number) {
        this.value = number;
        this.nodes = new ArrayList<>();
    }

    @Override
    public NodeExpression getParent() {
        return parent;
    }

    @Override
    public void setParent(NodeExpression parent) {
        this.parent = parent;
    }

    @Override
    public double getValue() {
        return value;
    }

    @Override
    public List<NodeExpression> getNodes() {
        return nodes;
    }

    @Override
    public void addNode(NodeExpression node) {
        nodes.add(0, node);
    }

}
