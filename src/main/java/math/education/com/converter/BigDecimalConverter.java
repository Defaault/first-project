package math.education.com.converter;

import javax.persistence.AttributeConverter;
import java.math.BigDecimal;

public class BigDecimalConverter implements AttributeConverter<BigDecimal, Double> {

    @Override
    public Double convertToDatabaseColumn(BigDecimal bigDecimal) {
        return bigDecimal.doubleValue();
    }

    @Override
    public BigDecimal convertToEntityAttribute(Double assessment) {
        return new BigDecimal(assessment);
    }

}
