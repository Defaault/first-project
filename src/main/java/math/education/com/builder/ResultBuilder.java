package math.education.com.builder;

import math.education.com.entity.Task;
import math.education.com.entity.Topic;
import org.springframework.data.domain.Page;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.Objects.nonNull;

public class ResultBuilder {

    private static final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    private Map<String, Object> attributes;

    private ResultBuilder() {
        attributes = new HashMap<>();
    }

    public static ResultBuilder builder() {
        return new ResultBuilder();
    }

    public ResultBuilder paginationData(Page<Task> taskPage) {
        attributes.put("paginationData", taskPage);

        return this;
    }

    public ResultBuilder activeTopics(List<Topic> topics) {
        attributes.put("activeTopics", topics);

        return this;
    }

    public ResultBuilder allTopics(List<Topic> allTopics) {
        attributes.put("allTopics", allTopics);

        return this;
    }

    public ResultBuilder fromDateTaskEducation(LocalDate fromDateTaskEducation) {
        String fromDateTaskEducationValue = formatDate(fromDateTaskEducation);

        attributes.put("fromDateTaskEducation", fromDateTaskEducationValue);

        return this;
    }

    public ResultBuilder toDateTaskEducation(LocalDate toDateTaskEducation) {
        String toDateTaskEducationValue = formatDate(toDateTaskEducation);

        attributes.put("toDateTaskEducation", toDateTaskEducationValue);

        return this;
    }

    public ResultBuilder existsAnswer(Boolean isExistsAnswer) {
        attributes.put("isExistsAnswer", isExistsAnswer);

        return this;
    }

    public ResultBuilder nonExistsAnswer(Boolean isNonExistsAnswer) {
        attributes.put("isNonExistsAnswer", isNonExistsAnswer);

        return this;
    }

    public ResultBuilder correctAnswer(Boolean isCorrectAnswer) {
        attributes.put("isCorrectAnswer", isCorrectAnswer);

        return this;
    }

    public ResultBuilder incorrectAnswer(Boolean isIncorrectAnswer) {
        attributes.put("isIncorrectAnswer", isIncorrectAnswer);

        return this;
    }

    public Map<String, Object> build() {
        return attributes;
    }

    private String formatDate(LocalDate localDate) {
        return nonNull(localDate)
                ? localDate.format(DATE_FORMAT)
                : null;
    }

}
