package math.education.com.filter;

import math.education.com.entity.dto.InnerSession;
import math.education.com.entity.User;
import math.education.com.service.SessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static java.util.Collections.singletonList;
import static java.util.Objects.nonNull;

public class SessionAuthenticationFilter extends OncePerRequestFilter {

    @Autowired
    private SessionService sessionService;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {
        String sessionId = request.getParameter("sessionId");
        InnerSession session = sessionService.getInnerSession(sessionId);

        if (nonNull(session)) {
            User user = (User) session.getAttribute("user");

            if (nonNull(user)) {
                SimpleGrantedAuthority authority = new SimpleGrantedAuthority(user.getRole().getRole().name());
                Authentication authentication = new UsernamePasswordAuthenticationToken(user.getEmail(), null, singletonList(authority));

                SecurityContextHolder.getContext().setAuthentication(authentication);
            }

        }

        filterChain.doFilter(request, response);
    }

}
