FROM openjdk:8
ADD files/* /opt/math-education-backend/
ADD target/*.jar /opt/math-education-backend/
RUN chmod 755 /opt/math-education-backend/start.sh
CMD exec /opt/math-education-backend/start.sh